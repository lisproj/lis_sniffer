#ifndef CAN_PROT_CPP
#define CAN_PROT_CPP
#include "can_prot.h"


QByteArray CanProt::IntToByteCAN(int number)
{
    QByteArray numberhex="00";
    if (number<16) numberhex="0"+QByteArray::number(number,16); // если адрес полбайта добавляем 0
    else numberhex=QByteArray::number(number,16);

    numberhex=numberhex.right(2);//обрезаем если число больше 1 байт
    return numberhex;
}
QByteArray CanProt::IntTo2ByteCAN(int number)
{
    QByteArray numberhex="0000";
    if (number<16) numberhex="000"+QByteArray::number(number,16); // если адрес полбайта добавляем 000
    else if (number<256) numberhex="00"+QByteArray::number(number,16); // если адрес полбайта добавляем 00
         else if (number<4096) numberhex="0"+QByteArray::number(number,16); // если адрес полбайта добавляем 0
              else numberhex=QByteArray::number(number,16);
   // qDebug()<<"numberhex: "<<numberhex;
        numberhex=numberhex.right(4);//обрезаем если число больше 2 байт
    return numberhex;
}

QByteArray CanProt::IntTo2ByteCANLM(int number)
{
    QByteArray numberhex="0000";
    QByteArray litle=IntToByteCAN(number&0x00ff);
    QByteArray most=IntToByteCAN((number>>8)&0x00ff);
    numberhex=litle+most;
  // qDebug()<<"numberhex: "<<numberhex;
        numberhex=numberhex.right(4);//обрезаем если число больше 2 байт
    return numberhex;
}
#endif // CAN_PROT_CPP
