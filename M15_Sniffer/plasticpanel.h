#ifndef PLASTICPANEL_H
#define PLASTICPANEL_H

#include <QDialog>

namespace Ui {
class PlasticPanel;
}

class PlasticPanel : public QDialog
{
    Q_OBJECT

public:
    explicit PlasticPanel(QWidget *parent = 0);
    ~PlasticPanel();
signals:
    void sig_on_pbTpClean_toggled(bool checked);
    void sig_on_pbTpNext_clicked();
    void sig_on_pbTpPrevious_clicked();
    void sig_on_pbTpEncoder_sliderMoved(int position);
    void sig_on_pbTpEncoder_sliderReleased();

private slots:
    void on_pbTpClean_toggled(bool checked);

    void on_pbTpPrevious_clicked();

    void on_pbTpNext_clicked();

    void on_pbTpEncoder_sliderMoved(int position);

    void on_pbTpEncoder_sliderReleased();

private:
    Ui::PlasticPanel *ui;
};

#endif // PLASTICPANEL_H
