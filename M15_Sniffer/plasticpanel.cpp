#include "plasticpanel.h"
#include "ui_plasticpanel.h"
#include "QDebug"

PlasticPanel::PlasticPanel(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlasticPanel)
{
    ui->setupUi(this);
     this->setWindowFlag(Qt::WindowStaysOnTopHint,true);//окно всегд поверх других
    //this->show();
    qDebug()<<"Create PP";
}

PlasticPanel::~PlasticPanel()
{
   qDebug()<<"Delete PP";
    delete ui;
}

void PlasticPanel::on_pbTpClean_toggled(bool checked)
{
 emit sig_on_pbTpClean_toggled( checked);
}

void PlasticPanel::on_pbTpPrevious_clicked()
{
  emit sig_on_pbTpPrevious_clicked();
}

void PlasticPanel::on_pbTpNext_clicked()
{
  emit   sig_on_pbTpNext_clicked();
}

void PlasticPanel::on_pbTpEncoder_sliderMoved(int position)
{
  emit   sig_on_pbTpEncoder_sliderMoved(position);
}

void PlasticPanel::on_pbTpEncoder_sliderReleased()
{
  emit   sig_on_pbTpEncoder_sliderReleased();
}
