#include "guns.h"


int Guns::ResID = 0;
int SensorMotion::SensID=0;
int SensorDanfoss::SensDanfID=0;
qint16 Guns::beadsMask = 0;

Guns::Guns(QWidget *parent) :  QHBoxLayout(parent)
{
     /*if (!ResID) */initGuns();
     ResID++;
     buttonID=ResID;
     type=GUN_TYPE_NONE;//
       //qDebug()<<"NEw gun"<<buttonID;
    leNumber= new QLineEdit();  // Создаем объект динамической кнопки
    leNumber->setReadOnly(1);
     leBrokeOrSolid= new QLabel();  // Сплошная или прерывистая
     leBrokeOrSolid->setScaledContents(true);
     leBrokeOrSolid->setFixedSize(50,15);
     leTi= new QLineEdit();  // Создаем объект динамической кнопки
     leTo=new QLineEdit();  // Создаем объект динамической кнопки
     leS2= new QLineEdit();  // Создаем объект динамической кнопки
     leR2=new QLineEdit();  // Создаем объект динамической кнопки
     leBase= new QLineEdit();  // Создаем объект динамической кнопки
     lebeads= new QLineEdit();  // Создаем объект динамической кнопки
  //   btsendM =new QPushButton();  // Создаем объект динамической кнопки
     lbState= new QLabel();

    lbState->setPixmap(QPixmap("://PNG/red-lamp.png"));
    lbState->setScaledContents(true);
    lbState->setFixedSize(15,15);
//     qDebug()<<   lbState->height()<< lbState->width();
    this->addWidget(leNumber);this->addWidget(leBrokeOrSolid);
    this->addWidget(leTi);this->addWidget(leTo);
    this->addWidget(leS2);this->addWidget(leR2);
    this->addWidget(leBase);this->addWidget(lebeads);
    this->addWidget(lbState);//this->addWidget(btsendM);
    leNumber->setText(QString::number(ResID));

// connect(btsendM, SIGNAL(clicked()), this, SLOT(slotGetNumber()));
    connect(this, SIGNAL(setBeadsGuns()),this,SLOT(settingBeads()));
    connect(this, SIGNAL(setLisGuns()),this,SLOT(settingLisGuns()));
    connect(this, SIGNAL(setSimpleGuns()),this,SLOT(settingSimpleGuns()));
    connect(this, SIGNAL(setState(bool)),this,SLOT(settingLamp(bool)));
}

Guns::~Guns()
{
  //qDebug()<<"Delete gun";
 delete leNumber;
 delete   leBrokeOrSolid;
 delete   leTi;
 delete   leTo;
 delete   leS2;
 delete   leR2;
 delete   leBase;
 delete   lebeads;
 delete   lbState;
}

const int Guns::getID()
{
  int  temp=buttonID;
  return temp;//             buttonID ;   // Локальная переменная, номер кнопки
}

void Guns::initGuns()
{
    //Инициализация
  //  qDebug()<<"init Guns";

        //lisGuns.number=buttonID;
        lisGuns.isVisible=false;
        lisGuns.brokeOrSolid=0;
        lisGuns.Ti=0;
        lisGuns.To=0;
        lisGuns.S2=0;
        lisGuns.R2=0;
        lisGuns.beads=0;
        lisGuns.Base=0;


        //beadsguns.number=buttonID;
        beadsguns.isVisible=false;
        beadsguns.Ti=0;
        beadsguns.To=0;
        beadsguns.Base=0;

}

void Guns::showGun()
{
    leNumber->show();
    leBrokeOrSolid->show();
    leTi->show();
    leTo->show();
    leS2->show();
    leR2->show();
    lebeads->show();
    leBase->show();
 //  btsendM->show();
    lbState->show();

}

void Guns::hideGun()
{
    leNumber->hide();
    leBrokeOrSolid->hide();
    leTi->hide();
    leTo->hide();
    leS2->hide();
    leR2->hide();
    lebeads->hide();
    leBase->hide();
  // btsendM->hide();
    lbState->hide();
}

/*void Guns::slotGetNumber()
{
    emit PbSend(buttonID);
    qDebug()<<"press"<<buttonID;
}*/

void Guns::settingBeads()
{

     leBrokeOrSolid->setDisabled(1);
     leS2->setDisabled(1);
     leR2->setDisabled(1);
     lebeads->setDisabled(1);
     leTo->setDisabled(0);
}

void Guns::settingLisGuns()
{
    leBrokeOrSolid->setDisabled(0);
    leS2->setDisabled(0);
    leR2->setDisabled(0);
    lebeads->setDisabled(0);
    leTo->setDisabled(0);
}

void Guns::settingSimpleGuns()
{
    leBrokeOrSolid->setDisabled(0);
    leS2->setDisabled(1);
    leR2->setDisabled(1);
    lebeads->setDisabled(0);
    leTo->setDisabled(1);
}

void Guns::settingLamp(bool newstate)
{
    if( newstate)
          lbState->setPixmap(QPixmap(":/PNG/green-lamp.png"));
       else
           lbState->setPixmap(QPixmap(":/PNG/red-lamp.png"));
}

SensorMotion::SensorMotion(QWidget *parent):  QHBoxLayout(parent)
{


    initGuns();
    SensID++;
    buttonID=SensID;
    //qDebug()<<"NEw gun"<<buttonID;
    leNumber= new QLineEdit();  // Создаем объект динамической кнопки
    leNumber->setReadOnly(1);
    leNumber->setFixedSize(35,15);
    leNumber->setAlignment(Qt::AlignHCenter);


    lbguns= new QLabel();  // Сплошная или прерывистая
    lbguns->setScaledContents(true);
    lbguns->setFixedSize(100,15);
    lbguns->setAlignment(Qt::AlignHCenter);


    lbAlarm= new QLabel();  // Создаем объект динамической кнопки
    lbAlarm->setScaledContents(true);
    lbAlarm->setFixedSize(55,15);
    lbAlarm->setAlignment(Qt::AlignHCenter);

    lbErrCAN= new QLabel();  // Создаем объект динамической кнопки
    lbErrCAN->setScaledContents(true);
    lbErrCAN->setFixedSize(75,15);
    lbErrCAN->setAlignment(Qt::AlignHCenter);

    lbErrEpr= new QLabel();  // Создаем объект динамической кнопки
    lbErrEpr->setScaledContents(true);
    lbErrEpr->setFixedSize(75,15);
    lbErrEpr->setAlignment(Qt::AlignHCenter);

    lbEprSwitch= new QLabel();  // Создаем объект динамической кнопки
    lbEprSwitch->setScaledContents(true);
    lbEprSwitch->setFixedSize(75,15);
    lbEprSwitch->setAlignment(Qt::AlignHCenter);

    lbEprChangeAdr= new QLabel();  // Создаем объект динамической кнопки
    lbEprChangeAdr->setScaledContents(true);
    lbEprChangeAdr->setFixedSize(75,15);
    lbEprChangeAdr->setAlignment(Qt::AlignHCenter);

    lbVers= new QLabel();  // Создаем объект динамической кнопки
    lbVers->setScaledContents(true);
    lbVers->setFixedSize(75,15);
    lbVers->setAlignment(Qt::AlignHCenter);

    btsendM =new QPushButton();  // Создаем объект динамической кнопки
    btsendM->setFixedSize(75,20);
    btsendM->setText("EEPROM");

    btsendM2 =new QPushButton();  // Создаем объект динамической кнопки
    btsendM2->setFixedSize(75,20);
    btsendM2->setText("Вкл");

    btsendM3 =new QPushButton();  // Создаем объект динамической кнопки
    btsendM3->setFixedSize(100,20);
    btsendM3->setText("Задать адрес");

    sbNewAdress=new QSpinBox();  // Создаем объект динамической кнопки
    sbNewAdress->setFixedSize(100,20);
    sbNewAdress->setMinimum(0);
    sbNewAdress->setMaximum(9);

    lbState= new QLabel();

    lbState->setPixmap(QPixmap("://PNG/red-lamp.png"));
    lbState->setScaledContents(true);
    lbState->setFixedSize(15,15);
    //     qDebug()<<   lbState->height()<< lbState->width();
    this->addWidget(leNumber);
    this->addWidget(lbState);
    this->addWidget(lbAlarm);
    this->addWidget(lbErrCAN);
    this->addWidget(lbErrEpr);
    this->addWidget(lbguns);
    this->addWidget(lbEprSwitch);
    this->addWidget(lbEprChangeAdr);
    this->addWidget(lbVers);
    this->addWidget(btsendM);
    this->addWidget(btsendM2);
    this->addWidget(btsendM3);
    this->addWidget(sbNewAdress);

    //
    leNumber->setText(QString::number(SensID-1));

    connect(btsendM, SIGNAL(clicked()), this, SLOT(reqEprom()));
    connect(btsendM2, SIGNAL(clicked()), this, SLOT(reqSetAlarm()));
    connect(btsendM3, SIGNAL(clicked()), this, SLOT(reqSetAdr()));
    connect(this, SIGNAL(setState(bool)),this,SLOT(settingLamp(bool)));

}

SensorMotion::~SensorMotion()
{
    //qDebug()<<"Delete gun";
    delete leNumber;
    delete   lbguns;
    delete   lbAlarm;
    delete   lbErrCAN;
    delete   lbErrEpr;
    delete   sbNewAdress;
    delete   lbState;
    delete   lbEprSwitch;
    delete   lbEprChangeAdr;
    delete   lbVers;

}

const int SensorMotion::getID()
{
    int  temp=buttonID;
    return temp;//             buttonID ;   // Локальная переменная, номер кнопки
}

void SensorMotion::initGuns()
{
    //Инициализация
 //   qDebug()<<"init SensorMotion";

    //beadsguns.number=buttonID;
    isVisible=false;
    numGuns=0;
    motion=1;
    alarm=2;
    errCan=0;
    errEprom=0;
    onOff=0;
    changeAdr=0;
    version=0;
}

void SensorMotion::showGun()
{
    leNumber->show();
    lbguns->show();
    lbAlarm->show();
    lbErrCAN->show();
    lbErrEpr->show();
    lbEprChangeAdr->show();
    lbEprSwitch->show();
    lbVers->show();
    sbNewAdress->show();
    btsendM->show();
    btsendM2->show();
    btsendM3->show();
    lbState->show();
}

void SensorMotion::hideGun()
{
    leNumber->hide();
    lbguns->hide();
    lbAlarm->hide();
    lbErrCAN->hide();
    lbErrEpr->hide();
    lbEprChangeAdr->hide();
    lbEprSwitch->hide();
    lbVers->hide();
    sbNewAdress->hide();
    btsendM->hide();
    btsendM2->hide();
    btsendM3->hide();
    lbState->hide();
}

void SensorMotion::reqEprom()
{
    emit sreqEprom(buttonID);
}

void SensorMotion::reqSetAlarm()
{
    emit sreqSetAlarm(buttonID);
}

void SensorMotion::reqSetAdr()
{
    emit sreqSetAdr(buttonID);
}

void SensorMotion::settingLamp(bool newstate)
{
    if( newstate)
          lbState->setPixmap(QPixmap(":/PNG/green-lamp.png"));
       else
           lbState->setPixmap(QPixmap(":/PNG/red-lamp.png"));
}




SensorDanfoss::SensorDanfoss(QWidget *parent)  :  QHBoxLayout(parent)
{
    SensDanfID++;
    buttonID=SensDanfID;
    //Инициализация
  //  qDebug()<<"init SensorDanfoss";
    isVisible=false;
    addres=0;
    rpm=0;
    name="не распознан";
    //qDebug()<<"NEw gun"<<buttonID;
    lbNumber= new QLabel();  // Создаем объект динамической кнопки
    lbNumber->setFixedSize(35,15);
    lbNumber->setAlignment(Qt::AlignHCenter);
    lbNumber->setText(QString::number(SensDanfID));


    lbAdress= new QLabel();  // Адрес
    lbAdress->setScaledContents(true);
    lbAdress->setFixedSize(50,15);
    lbAdress->setAlignment(Qt::AlignHCenter);
    lbAdress->setText(QString::number(addres));

    lbName= new QLabel();  // Имя
    lbName->setScaledContents(true);
    lbName->setFixedSize(300,15);
    lbName->setAlignment(Qt::AlignLeft);
    lbName->setText(name);


    lbRpm= new QLabel();  // Обороты
    lbRpm->setScaledContents(true);
    lbRpm->setFixedSize(50,15);
    lbRpm->setAlignment(Qt::AlignHCenter);
    lbRpm->setText(QString::number(rpm));

    btsendAdr =new QPushButton();  // Создаем объект динамической кнопки
    btsendAdr->setFixedSize(60,20);
    btsendAdr->setText("Выбрать");

    btsendSpeed =new QPushButton();  // Создаем объект динамической кнопки
    btsendSpeed->setFixedSize(80,20);
    btsendSpeed->setText("Период 200мс");


    this->addWidget(lbNumber);
    this->addWidget(lbAdress);
    this->addWidget(lbRpm);
    this->addWidget(lbName);
    this->addWidget(btsendSpeed);
    this->addWidget(btsendAdr);




   connect(btsendAdr, SIGNAL(clicked()), this, SLOT(reqAdr()));
   connect(btsendSpeed, SIGNAL(clicked()), this, SLOT(reqSpeed()));
  /*   connect(btsendM3, SIGNAL(clicked()), this, SLOT(reqSetAdr()));
    connect(this, SIGNAL(setState(bool)),this,SLOT(settingLamp(bool)));*/
}

SensorDanfoss::~SensorDanfoss()
{
    //qDebug()<<"Delete gun";
    SensDanfID--;
    delete lbNumber;
    delete   lbAdress;
    delete   lbRpm;
    delete   lbName;
    delete btsendAdr;
    delete btsendSpeed;
}

const int SensorDanfoss::getID()
{
    int  temp=buttonID;
    return temp;//             buttonID ;   // Локальная переменная, номер кнопки
}

void SensorDanfoss::showSens()
{
    lbNumber->show();
    lbAdress->show();
    lbRpm->show();
    lbName->show();
    btsendAdr->show();
    btsendSpeed->show();
}

void SensorDanfoss::hideSens()
{
    addres=0;
    lbNumber->hide();
    lbAdress->hide();
    lbRpm->hide();
    lbName->hide();
    btsendAdr->hide();
    btsendSpeed->hide();
}

void SensorDanfoss::reqAdr()
{
    emit transmitAddres(addres);
}

void SensorDanfoss::reqSpeed()
{
     emit transmitSpeed(addres);
}


