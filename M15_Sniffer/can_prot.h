#ifndef CAN_PROT_H
#define CAN_PROT_H

#include <QObject>
#include <QDebug>

//int makeCommand(void){qDebug()<<"MAkecommand"; return 12;}
namespace  CanProt {
typedef enum {         //Типы сообщений
    ALARM_MSG=0x00,  //Широковещательные сообщения
    CMD_MSG=0x04,       //Команда
    EVENT_MSG=0x08,     //Событие
    CMD_ACK_MSG=0x0C,   //Подтверждение команды
    EVENT_ACK_MSG=0x10, //Подтверждение События
    DATA_REQ_MSG=0x14,  //Запрос данных
    DATA_MSG=0x18,      //Данные
    BROADC_DATA=0x1C  //Широковещательные сообщения
}MsgId;
typedef enum {          //Адреса устройств
    CAN_ADR_MIK=0x40,           // Адрес мик
    CAN_ADR_MIK_2=0x41,           // Адрес мик 2
    CAN_ADR_TERMINAL=0x20,       // Адрес Пульт
    CAN_ADR_FAKE_TERMINAL=0x2F,  // Адрес Пультa от программы сниффера
    CAN_ADR_SCHNEIDR=0xC0,   // Адрес Шнайдер
    CAN_ADR_GYRO=0x70,   // Адрес гироскопа
    CAN_ADR_MBO=0x50,     //  MBO_ADR
    CAN_ADR_UBUP=0x10,     //  Адрес UBUP
    CAN_ADR_UBUP_2=0x11,     //  Адрес UBUP 2
    CAN_ADR_CANHUB=0x60,     //  Адрес CANHUB
    CAN_ADR_CAN2CAN=0x61,     //  Адрес CAN2CAN
    CAN_ADR_ALL=0xFF,
    CAN_ADR_MIX_L=0xC2,
    CAN_ADR_MIX_R=0xC3,
    CAN_ADR_DANFOSS_1=0xD1,
    CAN_ADR_DANFOSS_2=0xD2,
    CAN_ADR_DANFOSS_3=0xD3,
    CAN_ADR_TECH_TERMINAL=0xE0,
    CAN_ADR_SMS=0x80,
}DevAddr;
typedef enum {  //Коды структур данных
    CIKL_MARKING_START=0x60,    //команда сарта записи цикла разметки
    CIKL_MARKING=0x61,    //команда записи цикла разметки
    CIKL_MODE=0x00,       //Режим разметки
    COR_LIS_PARAMS=0x11,       //Параметры коррекциия для каждого пистолета
    COR_GUNS_PARAMS=0x21,       //Параметры коррекциия для покрасочного пистолета (без лиса)
    BEADS_PARAMS=0x01,       //Параметры коррекциия для каждого СОГ пистолета
    GUNS_SETUP=0x31,       //параметры размещения пистолетов
    CONF_CYCLE=0x41,        // Конфигурация цикла
    CONF_FOR_STATISTIC=0x42,        // Конфигурация для статистики
    CONF_BASE=0x43,        // Конфигурация машины
    CONF_MODUL=0x44,        // Конфигурация работы моудля
    CONF_HYDRO_PID1=0x45,        // Коэф. пид для гидромотора
    CONF_HYDRO_PID2=0x46,        //  Коэф. пид для гидромотора
    CONF_HYDRO_RPM=0x47,        // Идентификатор команды управления двигателем
    CONF_DELAYS_CP=0x49,        // Идентификатор команды задержек клапанов ХП
    CIKL_MARKING_STOP=0x55,        // Идентификатор последнего сообщения
    CONF_DIRECT=0x20,        // Задержки онлайн
    MIK_CMD=0x40,          //Команда управления MIK
    MIK_STOP=0x00,          //Команда управления MIK стоп разметки
    MIK_START=0x01,          //Команда управления MIK стар разметки
    MIK_SPEED_GEN=0x02,          //Команда управления MIK стар разметки
    MIK_START_RELEASE=0x03,          //Команда управления MIK стар разметки
    MIK_PHASE=0x04,          //Команда управления MIK стар разметки
    MIK_PHOTO_SENS=0x05,          //Команда управления MIK стар разметки
    MIK_CLEAN=0x06,          //Команда управления MIK прочистка
    MIK_SOURCE_COORD=0x07,          //Команда управления MIK стар разметки
    MIK_LINE_TYPE=0x08,          //Команда управления MIK стар разметки
    MIK_15_GUNS=0x10,          //Команда управления MIK стар разметки
    MIK_WASHING=0x011,          //Команда управления MIK промывка для ХП
    MIK_DIRECT_GUN=0x62,          //Команда управления пистолетами напрямую
    MIK_DIRECT_GUN_ON=0x01,          //включить режим прямого управления
    MIK_DIRECT_GUN_OFF=0x00,          //выключить режим прямого управления
    MIK_READ_DATA=0xA0,          //Команда запроса пула данных
    MIK_CMD_WRITE_DATA_EEP=0xDD,          //Команда записи в энергонезависимую память
   // MIK_DATA_LSTAT=0x00,          //Идентификатор Lstat

    MIK_DATA_PERC_OFF=0x00,       //Идентификатор PercOff процент заполнения для выключения
    MIK_DATA_LR=0x01,             //Идентификатор Lr длина разрыва в сплошной
    MIK_DATA_LMARK=0x02,          //Идентификатор Lmark длина отрезка сплошной меджу разрывами м
    MIK_DATA_COUNTER_SPEED=0x03,  //Идентификатор  счетчика пути
    MIK_DATA_PERC_DET=0x04,       //Идентификатор PercDet
    MIK_DATA_LIMIT_STROKE=0x05,   //Идентификатор допуск штриха см
    MIK_DATA_LIMIT_SPASE=0x06,     //Идентификатор допуск пробела см
    MIK_CMD_STATISTIC=0x62,       //Команда записи параметров статистики в регистратор

  //  MIK_START=0x01,          //Команда управления MIK стар разметки
    GYRO_DATE=0x01,         // Данные гироскопа
    GYRO_CMD=0x70,          //Команда управления гироскопом
    GYRO_SWITCH=0x00,       // функция ON/OFF
    GYRO_ON=0x01,       // функция включения  гироскопа
    GYRO_OFF=0x00,       // функция выключения  гироскопа

    //ADC command  for ColdPlastic  Modules

    MIK_ADC_CMD=0x72,       //Команда управления ацп
    ADC_CAN_OFF=0x00,   //выключение передачи значений в CAN
    ADC_CAN_ON_VOLT=0x01,   //включение передачи значений в Вольтах*100  0..30В
    ADC_CAN_ON_CUR=0x02,   //включение передачи значений в милиАмперах*100 0..20 мА
    ADC_CAN_ON_PERC=0x03,   // включение передачи значений в Процентах (0..100)
    ADC_CAN_ON_REG=0x04,   // включение передачи значений регистра 0..4096


    //Event
    EVENT_SWITCH_GUN=0x10,      // события переключения пистолета
    EVENT_SWITCH_OFF_GUN=0x11,      // события выклключения пистолета
    EVENT_KEYS_FROM_TP=0x12,      // события нажатия на терминальной панели
    //Broadcast PULT
    BC_PULT_TIME=0x01,      // время пульта
    BC_PULT_ADR_BLOCK=0x02,      // адреса блоков для разметочного блока
    //Broadcast MIK
    BC_MIK_CUR1=0x01,      // текущие параметры
    BC_MIK_CUR2=0x02,      // текущие параметры
    BC_MIK_CUR3=0x03,      // текущие параметры
    BC_MIK_CUR4=0x04,      // текущие параметры
    BC_MIK_CUR5=0x05,      // текущие параметры
    BC_MIK_CUR6=0x06,      // текущие параметры
    BC_MIK_CUR7=0x07,      // текущие параметры
    BC_MIK_CUR8=0x08,      // текущие параметры
    //DATA MIK
    DATA_MIK=0xDD, // данные (передача переменных)
    DATA_MIK_STAT=0xCA, // данные статистики

    //COMMAND_MBO
    MBO_CORRIDIR=0x41, // команда установки координат целевого коридора каретки привода
    MBO_COMMAND=0x50,    //команда записи цикла разметки

    //Broadcast DANFODSS
    BC_DANF_RPM=0xF8,



    //Broadcast Sheider
    BC_PRESSURE=0xFE,   // широковещательный давление
    // COMMAND Sheider
    SCHN_SET_RPM=0xF5,
    SCHN_SET_PID=0xF4,

}StrId;

typedef enum  //типы режимов работы
{
    WM_NONE=-1,
    WM_AUT0=0,
    WM_SEMI,
    WM_MANUAL,
    WM_AS_IS,
    WM_SEMI_LIS,
    WM_AUTO_LIS
}wM;


enum  //типы параметров для передачи по CAN
{
    NUMB_TYPE=0,
    STRING_TYPE,
    BYTES_TYPE,
    SINGL_BUFFER
};
typedef enum {  //Список устройств на шине can
    NO_DEVICE=-1,
    MOTION_DEVICE=0,
    TEHNOLOGY_DEVICE,
    //----------------
    TOT_DEVICES
}DevId;
enum    //статус общения с устройствами на шине
{
    NORMAL_STATUS=0,
    POOLREQ_STATUS,
    PARAM_WR_STATUS,
    STR_WR_STATUS,
    DATA_WR_STATUS
};
enum    //версии ПО
{
    VERS_A=0,   //0x00 – альфа-версия;
    VERS_B,     //0x01 – бета-версия;
    VERS_RC,    //0x02 – выпуск-кандидат;
    VERS_R      //0x03 – публичный выпуск.
};
enum    //типы устройств для выбора адреса
{
    MOTION_SELECT,
    TEHNOLOGY_SELECT,
  //---------------------------
    TOTAL_SELECT
};

enum
{
    SET_OFF=0,
    SET_ON=1
};
enum hydroType{  // тип гидромотора
    SCREW=0,       // Шнек
    DRUM=1,        // барабан/мешалка
    PUMPLEFT=2,    // левый насос
    PUMPRIGHT=3,   // правый насос
    HARDLEFT=4,    // левый отвердитель
    HARDRIGHT=5,   // правый отвердитель
    MIXLEFT=6,     // левый миксер
    MIXRIGHT=7,    // правый миксер
    SPRAYPUMP=8,   // спрей пластик
    PUMPLEFTMAG=9,    // левый насос магистральной
    PUMPRIGHTMAG=10,   // правый насос магистральной,
    ALL_HYDRO
};
enum  ValNames//перечисление всех клапанов для ХП
{
    VPLASTL = 0,  //клапан материала левый
    VPLASTR = 1,  //клапан материала правый
    VHARDL = 2,  // отвердитель левый
    VHARDR = 3,  // отвердитель правый
    VSHIBL = 4,  // шибер левый
    VSHIBR = 5,  // шибер правый
    VSP1L = 6,   // Скоростной клапан 1 левый
    VSP2L = 7,   // Скоростной клапан 2 левый
    VSP1R = 8,   // Скоростной клапан 1 правый
    VSP2R = 9,   // Скоростной клапан 2 правый
    NUM_VALVES_CP=VSP2R+1,    // клапана для разметки
    VRPLASTL = 10,  //клапан  рециркуляции материала левый
    VRPLASTR = 11,  //клапан рециркуляции  материала правый
    VRHARDL = 12,  // отвердитель рециркуляции левый
    VRHARDR = 13,  // отвердитель рециркуляции  правый
    NUM_VALVES_CP_REC=VRHARDR,
    VCLL =14,
    VCLR =15,
    VSOLVL =16,
    VSOLVR =17,
    NUM_VALVES_ALL=  VSOLVR,
    PPLL=18,  //  задержка насоса пластика левого
    PPLR=19, //  задержка насоса пластика правого
    PHDL=20, //  задержка насоса отвердителя левого
    PHDR=21, //  задержка насоса отвердителя правого
    PPPUSH=22, // амплитуда толчка и длительность насоса пластика
    HARDSENSORS=23, // порог сработки отвердителя % и  зона нечуствительности в см
    NUM_DELAYS_ALL=HARDSENSORS
};

enum  hydroMode{  // режим гидромотора
    HYD_0FF=0,       // выкл
    HYD_MPID=1,        // ручной пид
    HYD_APID=2,    // авто пид
    HYD_PWM=3,   // шим
    HYD_SSPID=4,    // старт стоп ПИД
    ALL_HYDRO_MODE
};

enum techPanel
{
/*  CAN_DATA[2] - два байта данных
    формат данных кнопок
    CAN_DATA[0]=EVENTS_KEY_*, CAN_DATA[1]=Buttons
    формат данных энкодера
    CAN_DATA[0]=EVENTS_ENCODER_*, CAN_DATA[1]=значение энкодера 0-255
*/
//EVENTS
EVENT_KEY_PRESS =0x01,
EVENT_KEY_UNPRESS =0x02,
EVENT_ENCODER_LEFT =0x03,
EVENT_ENCODER_RIGHT =0x04,
// Buttons
TP_KEY_CLEAN=0x01,  //  Промывка
TP_KEY_PREV_PARAM=0x02,  //  Предыдущий параметр
TP_KEY_NEXT_PARAM=0x03,  //  Следующий параметр
//Joysstick Lis
JOY_UP=0x04,  //  Джойстик вверх
JOY_DOWN=0x05,  //  Джойстик вниз
JOY_LEFT=0x06,  //  Джойстик влево
JOY_RIGHT=0x07,  //  Джойстик вправо
PB_SPEED=0x08,  //  Черепаха/зайчик

};

QByteArray IntToByteCAN(int number); // преобразование int в QByteArray c добавлением нуля.
QByteArray IntTo2ByteCAN(int number); // преобразование int в 2 БАЙТА QByteArray c добавлением нуля младший справа.
QByteArray IntTo2ByteCANLM(int number); // преобразование int в 2 БАЙТА QByteArray c добавлением нуля младший слева.

}
//------------------------------------------------

#endif // CAN_PROT_H
