#include "port.h"
#include <qdebug.h>
#include <QSettings>
#include <QDateTime>

#include "ui_mainwindow.h"
#include "mainwindow.h"
using namespace CanProt;



Port::Port(QObject *parent) :
    QObject(parent)
{
    timer.setInterval(TIMEOUT_TIMER_CAN);
    timer.setSingleShot(false);
    QObject::connect(
                &timer, SIGNAL(timeout()),
                this, SLOT(workTimeout()));
    timer.start();
    countSnifferPacket=0; // число перехваченных пакетов
    mikOffset=0;  // по умолчанию смещение мик=0

    speedCurPacketInMinute=0; // текущая скорость число перехваченных пакетов в минуту
    speedMidPacketInMinute=0; // средняя скорость число перехваченных пакетов в минуту
    countSendingPacket=0;
//    countPacketMustSended=0;// число дошедших пакетов
    //countPacketAnswered=0;// число ответивших пакетов
    //countOtherPacketAnswered=0;
   // countNotAnswered=0;
    qDebug()<<"Create new Port!";
   // connect(this,SIGNAL(stimeOut(QByteArray)),this,SLOT(timeOut(QByteArray)));


    enableRead=true;
    mik[0].workMode=WM_NONE;// режим работы не задан
    mik[0].transitionType=0;
    mik[0].solidGuns=0; // сплошная
    mik[0].brokenGuns=0; // прерывистая

    mik[0].markerPath=0;// расстояние от базовой линии до маркера в см
    mik[0].scanerParth=0; //  расстояние от базовой линии до Сканера в см
    mik[0].coefCorr=0; //  коэф коррекции цикла умнож. на 1000

     mik[0].lenGap=0; // длина пробела в см
     mik[0].lenMark=0; // длина штриха в см
     mik[0].lenToleranceGap=0; // длина пробела в см
     mik[0].lenToleranceMark=0; // длина штриха в см

     mik[1].workMode=WM_NONE;// режим работы не задан
     mik[1].transitionType=0;
     mik[1].solidGuns=0; // сплошная
     mik[1].brokenGuns=0; // прерывистая

     mik[1].markerPath=0;// расстояние от базовой линии до маркера в см
     mik[1].scanerParth=0; //  расстояние от базовой линии до Сканера в см
     mik[1].coefCorr=0; //  коэф коррекции цикла умнож. на 1000

     mik[1].lenGap=0; // длина пробела в см
     mik[1].lenMark=0; // длина штриха в см
     mik[1].lenToleranceGap=0; // длина пробела в см
     mik[1].lenToleranceMark=0; // длина штриха в см

    gyroX=0;
    gyroY=0;
    gyroZ=0;
    gyroSpeed=0;

    mboContrast=0;
    mboIznos=0;
    mboShum=0;
    mboQuality=0;

    rpmMixerL=0; rpmMixerR=0;
    rpmDanfossL=0;rpmDanfossR=0;

    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
        Paintguns[i]= new Guns();
    }

    for(int i=0;i<NUMBER_SENSOR_MOTION;i++)
    {
        sms[i]= new SensorMotion();
    }

    for(int i=0;i<NUMBER_SENSOR_DANFOSS;i++)
    {
        sensDanf[i]= new SensorDanfoss();
    }

    for(int i=0;i<ALL_TYPES_COUNT;i++)
    {
        mik[0].statMIK[i]= 0;
        mik[1].statMIK[i]= 0;
        statTerm[i]=0;
        statUBUP[i]=0;
        statMBO[i]=0;
    }
    statBCSchneider=0;
    statBCCanHub=0;

    for(int i=0;i<MIK_PAR_ALL_PARAMS;i++)
    {
        switch (i) {
        case MIK_PAR_SPEED:
         mik[0].mikBcParams[0][i]="Текущая скорость";
         mik[1].mikBcParams[0][i]="Текущая скорость";
            break;
        case MIK_PAR_STEP_M:
         mik[0].mikBcParams[0][i]="Длина тек. разм.,м";
         mik[1].mikBcParams[0][i]="Длина тек. разм.,м";
            break;
        case MIK_PAR_ALL_MARK_SM:
         mik[0].mikBcParams[0][i]="Сумм длина разм.,см";
         mik[1].mikBcParams[0][i]="Сумм длина разм.,см";
            break;
        case MIK_PAR_PUMP_CYCL_SM:
         mik[0].mikBcParams[0][i]="Длина на цикл помпы, см";
         mik[1].mikBcParams[0][i]="Длина на цикл помпы, см";
            break;
        case MIK_PAR_PUMP_CYCL:
         mik[0].mikBcParams[0][i]="К-во циклов помпы";
         mik[1].mikBcParams[0][i]="К-во циклов помпы";
            break;
        case MIK_PAR_MARK_SM:
         mik[0].mikBcParams[0][i]="Длина штриха.,см";
         mik[1].mikBcParams[0][i]="Длина штриха.,см";
            break;
        case MIK_PAR_GAP_SM:
         mik[0].mikBcParams[0][i]="Длина пробела.,см";
         mik[1].mikBcParams[0][i]="Длина пробела.,см";
            break;
        case MIK_PAR_STATUS:
         mik[0].mikBcParams[0][i]="Статус МИК";
         mik[1].mikBcParams[0][i]="Статус МИК";
            break;
        case MIK_PAR_PUMP_CYCL_SM_2:
         mik[0].mikBcParams[0][i]="Расст.за цикл помпы,см";
         mik[1].mikBcParams[0][i]="Расст.за цикл помпы,см";
            break;
        case MIK_PAR_MILEAGE_SM:
         mik[0].mikBcParams[0][i]="Пробег,см";
         mik[1].mikBcParams[0][i]="Пробег2,см";
            break;
        case MIK_PAR_CYCL:
         mik[0].mikBcParams[0][i]="К-во циклов";
         mik[1].mikBcParams[0][i]="К-во циклов";
            break;
        case MIK_PAR_MARK_TIME_S:
         mik[0].mikBcParams[0][i]="Время шага в секундах";
         mik[1].mikBcParams[0][i]="Время шага в секундах";
            break;
        case MIK_PAR_GUN_STATE:
         mik[0].mikBcParams[0][i]="Пистолеты";
         mik[1].mikBcParams[0][i]="Пистолеты";
            break;
        case MIK_PAR_SPEED_SENSOR:
         mik[0].mikBcParams[0][i]="Импульсы датчика скорости";
         mik[1].mikBcParams[0][i]="Импульсы датчика скорости";
            break;
        case MIK_PAR_GUN_STATE_2:
         mik[0].mikBcParams[0][i]="Пистолеты 2";
         mik[1].mikBcParams[0][i]="Пистолеты 2";
            break;
        case MIK_PAR_SPEED_SENSOR_2:
         mik[0].mikBcParams[0][i]="Импульсы датчика скорости все";
         mik[1].mikBcParams[0][i]="Импульсы датчика скорости все";
            break;

        default:
         mik[0].mikBcParams[0][i]="XXX";
            break;
        }

    mik[0].mikBcParams[1][i]="__";
    mik[1].mikBcParams[1][i]="__";
    }

    for(int i=0;i<MUK_PAR_ALL_PARAMS;i++)
    {
        switch (i) {
        case MUK_RPM_HYDRO_1:
         mik[0].mukBcParams[0][i]="Обороты гидромотора 1";
         mik[1].mukBcParams[0][i]="Обороты гидромотора 1";
            break;
        case MUK_RPM_HYDRO_2:
         mik[0].mukBcParams[0][i]="Обороты гидромотора 2";
         mik[1].mukBcParams[0][i]="Обороты гидромотора 2";
            break;
        case MUK_RPM_HYDRO_3:
         mik[0].mukBcParams[0][i]="Обороты гидромотора 3";
         mik[1].mukBcParams[0][i]="Обороты гидромотора 3";
            break;
        case MUK_RPM_HYDRO_4:
         mik[0].mukBcParams[0][i]="Обороты гидромотора 4";
         mik[1].mukBcParams[0][i]="Обороты гидромотора 4";
            break;
        case MUK_MODUL:
         mik[0].mukBcParams[0][i]="Технология";
         mik[1].mukBcParams[0][i]="Технология";
            break;
        case MUK_WORK_MODE:
         mik[0].mukBcParams[0][i]="Тип МДР";
         mik[1].mukBcParams[0][i]="Тип МДР";
            break;
        case MUK_RPM_MIXER:
         mik[0].mukBcParams[0][i]="Обороты миксера";
         mik[1].mukBcParams[0][i]="Обороты миксера";
            break;
        case MUK_ERRORS:
         mik[0].mukBcParams[0][i]="Ошибки";
         mik[1].mukBcParams[0][i]="Ошибки";
            break;
        case MUK_HARD_PERC_L:
         mik[0].mukBcParams[0][i]="Прот. отв. лев";
         mik[1].mukBcParams[0][i]="Прот. отв. лев";
            break;
        case MUK_HARD_PERC_R:
         mik[0].mukBcParams[0][i]="Прот. отв. пр";
         mik[1].mukBcParams[0][i]="Прот. отв. пр";
            break;
        case MUK_ADC_VOLT_1:
         mik[0].mukBcParams[0][i]="Значение АЦП 1";
         mik[1].mukBcParams[0][i]="Значение АЦП 1";
            break;
        case MUK_ADC_VOLT_2:
         mik[0].mukBcParams[0][i]="Значение АЦП 2";
         mik[1].mukBcParams[0][i]="Значение АЦП 2";
            break;
        case MUK_ADC_VOLT_3:
         mik[0].mukBcParams[0][i]="Значение АЦП 3";
         mik[1].mukBcParams[0][i]="Значение АЦП 3";
            break;
        case MUK_ADC_VOLT_4:
         mik[0].mukBcParams[0][i]="Значение АЦП 4";
         mik[1].mukBcParams[0][i]="Значение АЦП 4";
            break;

        default:
         mik[0].mukBcParams[0][i]="XXX";
         mik[1].mukBcParams[0][i]="XXX";
            break;
        }

    mik[0].mukBcParams[1][i]="__";
    mik[1].mukBcParams[1][i]="__";
    }
    QTimer *timerSpCalc = new QTimer();
    connect(timerSpCalc, SIGNAL(timeout()), this, SLOT(speedCalc()));
    timerSpCalc->setTimerType(Qt::PreciseTimer);
    timerSpCalc->start(100); // И запустим таймер рассчета скорости

      mik[0].mikStat=new MikStat[NUMBER_FOR_MIK_STATISTIC];
      mik[1].mikStat=new MikStat[NUMBER_FOR_MIK_STATISTIC];

    for (int i = 0; i < NUMBER_FOR_MIK_STATISTIC; i++) {
           // Заполнение массива и вывод значений его элементов
          mik[0].mikStat[i].number = i;
          mik[0].mikStat[i].type = 0;
          mik[0].mikStat[i].time = "00:00:00";
          mik[0].mikStat[i].width = 0;
          mik[0].mikStat[i].gunsSolid= 0;
          mik[0].mikStat[i].gunsBeads = 0;
          mik[0].mikStat[i].gunsBroke = 0;
          mik[0].mikStat[i].spaseSm = 0;
          mik[0].mikStat[i].strokeSm = 0;
          mik[0].mikStat[i].markCycle = 0;
          mik[0].mikStat[i].lenAllGunsSm = 0;
          mik[0].mikStat[i].lenStepSm = 0;
          mik[0].mikStat[i].pumpCycle = 0;
          mik[0].mikStat[i].averSpeed = 0;
          mik[0].mikStat[i].isGenerator = 0;

          mik[1].mikStat[i].number = i;
          mik[1].mikStat[i].type = 0;
          mik[1].mikStat[i].time = "00:00:00";
          mik[1].mikStat[i].width = 0;
          mik[1].mikStat[i].gunsSolid= 0;
          mik[1].mikStat[i].gunsBeads = 0;
          mik[1].mikStat[i].gunsBroke = 0;
          mik[1].mikStat[i].spaseSm = 0;
          mik[1].mikStat[i].strokeSm = 0;
          mik[1].mikStat[i].markCycle = 0;
          mik[1].mikStat[i].lenAllGunsSm = 0;
          mik[1].mikStat[i].lenStepSm = 0;
          mik[1].mikStat[i].pumpCycle = 0;
          mik[1].mikStat[i].averSpeed = 0;
          mik[1].mikStat[i].isGenerator = 0;
       }
    mik[0].mikStat[0].time = "-------";
    mik[1].mikStat[0].time = "-------";

    for (int i = 0; i < CanProt::ALL_HYDRO; i++) {
        hydroFists[i]=0;
        hydroRpm[i]=0;
        hydroDirection[i]=0;
        hydroMode[i]=0;
        hydroImpOnRpm[i]=0;
        hydroPID_P[i]=0;
        hydroPID_I[i]=0;
        hydroPID_D[i]=0;
       // hydroDelayOn[i]=0;
       // hydroDelayOff[i]=0;
    }
    for(int i = 0; i < 11; i++) {
    presetsSCREW[i]=0;
    }

    for(int i = 0; i < CanProt::NUM_VALVES_CP_REC; i++) {
        valvDelOn[i]=0;
        valvDelOff[i]=0;
    }

    pressure=0;
    setLeftRpm=0;
    setRightRpm=0;

    for(int i=0;i<STATUS_ALL;i++)
     countLocalBC[i]=0;
}

Port::~Port()
{
    qDebug("By in Thread!");
//    delete[] mikStat;
    emit finished_Port();
}

void Port :: process_Port(){
    qDebug("Port in Thread!");
    connect(&thisPort,SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
    connect(&thisPort, SIGNAL(readyRead()),this,SLOT(ReadInPort()));
}

void Port :: Write_Settings_Port(QString name, int baudrate,int DataBits,
                         int Parity,int StopBits, int FlowControl){
    SettingsPort.name = name;
    SettingsPort.baudRate = (QSerialPort::BaudRate) baudrate;
    SettingsPort.dataBits = (QSerialPort::DataBits) DataBits;
    SettingsPort.parity = (QSerialPort::Parity) Parity;
    SettingsPort.stopBits = (QSerialPort::StopBits) StopBits;
    SettingsPort.flowControl = (QSerialPort::FlowControl) FlowControl;
}

void Port :: ConnectPort(void){//
    thisPort.setPortName(SettingsPort.name);
    if (thisPort.open(QIODevice::ReadWrite)) {
        if (thisPort.setBaudRate(SettingsPort.baudRate)
                && thisPort.setDataBits(SettingsPort.dataBits)//DataBits
                && thisPort.setParity(SettingsPort.parity)
                && thisPort.setStopBits(SettingsPort.stopBits)
                && thisPort.setFlowControl(SettingsPort.flowControl))
        {
            if (thisPort.isOpen()){
                error_((SettingsPort.name+ " >> Opened!\r").toLocal8Bit());
            }
        } else {
            thisPort.close();
            error_(thisPort.errorString().toLocal8Bit());
          }
    } else {
        thisPort.close();
        error_(thisPort.errorString().toLocal8Bit());
    }
}

void Port::handleError(QSerialPort::SerialPortError error)//
{
    if ( (thisPort.isOpen()) && (error == QSerialPort::ResourceError)) {
        error_(thisPort.errorString().toLocal8Bit());
        DisconnectPort();
    }
}

qint16 Port::TwosComplementToRotate(quint8 LSB, quint8 MSB)
{
    uint16_t rotation=0;
    rotation=(MSB<<8)+LSB;
    if (!rotation) return 0; // если нулевое значение возвращае мноль оборотов
    if (rotation==0x8000) return  -32678; // если нулевое значение возвращаем ноль оборотов
    if (MSB>>7)  // если старший бит 1, значит обороты положительные
    {
    //rotation+=LSB;
    return (0x10000-rotation)>=25000 ? 2500 : (0x10000-rotation)/10;
    }
    else // иначе со знаком минус
    return  rotation>=25000 ? -2500 : -rotation/10;

}

void Port::setSensDanf(int adr, int rpm)
{

    //qDebug()<<"setSensDanf"<<adr<<rpm;
    // Проверяем, если адрес есть уже есть обновяем параметры
    for (int i = 0; i < NUMBER_SENSOR_DANFOSS; i++) {
       if(sensDanf[i]->addres == 0) break; // датчиков дальше нет
        if (sensDanf[i]->addres == adr) {
            // Адрес уже существует
            sensDanf[i]->activity=  5;
            sensDanf[i]->rpm=  rpm;
            sensDanf[i]->isVisible=true;
            sensDanf[i]->name=setNameSensDanf(adr);
            emit refrParams(ID_SENSOR_DANFOSS,i);
            return;
        }
    }

    // если адреса нет добавляем
    // Проверяем, если адрес есть уже есть обновяем параметры
    for (int i = 0; i < NUMBER_SENSOR_DANFOSS; i++) {
        if (sensDanf[i]->addres == 0) {
            // Адрес уже существует
            sensDanf[i]->addres=adr;
            sensDanf[i]->activity=  5;
            sensDanf[i]->rpm=  rpm;
            sensDanf[i]->isVisible=true;
            emit refrParams(ID_SENSOR_DANFOSS,i);
            return;
        }
    }




}

QString Port::setNameSensDanf(int adr)
{
QString nameSens="не распознан";


    switch (adr) {
    case 0x51:
        nameSens="Новый датчик";
        break;
    case 0x52:
        nameSens="М12 ТП: Шнек ХП:Лев.насос ";
        break;
    case 0x53:
        nameSens="М12 ТП: Насос спрей ХП:Прав.насос";
        break;
    case 0x54:
        nameSens="М12 ТП: Барабан ХП:Лев.миксер";
        break;
    case 0x55:
        nameSens="М12  ХП:Прав.миксер";
        break;

    case 0xC2:
        if(mukModul==MODULE_TP)
        {
            nameSens="М15 ТП: Шнек";
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов левого транспортера ";
        }
        else if(mukModul==MODULE_CP)
        {
          nameSens="М15 ХП:Лев.миксер ";
        }
        else  nameSens="М15 ТП: Шнек ХП:Лев.миксер ";
        break;
    case 0xC3:
        if(mukModul==MODULE_TP)
        {
            nameSens="М15 ТП: Насос спрей";
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов правого транспортера";
        }
        else if(mukModul==MODULE_CP)
        {
          nameSens="М15 ХП:Прав.миксер ";
        }
        else
        nameSens="М15 ТП: Насос спрей ХП:Прав.миксер";
        break;
    case 0xC4:
        if(mukModul==MODULE_TP)
        {
            nameSens="М15 ТП: Барабан";
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов левого бойлера мешалки ";
        }
        else if(mukModul==MODULE_CP)
        {
          nameSens="... ";
        }
        else
        nameSens="М15 ТП: Барабан";
        break;
    case 0xC5:
        if(mukModul==MODULE_TP)
        {
            nameSens="М15  TП: Меш. емкости";
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов правого бойлера мешалки ";
        }
        else if(mukModul==MODULE_CP)
        {
          nameSens="... ";
        }
        else
        nameSens="М15  TП: Меш. емкости";
        break;
    case 0xC6:
        if(mukModul==MODULE_TP)
        {
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов левого рециркуляционного насоса пластика ";
        }
        break;
    case 0xC7:
        if(mukModul==MODULE_TP)
        {
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов правого рециркуляционного насоса пластика ";
        }
        break;
    case 0xC9:
        if(mukModul==MODULE_TP)
        {
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов правого рециркуляционного насоса масла ";
        }
        break;
    case 0xCA:
        if(mukModul==MODULE_TP)
        {
            if (mik[mikAdr].isMagistal)  nameSens="Датчик оборотов левого рециркуляционного насоса масла";
        }
        break;

    default:
        break;
    }

    return nameSens;

}//


void  Port::DisconnectPort(){
    if(thisPort.isOpen()){
        thisPort.close();
        error_(SettingsPort.name.toLocal8Bit() + " >> Закрыт!\r");
    }
}
//ot tuta kovuratji!!!
void Port :: WriteToPort(QByteArray data){
    if(thisPort.isOpen()){
        thisPort.write(data);
    }
}

void Port::WriteToCanPort(QByteArray data)
{

    if(thisPort.isOpen()){
        data="T"+data+"\r";
        thisPort.write(data);
         countSendingPacket++;
      /*  timeoutTimer= new QTimer;
        timeoutTimer->setInterval(500);
        timeoutTimer->setSingleShot(false);
        QObject::connect(
                    timeoutTimer, SIGNAL(timeout()),
                    this, SLOT(timeOut()));
        timeoutTimer->start();*/
     //    QTimer::singleShot(250, this, SLOT(timeOut()));
         //SendsPacket.insert(data);
     qDebug()<<"WriteToCanPort!"<<countSendingPacket<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz")<<data;
     thisPort.waitForReadyRead(3);
    }

}


void Port :: workTimeout(){
//qDebug()<<"workTimeout;" <<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    int stateModuls=0;


    if(countLocalBC[STATUS_MIK_0]<mik[0].statMIK[NUM_BC])
        stateModuls|=1<<STATUS_MIK_0;
    countLocalBC[STATUS_MIK_0]=mik[0].statMIK[NUM_BC];

    if(countLocalBC[STATUS_MIK_1]<mik[1].statMIK[NUM_BC])
        stateModuls|=1<<STATUS_MIK_1;
    countLocalBC[STATUS_MIK_1]=mik[1].statMIK[NUM_BC];

    if(countLocalBC[STATUS_TERMINAL]<statTerm[NUM_BC])
        stateModuls|=1<<STATUS_TERMINAL;
    countLocalBC[STATUS_TERMINAL]=statTerm[NUM_BC];

    if(countLocalBC[STATUS_GYRO]<mik[0].statMIK[NUM_BC_GYRO]+mik[1].statMIK[NUM_BC_GYRO])
        stateModuls|=1<<STATUS_GYRO;
    countLocalBC[STATUS_GYRO]=mik[0].statMIK[NUM_BC_GYRO]+mik[1].statMIK[NUM_BC_GYRO];

    if(countLocalBC[STATUS_UBUP]<statUBUP[NUM_BC])
        stateModuls|=1<<STATUS_UBUP;
    countLocalBC[STATUS_UBUP]=statUBUP[NUM_BC];

    if(countLocalBC[STATUS_MBO]<statMBO[NUM_BC])
        stateModuls|=1<<STATUS_MBO;
    countLocalBC[STATUS_MBO]=statMBO[NUM_BC];

    if(countLocalBC[STATUS_SHNEIDER]<statBCSchneider)
        stateModuls|=1<<STATUS_SHNEIDER;
    countLocalBC[STATUS_SHNEIDER]=statBCSchneider;

    if(countLocalBC[STATUS_CANHUB]<statBCCanHub)
        stateModuls|=1<<STATUS_CANHUB;
    countLocalBC[STATUS_CANHUB]=statBCCanHub;

     //emit setstatePM(0x0F,0x01); //For test
    emit setModuleState(stateModuls);

}

void Port::ParseBroadCast(int command, int transmitter, quint8 *data)
{
    // qDebug()<<"parsing"<<   command<<transmitter<<data[0]<<data[1];
    int nMik=0;//номер МИК 0 или 1
    QString str="";
    if(transmitter==CAN_ADR_MIK){
        nMik=0;
    }else if(transmitter==CAN_ADR_MIK_2)
        nMik=1;

    switch (transmitter) {
    case CAN_ADR_MIK:
    case CAN_ADR_MIK_2:

        mik[nMik].statMIK[NUM_BC]++;
        if(command==BC_MIK_CUR1){
            mik[nMik].mikBcParams[1][MIK_PAR_SPEED]=QString::number((data[0]+(data[1]<<8))/10.0);
            mik[nMik].mikBcParams[1][MIK_PAR_STEP_M]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_ALL_MARK_SM]=QString::number(data[4]+(data[5]<<8)+(data[6]<<16)+(data[7]<<24));
            emit refrParams(ID_STATUS_MIK,1);
        }else   if(command==BC_MIK_CUR2){
            mik[nMik].mikBcParams[1][MIK_PAR_PUMP_CYCL_SM]=QString::number(data[0]+(data[1]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_PUMP_CYCL]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_MARK_SM]=QString::number(data[4]+(data[5]<<8));
            mik[nMik]. mikBcParams[1][MIK_PAR_GAP_SM]=QString::number(data[6]+(data[7]<<8));
             emit refrParams(ID_STATUS_MIK,2);
        }else if(command==BC_MIK_CUR3){
            mik[nMik].mikBcParams[1][MIK_PAR_STATUS]=QString::number(data[0]+(data[1]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_PUMP_CYCL_SM_2]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_MILEAGE_SM]=QString::number(data[4]+(data[5]<<8)+(data[6]<<16)+(data[7]<<24));

             emit refrParams(ID_STATUS_MIK,3);
        }
        else if(command==BC_MIK_CUR4){
            mik[nMik].mikBcParams[1][MIK_PAR_CYCL]=QString::number(data[0]+(data[1]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_MARK_TIME_S]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mikGunStateOld=data[4]+(data[5]<<8);
            mik[nMik].mikBcParams[1][MIK_PAR_GUN_STATE]="";
            for(int i=0;i<32;i++)
            {
                if((mik[nMik].mikGunStateOld>>i)&0x01)
                {
                    mik[nMik].mikBcParams[1][MIK_PAR_GUN_STATE].append(QString::number(i+1)+" ");
                }
            }
            //  mikBcParams[1][MIK_PAR_GUN_STATE]=QString::number(data[4]+(data[5]<<8));
            mik[nMik].mikBcParams[1][MIK_PAR_SPEED_SENSOR]=QString::number(data[6]+(data[7]<<8));

             emit refrParams(ID_STATUS_MIK,4);
        }
        else if(command==BC_MIK_CUR5){
            mik[nMik].mikBcParams[1][MIK_PAR_GUN_STATE_2]="";
            mik[nMik].mikGunState=data[0]+(data[1]<<8)+(data[2]<<16)+(data[3]<<24);
            for(int i=0;i<32;i++)
            {
                if((mik[nMik].mikGunState>>i)&0x01)
                {
                   mik[nMik].mikBcParams[1][MIK_PAR_GUN_STATE_2].append(QString::number(i+1)+" ");
                }
            }
            mik[nMik].mikBcParams[1][MIK_PAR_SPEED_SENSOR_2]=QString::number(data[4]+(data[5]<<8)+(data[6]<<16)+(data[7]<<24));
            emit refrParams(ID_STATUS_MIK,5);
        } else   if(command==BC_MIK_CUR6){
            mik[nMik].mukBcParams[1][MUK_RPM_HYDRO_1]=QString::number(data[0]+(data[1]<<8));
            mik[nMik].mukBcParams[1][MUK_RPM_HYDRO_2]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mukBcParams[1][MUK_RPM_HYDRO_3]=QString::number(data[4]+(data[5]<<8));
            mik[nMik].mukBcParams[1][MUK_RPM_HYDRO_4]=QString::number(data[6]+(data[7]<<8));
                    emit refrParams(ID_STATUS_MIK,6);

        }else   if(command==BC_MIK_CUR7){            
            mukModul=data[0];
            if(data[0]==0){
                mik[nMik].mukBcParams[1][MUK_MODUL]="Краска";
                if(data[1]==0) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Пистолеты";
                if(data[1]==1) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Спрей 98:2";

            }
            else if(data[0]==1) {

                /*
                 *
                 *     HP_NONE=0,
    HP_EXTRUDER=1,
    HP_MULTIDOT=2,
    HP_MAGISTRAL_L=3,   // левая сторона магистральной термопластик 10 кул
    HP_MAGISTRAL_SPRAY_L=4, // левая сторона магистральной спрей 2 кул
    HP_MAGISTRAL_R=5,   // правая сторона магистральной термопластик 6 кул мультидот
    HP_MAGISTRAL_SPRAY_R=6, // левая сторона магистральной спрей 2 кул
 HP_NONE*/

                mik[nMik].mukBcParams[1][MUK_MODUL]="Термопластик";
                if(data[1]==HP_NONE) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Нет";
                else if(data[1]==HP_EXTRUDER) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Экструдер";
                else if(data[1]==HP_MULTIDOT) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Мультидот";
                else if(data[1]==HP_MAGISTRAL_L) {mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Магистр.лев"; mik[nMik].isMagistal=true;}
                else if(data[1]==HP_MAGISTRAL_SPRAY_L){ mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Магистр. лев. спрей";mik[nMik].isMagistal=true;}
                else if(data[1]==HP_MAGISTRAL_R) {mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Магистр.прав";mik[nMik].isMagistal=true;}
                else if(data[1]==HP_MAGISTRAL_SPRAY_R) {mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Магистр.прав. спрей";mik[nMik].isMagistal=true;}
                else if(data[1]==HP_CLOSED_MB) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="З.Р.Б";
                else if(data[1]==HP_400_SPRAY) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="400 спрей";
                else mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Новый тип";
            }

            else if(data[0]==2){
                mik[nMik].mukBcParams[1][MUK_MODUL]="Холодный пластик";
                if(data[1]==0) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Обычный";
                else if(data[1]==1) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Рецеркуляция 1";
                else if(data[1]==2) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Рецеркуляция 2";
                else if(data[1]==3) mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Спрей ХП";
                else mik[nMik].mukBcParams[1][MUK_WORK_MODE]="Неизвестно";
            }
            else mik[nMik].mukBcParams[1][MUK_MODUL]="Неизвестно";
            mik[nMik].mukBcParams[1][MUK_RPM_MIXER]=QString::number(data[2]+(data[3]<<8));

            mik[nMik].mukBcParams[1][MUK_ERRORS]="";
            int16_t mask=data[4]+(data[5]<<8);
            //mask=0x03;
            if((mask>>0)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("Горелка;");
            if((mask>>1)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("СОГ1;");
            if((mask>>2)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("СОГ2;");
            if((mask>>3)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("СОГ3;");
            if((mask>>4)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("ОТВ.ЛЕВ;");
            if((mask>>5)&0x01)  mik[nMik].mukBcParams[1][MUK_ERRORS].append("ОТВ.ПР;");

            mik[nMik].mukBcParams[1][MUK_HARD_PERC_L]=QString::number(data[6]);
            mik[nMik].mukBcParams[1][MUK_HARD_PERC_R]=QString::number(data[7]);

           // mukBcParams[1][MUK_ERRORS]=QString::number(data[4]+(data[5]<<8));
             emit refrParams(ID_STATUS_MIK,7);
        }
        else   if(command==BC_MIK_CUR8){

            mik[nMik].mukBcParams[1][MUK_ADC_VOLT_1]=QString::number(data[0]+(data[1]<<8));
            mik[nMik].mukBcParams[1][MUK_ADC_VOLT_2]=QString::number(data[2]+(data[3]<<8));
            mik[nMik].mukBcParams[1][MUK_ADC_VOLT_3]=QString::number(data[4]+(data[5]<<8));
            mik[nMik].mukBcParams[1][MUK_ADC_VOLT_4]=QString::number(data[6]+(data[7]<<8));
        emit refrParams(ID_STATUS_MIK,8);
        }
           break;

    case CAN_ADR_GYRO:
        mik[0].statMIK[NUM_BC_GYRO]++;
        if(command==GYRO_DATE){
           gyroX=signed int(data[0]+(data[1]<<8));
           gyroY=signed int(data[2]+(data[3]<<8));
           gyroZ=signed int(data[4]+(data[5]<<8));
           gyroSpeed=signed int(data[6]+(data[7]<<8))/10;
           emit refrParams(ID_GYRO_XYZ,0);
          // emit refrParams(ID_STATUS_GYRO);
        }
        break;


    case CAN_ADR_TERMINAL:
        statTerm[NUM_BC]++;
        if(command==BC_PULT_TIME)
        {
          timeTerminal=QString::number(data[2]).rightJustified(2, '0')+":"+QString::number(data[1]).rightJustified(2, '0')+":"+QString::number(data[0]).rightJustified(2, '0')+"   "
                  +QString::number(data[3]).rightJustified(2, '0')+"_"+QString::number(data[4]).rightJustified(2, '0')+"_"+QString::number(data[5]).rightJustified(2, '0')
                  +" GMT:"+(((int8_t)data[6]>0)?"+":"")+QString::number((int8_t)data[6]);

        }
        if(command==BC_PULT_ADR_BLOCK)
        {
          pultAdrMik=data[0];
          pultAdrUBUP=data[1];
          pultAdrMBO=data[2];

        }
        emit refrParams(ID_STATUS_TERMINAL,0);
        break;

    case CAN_ADR_UBUP:
        statUBUP[NUM_BC]++;
        emit refrParams(ID_STATUS_UBUP,0);
        break;

    case CAN_ADR_MBO:
        statMBO[NUM_BC]++;
        emit refrParams(ID_STATUS_MBO,0);
        break;

    case CAN_ADR_SCHNEIDR:        
        statBCSchneider++;
      if(command==BC_PRESSURE)
      {
      pressure=data[0]+(data[1]<<8);
      emit refrParams(ID_PESSURE_SHNEIDER,0);
      }
        emit refrParams(ID_STATUS_SHNEIDER,0);
        break;
    case CAN_ADR_CANHUB:
        statBCCanHub++;
        emit refrParams(ID_STATUS_CANHUB,0);
        break;
    case CAN_ADR_DANFOSS_1:
        if(command==BC_DANF_RPM){
        rpmDanfossL=data[0]+(data[1]<<8);
        rpmDanfossR=data[2]+(data[3]<<8);
        emit refrParams(ID_RPM_DANFOSS,0);
        }

        break;

    case CAN_ADR_SMS:
    case CAN_ADR_SMS+1:
    case CAN_ADR_SMS+2:
    case CAN_ADR_SMS+3:
    case CAN_ADR_SMS+4:
    case CAN_ADR_SMS+5:
    case CAN_ADR_SMS+6:
    case CAN_ADR_SMS+7:
    case CAN_ADR_SMS+8:
    case CAN_ADR_SMS+9:
       if(command==0x01)
       {
           sms[transmitter-CAN_ADR_SMS]->isVisible=true;
           sms[transmitter-CAN_ADR_SMS]->activity=5;
           sms[transmitter-CAN_ADR_SMS]->motion=(data[0]>>7)&0x01;
           sms[transmitter-CAN_ADR_SMS]->setState((bool)((data[0]>>7)&0x01));
           sms[transmitter-CAN_ADR_SMS]->alarm=(data[0]>>0)&0x01;
           sms[transmitter-CAN_ADR_SMS]->errCan=(data[1]>>6)&0x01;
           sms[transmitter-CAN_ADR_SMS]->errEprom=(data[1]>>7)&0x01;
           emit refrParams(ID_SENSOR_MS,transmitter-CAN_ADR_SMS);
       }

        break;


    case CAN_ADR_CAN2CAN:
        if(command==0x01)
        {
            str="Режим работы: ";
            if((data[0]>>0)&0x01) str+="CAN1 в USB \n";
            if((data[0]>>1)&0x01) str+="CAN2 в USB \n";
            if((data[0]>>4)&0x01) str+="CAN1 в CAN2 \n";
            if((data[0]>>7)&0x01) str+="Передача статуса: включена\n";
            else "Передача статуса: отключена\n";
            str+="Нагрузка на CAN1: "+ QString::number(data[1]+(data[2]<<8))+" сообщений в секунду\n";
            str+="Нагрузка на CAN2: "+ QString::number(data[3]+(data[4]<<8))+" сообщений в секунду\n";
            emit refrParams(ID_BC_C2C,str);
        }
    default:
        break;
    }

}

void Port::ParseAnswerCommand(int command, int reciever, int transmitter, quint8 *data, int len)
{
    QString temp="";
    if((transmitter)==CAN_ADR_MIK && reciever==CAN_ADR_FAKE_TERMINAL
            && command==CIKL_MARKING_START && data[0]==0 && flagReadySendCycle==true)
    {
        mik[0].statMIK[NUM_ACK_GOOD]++;
        flagReadySendCycle=false;
        emit  readyStartCycle();
        return;
    }
    if((transmitter)==CAN_ADR_MIK_2 && reciever==CAN_ADR_FAKE_TERMINAL
            && command==CIKL_MARKING_START && data[0]==0 && flagReadySendCycle==true)
    {
        mik[1].statMIK[NUM_ACK_GOOD]++;
        flagReadySendCycle=false;
        emit  readyStartCycle();
        return;
    }



    switch (transmitter) {
    case CAN_ADR_MIK:
        if(data[0]==0 || command==MIK_READ_DATA || len==0) //исключение: чтение параметров
            mik[0].statMIK[NUM_ACK_GOOD]++;
        else
        {
            mik[0].statMIK[NUM_ACK_BAD]++;
        qDebug()<<"DATA_BAD="<<data[0];}
            emit refrParams(ID_MIK_ACK,0);

        if( data[0]==0 && command==CIKL_MARKING_START && len==2 )
        {
            temp="Мик принял " + QString::number(data[1]) + " пакетов";
            toPrintLOG(transmitter,reciever,0,temp,true);
        }

        break;

    case CAN_ADR_MIK_2:
        if(data[0]==0 || command==MIK_READ_DATA || len==0) //исключение: чтение параметров
            mik[1].statMIK[NUM_ACK_GOOD]++;
        else
        {
            mik[1].statMIK[NUM_ACK_BAD]++;
        qDebug()<<"DATA_BAD="<<data[0];}
            emit refrParams(ID_MIK_ACK,0);
        if( data[0]==0 && command==CIKL_MARKING_START && len==2  )
        {
            temp="Мик принял " + QString::number(data[1]) + " пакетов";
            toPrintLOG(transmitter,reciever,0,temp,true);
        }

        break;
    case CAN_ADR_TERMINAL:
        if(data[0]==0 || command==MIK_CMD_STATISTIC) // исключение: подтверждение приема статистики
            statTerm[NUM_ACK_GOOD]++;
        else
        {
            statTerm[NUM_ACK_BAD]++;
            //emit refrParams(ID_MIK_ACK_ERROR);
        }


        break;

    case CAN_ADR_MBO:
        if(len==3 && command==0x50 && reciever==CAN_ADR_TERMINAL) {
            mboContrast=data[2];
            mboShum=data[0];
            mboIznos=data[1];
            emit refrParams(ID_MBO_ANS_CMD,0);
        }

        break;

    case CAN_ADR_SMS:
    case CAN_ADR_SMS+1:
    case CAN_ADR_SMS+2:
    case CAN_ADR_SMS+3:
    case CAN_ADR_SMS+4:
    case CAN_ADR_SMS+5:
    case CAN_ADR_SMS+6:
    case CAN_ADR_SMS+7:
    case CAN_ADR_SMS+8:
    case CAN_ADR_SMS+9:
        if(len==8 && command==0x12 && reciever==CAN_ADR_TERMINAL) {
         //пришел ответ
           // qDebug()<<"пришел ответ"<<(data[2]<<8)+data[3]<<(data[4]<<8)+data[5]<<data[6];
            sms[transmitter-CAN_ADR_SMS]->numGuns=data[0];
            sms[transmitter-CAN_ADR_SMS]->onOff=(data[2]<<8)+data[3];
            sms[transmitter-CAN_ADR_SMS]->changeAdr=(data[4]<<8)+data[5];
            sms[transmitter-CAN_ADR_SMS]->version=data[6];
            emit refrParams(ID_SENSOR_MS,transmitter-CAN_ADR_SMS);
        }

        break;

    default:
        break;
    }
}

void Port::ParseEvent(int command, int transmitter, quint8 *data)
{

    switch (transmitter) {
    case CAN_ADR_MIK:

        mik[0].statMIK[NUM_EVENT]++;

        switch (command) {
        case EVENT_SWITCH_GUN:
            mik[0].textEvent.clear();
            if(mikAdr==0 && data[0]<ALL_GUNS_PAINT ){

            Paintguns[data[0]]->setState((bool)data[1]);
            }
            // qDebug()<<"переключен пистолет="<<data[0]<<"состояние="<<data[1]<<"Импульсов" << int(data[2]+(data[3]<<8)+(data[4]<<16)+(data[4]<<24))<<" Задержка вкл лед="<< data[6]<<" Задержка вsкл лед="<< data[7];
             //qDebug()<<data[0]<<data[1]<<data[2]<<data[3]<<data[4]<<data[5]<<data[6]<<data[7];
            mik[0].textEvent.append("Соб: перек. пист["+ QString::number(data[0])+"]= "+ QString::number(data[1])+", Имп:=" + QString::number(int(data[2]+(data[3]<<8)+(data[4]<<16)+(data[4]<<24)))
                    +" LED+="+QString::number(data[6])+" LED-="+ QString::number(data[7]));
            emit refrParams(ID_EVENT_MIK,mik[0].textEvent);
            break;



        case EVENT_SWITCH_OFF_GUN:
            mik[0].textEvent.clear();
           // qDebug()<<"выключен пистолет="<<data[0]<<" Длина штриха=" << int(data[2]+(data[3]<<8))<<" Расстояние="<< int(data[4]+(data[5]<<8));
            mik[0].textEvent.append("Соб: выкл. пист["+QString::number(data[0])+"], L штрих=" + QString::number(int(data[2]+(data[3]<<8)))+" L цикла="+QString::number(int(data[4]+(data[5]<<8))));

            emit refrParams(ID_EVENT_MIK,mik[0].textEvent);
            break;
        default:
            qDebug()<<"Неизветсное событие МИК"<<command;
            break;

        }


        break;

    case CAN_ADR_MIK_2:

        mik[1].statMIK[NUM_EVENT]++;

        switch (command) {
        case EVENT_SWITCH_GUN:
            mik[1].textEvent.clear();
            if(mikAdr==1){
            Paintguns[data[0]]->setState((bool)data[1]);
            }
            // qDebug()<<"переключен пистолет="<<data[0]<<"состояние="<<data[1]<<"Импульсов" << int(data[2]+(data[3]<<8)+(data[4]<<16)+(data[4]<<24))<<" Задержка вкл лед="<< data[6]<<" Задержка вsкл лед="<< data[7];
             //qDebug()<<data[0]<<data[1]<<data[2]<<data[3]<<data[4]<<data[5]<<data[6]<<data[7];
            mik[1].textEvent.append("Соб: перек. пист["+ QString::number(data[0])+"]= "+ QString::number(data[1])+", Имп:=" + QString::number(int(data[2]+(data[3]<<8)+(data[4]<<16)+(data[4]<<24)))
                    +" LED+="+QString::number(data[6])+" LED-="+ QString::number(data[7]));
            emit refrParams(ID_EVENT_MIK,mik[1].textEvent);
            break;



        case EVENT_SWITCH_OFF_GUN:
            mik[1].textEvent.clear();
           // qDebug()<<"выключен пистолет="<<data[0]<<" Длина штриха=" << int(data[2]+(data[3]<<8))<<" Расстояние="<< int(data[4]+(data[5]<<8));
            mik[1].textEvent.append("Соб: выкл. пист["+QString::number(data[0])+"], L штрих=" + QString::number(int(data[2]+(data[3]<<8)))+" L цикла="+QString::number(int(data[4]+(data[5]<<8))));

            emit refrParams(ID_EVENT_MIK,mik[1].textEvent);
            break;
        default:
            qDebug()<<"Неизветсное событие МИК"<<command;
            break;

        }


        break;

    case CAN_ADR_TERMINAL:
        statTerm[NUM_EVENT]++;
        break;

    case CAN_ADR_CAN2CAN:
        if(command==0x02)
        {
           emit refrParams(ID_EVENT_C2C,0);
        }

        break;
    default:
        break;
    }

}

void Port::ParseData(int command, int reciever,int transmitter, quint8 *data)
{    
    QString temp="";
    switch (transmitter) {
    case CAN_ADR_MIK:
        mik[0].statMIK[NUM_DATA]++;
        //  qDebug()<<"ParseData";




        if(command==DATA_MIK){
            if((data[0]+(data[1]<<8))==MIK_DATA_PERC_OFF)
            {
                percOff=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
                temp="Читаем LED_0FF= "+QString::number(data[2])+" %";
                toPrintLOG(transmitter,reciever,0,temp,true);
            }

            if((data[0]+(data[1]<<8))==MIK_DATA_LR)   lr=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LMARK)   lmark=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_PERC_DET)   {
                percDet=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
                temp="Читаем LED_0N= "+QString::number(data[2])+" %";
                toPrintLOG(transmitter,reciever,0,temp,true);
            }
            if((data[0]+(data[1]<<8))==MIK_DATA_LIMIT_STROKE)   limStroke=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LIMIT_SPASE)   limSpace=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_COUNTER_SPEED)   counterSpeed=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            emit refrParams(ID_DATA_MIK,0);
        }
        if(command==DATA_MIK_STAT){
           // qDebug()<<"Get STAT"<<counterMikStat;
            if(data[0]==0x00)
            {
                mik[0].mikStat[mik[0].counterMikStat+1].gunsSolid= data[1]+(data[2]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].gunsBroke= data[3]+(data[4]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].gunsBeads= data[5]+(data[6]<<8);
            }else if(data[0]==0x01)
            {
                mik[0].mikStat[mik[0].counterMikStat+1].spaseSm= data[1]+(data[2]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].strokeSm= data[3]+(data[4]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].markCycle= data[5]+(data[6]<<8);//

                mik[0].mikStat[0].markCycle += data[5]+(data[6]<<8);//общий

            }else if(data[0]==0x02)  {
                mik[0].mikStat[mik[0].counterMikStat+1].lenAllGunsSm= data[1]+(data[2]<<8)+(data[3]<<16)+(data[4]<<24);
                mik[0].mikStat[mik[0].counterMikStat+1].lenStepSm= data[5]+(data[6]<<8)+(data[7]<<16);

                mik[0].mikStat[0].lenAllGunsSm+= data[1]+(data[2]<<8)+(data[3]<<16)+(data[4]<<24);
                mik[0].mikStat[0].lenStepSm+= data[5]+(data[6]<<8)+(data[7]<<16);


            }else if(data[0]==0x03){
                mik[0].mikStat[mik[0].counterMikStat+1].pumpCycle= data[1]+(data[2]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].averSpeed= data[3]+(data[4]<<8);

                mik[0].mikStat[0].pumpCycle+= data[1]+(data[2]<<8);
                mik[0].mikStat[mik[0].counterMikStat+1].isGenerator= data[5];


            }
            if(data[0]==0x04)
            {
                mik[0].mikStat[mik[0].counterMikStat+1].type= data[1];
                mik[0].mikStat[mik[0].counterMikStat+1].width= data[3];
                mik[0].mikStat[mik[0].counterMikStat+1].time= QDateTime::currentDateTime().toString("hh:mm:ss.zzz");

                mik[0].counterMikStat++;
                emit refrParams(ID_DATA_MIK_STATISTIC,0);
            }

        }
        break;
    case CAN_ADR_MIK_2:
        mik[1].statMIK[NUM_DATA]++;
        //  qDebug()<<"ParseData";
        if(command==DATA_MIK){
            if((data[0]+(data[1]<<8))==MIK_DATA_PERC_OFF)   percOff=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LR)   lr=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LMARK)   lmark=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_PERC_DET)   percDet=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LIMIT_STROKE)   limStroke=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_LIMIT_SPASE)   limSpace=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            if((data[0]+(data[1]<<8))==MIK_DATA_COUNTER_SPEED)   counterSpeed=  data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
            emit refrParams(ID_DATA_MIK,0);
        }
        if(command==DATA_MIK_STAT){
           // qDebug()<<"Get STAT"<<counterMikStat;
            if(data[0]==0x00)
            {
                mik[1].mikStat[mik[1].counterMikStat+1].gunsSolid= data[1]+(data[2]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].gunsBroke= data[3]+(data[4]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].gunsBeads= data[5]+(data[6]<<8);
            }else if(data[0]==0x01)
            {
                mik[1].mikStat[mik[1].counterMikStat+1].spaseSm= data[1]+(data[2]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].strokeSm= data[3]+(data[4]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].markCycle= data[5]+(data[6]<<8);//

                mik[1].mikStat[0].markCycle += data[5]+(data[6]<<8);//общий

            }else if(data[0]==0x02)  {
                mik[1].mikStat[mik[1].counterMikStat+1].lenAllGunsSm= data[1]+(data[2]<<8)+(data[3]<<16)+(data[4]<<24);
                mik[1].mikStat[mik[1].counterMikStat+1].lenStepSm= data[5]+(data[6]<<8)+(data[7]<<16);

                mik[1].mikStat[0].lenAllGunsSm+= data[1]+(data[2]<<8)+(data[3]<<16)+(data[4]<<24);
                mik[1].mikStat[0].lenStepSm+= data[5]+(data[6]<<8)+(data[7]<<16);


            }else if(data[0]==0x03){
                mik[1].mikStat[mik[1].counterMikStat+1].pumpCycle= data[1]+(data[2]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].averSpeed= data[3]+(data[4]<<8);

                mik[1].mikStat[0].pumpCycle+= data[1]+(data[2]<<8);
                mik[1].mikStat[mik[1].counterMikStat+1].isGenerator= data[5];


            }
            if(data[0]==0x04)
            {
                mik[1].mikStat[mik[1].counterMikStat+1].type= data[1];
                mik[1].mikStat[mik[1].counterMikStat+1].width= data[3];
                mik[1].mikStat[mik[1].counterMikStat+1].time= QDateTime::currentDateTime().toString("hh:mm:ss.zzz");

                mik[1].counterMikStat++;
                emit refrParams(ID_DATA_MIK_STATISTIC,1);
            }

        }
        break;
    case CAN_ADR_TERMINAL:
        statTerm[NUM_DATA]++;
        break;

    case CAN_ADR_MBO:
        if( (data[0]+(data[1]<<8))==0x04) {
            mboQuality=data[2]+(data[3]<<8);
            emit refrParams(ID_MBO_ANS_QUALITY,0);
            temp="Читаем качество= "+QString::number(data[2]);
            toPrintLOG(transmitter,reciever,0,temp,true);
        }
        if( (data[0]+(data[1]<<8))==0x09) {

            temp="Читаем допуск ширины в минус= "+QString::number(data[2])+" мм";
            toPrintLOG(transmitter,reciever,0,temp,true);
        }

        break;
}


    //Датчики оборотов Данфосс
    if(command==0xFF && reciever==0x20)
    {
        setSensDanf(transmitter,TwosComplementToRotate(data[3],data[4]));
        // qDebug()<<"Датчик "<< transmitter;
    }

    switch (transmitter) {
    case CAN_ADR_MIX_L:
            //rpmMixerL=(data[3]+(data[4]<<8))/10;
            rpmMixerL=TwosComplementToRotate(data[3],data[4]);
            emit refrParams(ID_RPM_MIX_L,0);
        break;

    case CAN_ADR_MIX_R:
            //rpmMixerR=(data[3]+(data[4]<<8))/10;
        rpmMixerR=TwosComplementToRotate(data[3],data[4]);
        emit refrParams(ID_RPM_MIX_R,0);
        break;
    }



}

void Port::ParseCommand(int command,int reciever, int transmitter, quint8 *data)
{
    QString temp="";
    int nMik=0;//номер МИК 0 или 1

    int ciklId=data[0]+(data[1]<<8); //идентефикатор переменной
    switch (transmitter) {
    case CAN_ADR_MIK:
        mik[0].statMIK[NUM_CMD]++;
        break;
    case CAN_ADR_MIK_2:
        mik[1].statMIK[NUM_CMD]++;
        break;
    case CAN_ADR_MBO:
        statMBO[NUM_CMD]++;
        switch (command) {
        case MBO_CORRIDIR:
            leftcoord=data[1]+(data[2]<<8);
            rightcoord=data[3]+(data[4]<<8);
            emit refrParams(ID_MBO_CORRIDOR,0);
            break;
        default:
            break;
        }

        break;
    case CAN_ADR_TERMINAL:
        statTerm[NUM_CMD]++;

        if(reciever==CAN_ADR_MIK){
            nMik=0;
        }else if(reciever==CAN_ADR_MIK_2)
            nMik=1;
        if(reciever==CAN_ADR_MIK||reciever==CAN_ADR_MIK_2)
        switch (command) {
        case CIKL_MARKING_START:
            if(mikAdr==nMik){
                for(int i=0;i<ALL_GUNS_PAINT;i++)
                {

                    Paintguns[i]->beadsMask=0;
                    Paintguns[i]->beadsguns.isVisible=false;
                    Paintguns[i]->setLisGuns();
                }
            }

            if( data[0]==2)
            {
                temp="П готов к передачи " + QString::number(data[1]) + " пакетов";
                toPrintLOG(transmitter,reciever,0,temp,true);
            }
            break;

        case CIKL_MARKING:
            switch (ciklId) {
            case CIKL_MODE:
                mik[nMik].workMode=data[2];
                mik[nMik].transitionType=data[3];
                mik[nMik].solidGuns=data[4]+(data[5]<<8);
                mik[nMik].brokenGuns= data[6]+(data[7]<<8);
                mik[nMik].workGuns=mik[nMik].solidGuns|mik[nMik].brokenGuns;
                //qDebug()<<"CIKL_MODE";
                temp="Режим: ";
                switch(mik[nMik].workMode){
                case 0: temp.append("Авто "); break;
                case 1: temp.append("Полуавтомат "); break;
                case 2: temp.append("Ручной "); break;
                case 3: temp.append("Как есть "); break;
                default: temp.append(QString::number(mik[nMik].workMode)); break;
                }

                temp.append("Переход: ");
                temp.append(QString::number(mik[nMik].transitionType));
                temp.append(" Слошн: ");
                temp.append(QString::number(mik[nMik].solidGuns));
                temp.append(" Прерыв: ");
                temp.append(QString::number(mik[nMik].brokenGuns));
                toPrintLOG(transmitter,reciever,0,temp,true);
                if(mikAdr==0){
                    for(int i=0;i<ALL_GUNS_PAINT;i++)
                    {
                        if((mik[nMik].workGuns>>i)&0x01)
                        {
                            Paintguns[i]->lisGuns.isVisible=true;
                        }
                        else
                            Paintguns[i]->lisGuns.isVisible=false;

                        /* Paintguns[i]->beadsMask=0;
                   Paintguns[i]->beadsguns.isVisible=false;
                   Paintguns[i]->setLisGuns();*/
                    }
                }

                emit refrParams(ID_WORK_MODE,0);
                break;

            case BEADS_PARAMS:
            case BEADS_PARAMS+1:
            case BEADS_PARAMS+2:
            case BEADS_PARAMS+3:
            case BEADS_PARAMS+4:
            case BEADS_PARAMS+5:
            case BEADS_PARAMS+6:
            case BEADS_PARAMS+7:
            case BEADS_PARAMS+8:
            case BEADS_PARAMS+9:
            case BEADS_PARAMS+0x0A:
            case BEADS_PARAMS+0x0B:
            case BEADS_PARAMS+0x0C:
            case BEADS_PARAMS+0x0D:
            case BEADS_PARAMS+0x0E:
            case BEADS_PARAMS+0x0F:
                //  qDebug()<<"commandBEADS_PARAMS"<<ciklId<<data[2]<<data[3]<<data[4];
                if(mikAdr==0){
                    Paintguns[ciklId-BEADS_PARAMS]->beadsguns.Ti=data[2];
                    Paintguns[ciklId-BEADS_PARAMS]->beadsguns.To=data[3];
                    Paintguns[ciklId-BEADS_PARAMS]->beadsguns.Base=data[4];
                    Paintguns[ciklId-BEADS_PARAMS]->type=GUN_TYPE_BEADS;
                    Paintguns[ciklId-BEADS_PARAMS]->beadsguns.isVisible=true;
                }

                emit refrParams(ID_BEADS_PAR,0);
                break;

            case COR_LIS_PARAMS:
            case COR_LIS_PARAMS+1:
            case COR_LIS_PARAMS+2:
            case COR_LIS_PARAMS+3:
            case COR_LIS_PARAMS+4:
            case COR_LIS_PARAMS+5:
            case COR_LIS_PARAMS+6:
            case COR_LIS_PARAMS+7:
            case COR_LIS_PARAMS+8:
            case COR_LIS_PARAMS+9:
            case COR_LIS_PARAMS+0x0A:
            case COR_LIS_PARAMS+0x0B:
            case COR_LIS_PARAMS+0x0C:
            case COR_LIS_PARAMS+0x0D:
            case COR_LIS_PARAMS+0x0E:
            case COR_LIS_PARAMS+0x0F:
                //  qDebug()<<"command"<<ciklId<<data[2]<<data[3];
                if(mikAdr==0){
                    Paintguns[ciklId-COR_LIS_PARAMS]->lisGuns.S2=data[2];
                    Paintguns[ciklId-COR_LIS_PARAMS]->lisGuns.R2=data[3];
                    Paintguns[ciklId-COR_LIS_PARAMS]->lisGuns.Ti=data[4]+(data[5]<<8);
                    Paintguns[ciklId-COR_LIS_PARAMS]->lisGuns.To=data[6]+(data[7]<<8);
                    Paintguns[ciklId-COR_LIS_PARAMS]->type=GUN_TYPE_LIS;
                    Paintguns[ciklId-COR_LIS_PARAMS]->setLisGuns();
                }
                emit refrParams(ID_LIS_CORR,0);

                break;

            case COR_GUNS_PARAMS:
            case COR_GUNS_PARAMS+1:
            case COR_GUNS_PARAMS+2:
            case COR_GUNS_PARAMS+3:
            case COR_GUNS_PARAMS+4:
            case COR_GUNS_PARAMS+5:
            case COR_GUNS_PARAMS+6:
            case COR_GUNS_PARAMS+7:
            case COR_GUNS_PARAMS+8:
            case COR_GUNS_PARAMS+9:
            case COR_GUNS_PARAMS+0x0A:
            case COR_GUNS_PARAMS+0x0B:
            case COR_GUNS_PARAMS+0x0C:
            case COR_GUNS_PARAMS+0x0D:
            case COR_GUNS_PARAMS+0x0E:
            case COR_GUNS_PARAMS+0x0F:
             //   qDebug()<<"command"<<ciklId<<data[2]<<data[3];
                if(mikAdr==0){
                    Paintguns[ciklId-COR_GUNS_PARAMS]->lisGuns.Ti=data[2]+(data[3]<<8);
                    Paintguns[ciklId-COR_GUNS_PARAMS]->type=GUN_TYPE_SIMPLE;
                    Paintguns[ciklId-COR_GUNS_PARAMS]->setSimpleGuns();
                }
                emit refrParams(ID_LIS_CORR,0);
                break;

            case GUNS_SETUP:
            case GUNS_SETUP+1:
            case GUNS_SETUP+2:
            case GUNS_SETUP+3:
            case GUNS_SETUP+4:
            case GUNS_SETUP+5:
            case GUNS_SETUP+6:
            case GUNS_SETUP+7:
            case GUNS_SETUP+8:
            case GUNS_SETUP+9:
            case GUNS_SETUP+0x0A:
            case GUNS_SETUP+0x0B:
            case GUNS_SETUP+0x0C:
            case GUNS_SETUP+0x0D:
            case GUNS_SETUP+0x0E:
            case GUNS_SETUP+0x0F:
                //qDebug()<<"command"<<ciklId<<data[2]<<data[3];
                if(mikAdr==0){
                    Paintguns[ciklId-GUNS_SETUP]->lisGuns.beads=data[2]+(data[3]<<8);
                    Paintguns[ciklId-GUNS_SETUP]->lisGuns.Base=data[4];
                    Paintguns[ciklId-GUNS_SETUP]->beadsMask= Paintguns[ciklId-GUNS_SETUP]->beadsMask|(data[2]+(data[3]<<8));
                    for(int i=0;i<16;i++)
                    {
                        if(( Paintguns[ciklId-GUNS_SETUP]->beadsMask>>i)&0x01)
                        {
                            Paintguns[i]->beadsguns.isVisible=true;
                            Paintguns[i]->setBeadsGuns();
                            emit refrParams(ID_WORK_MODE,0);
                            // qDebug()<<"Set beadsguns.isVisible" <<i;
                        }
                    }
                }

                emit refrParams(ID_GUNS_SETUP,0);
                break;


            case CONF_BASE:

                mik[nMik].markerPath=data[2]+(data[3]<<8);
                mik[nMik].scanerParth=data[4]+(data[5]<<8);
                mik[nMik].coefCorr= data[6]+(data[7]<<8);
                //qDebug()<<"CONF_BASE";
                emit refrParams(ID_CONF_BASE,0);
                break;

            case CONF_MODUL:

                workModul=data[2];
                speedSensorType=data[3];
                speedSensorCoef=data[4]+(data[5]<<8);
                lineTypeCP=data[6];
                distBetweenDots=data[7];


                emit refrParams(ID_CONF_MODUL,0);
                break;
            case CONF_HYDRO_PID1://  Коэф. пид для гидромотора
                hydroType=data[2];
                hydroPID_P[hydroType]=data[3]+(data[4]<<8);
                hydroPID_I[hydroType]=data[5]+(data[6]<<8);
                //                hydroFists[hydroType]=data[7];
                //                hydroRpm[hydroType]=data[5]+(data[6]<<8);

                emit refrParams(ID_SETUP_HYDRO_1,hydroType);
                break;

            case CONF_HYDRO_PID2://  Коэф. пид для гидромотора
                hydroType=data[2];
                hydroPID_D[hydroType]=data[3]+(data[4]<<8);
                hydroImpOnRpm[hydroType]=data[5];
                //                hydroFists[hydroType]=data[7];
                //                hydroRpm[hydroType]=data[5]+(data[6]<<8);

                emit refrParams(ID_SETUP_HYDRO_2,hydroType);
                break;
            case CONF_HYDRO_RPM:
                hydroType=data[2];
                hydroMode[hydroType]=data[3];
                hydroDirection[hydroType]=data[4];
                hydroFists[hydroType]=data[7];
                hydroRpm[hydroType]=data[5]+(data[6]<<8);
                qDebug()<<hydroType <<hydroRpm[hydroType];
                if(hydroType==SCREW && (hydroMode[SCREW]==HYD_SSPID || hydroMode[SCREW]==HYD_PWM)  )   presetsSCREW[data[7]]=data[5]+(data[6]<<8);
                emit refrParams(ID_SETUP_HYDRO,hydroType);
                break;

            case CONF_DELAYS_CP:
                valvDelOn[data[2]]=data[3]+(data[4]<<8);
                valvDelOff[data[2]]=data[5]+(data[6]<<8);
                emit refrParams(ID_DELAYS_CP,data[2]);
                break;


            case CONF_CYCLE:

                mik[nMik].lenGap=data[2]+(data[3]<<8);
                mik[nMik].lenMark=data[4]+(data[5]<<8);
                mik[nMik].lenToleranceGap= data[6];
                mik[nMik].lenToleranceMark=data[7];
                //qDebug()<<"CONF_BASE";
                emit refrParams(ID_CONF_CYCLE,0);
                break;

            case CONF_FOR_STATISTIC:

                mik[nMik].standart= data[2]+(data[3]<<8)+(data[4]<<16)+(data[5]<<24);
                mik[nMik].modul=data[6];
                mik[nMik].setWidth=data[4];
                //qDebug()<<data[2]<<data[3]<<data[4]<<data[5];
                temp="Стат-кa. Условия: ";
                switch(data[3]){
                /*CENTRE_CITY       = 0,      // Осевая <60 км/ч
               CENTRE_HIGHWAY,              // Осевая >60 км/ч
               EDGE,                        // Краевая
               FREE_MODE,                   // Свободный режим*/
                case 0: temp.append("Город"); break;
                case 1: temp.append("Трасса"); break;
                case 2: temp.append("Краевая"); break;
                case 3: temp.append("Свободный"); break;
                default: temp.append(QString::number(data[3])); break;
                }
                temp.append(" Режим: ");
                switch(data[5]){
                /*  WM_AUT0=0,
               WM_SEMI,
               WM_MANUAL,
               WM_AS_IS,
               WM_SEMI_LIS,
               WM_AUTO_LIS*/
                case 0: temp.append("Авто"); break;
                case 1: temp.append("Полуавтомат"); break;
                case 2: temp.append("Ручной"); break;
                case 3: temp.append("Как есть"); break;
                default: temp.append(QString::number(data[5])); break;
                }
                temp.append(" Ширина:"+QString::number(data[4])) ;
                temp.append(" Номер ID:"+QString::number(data[2])) ;

                temp.append(" Модуль: ");
                switch(data[6]){
                /*  Маски модулей:
               0х01 – задний модуль;
               0х02 – левый модуль;
               0х04 – передний модуль;
               0х08 – правый модуль.*/
                case 0x01: temp.append("Задний"); break;
                case 0x02: temp.append("Левый"); break;
                case 0x04: temp.append("Передний"); break;
                case 0x08: temp.append("Правый"); break;
                default: temp.append(QString::number(data[6])); break;
                }
                toPrintLOG(transmitter,reciever,0,temp,true);

                emit refrParams(ID_FOR_STATISTIC,0);
                break;


            default:
                break;
            }
            // qDebug()<<"CIKL_MARKING"<<data[0]<<data[1];
            break;
        case CONF_DIRECT:
            qDebug()<<"CIKL_MARKING"<<data[0]<<data[1];
            if(mikAdr==0){
                Paintguns[data[0]]->lisGuns.S2=data[1];
                Paintguns[data[0]]->lisGuns.R2=data[2];
                Paintguns[data[0]]->lisGuns.Ti=data[3]+(data[4]<<8);
                Paintguns[data[0]]->lisGuns.To=data[5]+(data[6]<<8);
            }
            emit refrParams(ID_LIS_CORR,0);
            break;
        case MIK_CMD:
            //  qDebug()<<"MIK_CMD"<<data[0]<<data[1];
            mik[nMik].textCommand.clear();
            switch(data[0])
            {
            case MIK_STOP :
                mik[nMik].textCommand.append("МИК0 СТОП!");
                temp="НАЖАЛИ СТОП";
                toPrintLOG(transmitter,reciever,0,temp,true);
                break;
            case MIK_START :
                mik[nMik].textCommand.append("МИК0 СТAРT!");
                temp="НАЖАЛИ СТАРТ";
                toPrintLOG(transmitter,reciever,0,temp,true);
                break;
            case MIK_SPEED_GEN :
                mik[nMik].textCommand.append("Генератор скорости ");
                if(data[1]==0)  mik[nMik].textCommand.append("выкл ");
                if(data[1]==1)  mik[nMik].textCommand.append("вкл ");
                mik[nMik].textCommand.append(QString::number((data[2]+(data[3]<<8)*10)));
                temp="Генератор скорости" + QString::number((data[2]+(data[3]<<8)*10));
                toPrintLOG(transmitter,reciever,0,temp,true);
                break;
            case MIK_START_RELEASE :
                mik[nMik].textCommand.append("Кнопка старт отпущена!");
                temp="ОТПУСТИЛИ СТАРТ";
                toPrintLOG(transmitter,reciever,0,temp,true);

                break;
            case MIK_PHASE :
                mik[nMik].textCommand.append("Зад. штриха см: ");
                mik[nMik].textCommand.append(QString::number((data[1]+(data[2]<<8)*10)));
                mik[nMik].textCommand.append(" Длина штриха см: ");
                mik[nMik].textCommand.append(QString::number((data[3]+(data[4]<<8)*10)));
                break;
            case MIK_PHOTO_SENS :
                mik[nMik].textCommand.append("Фотодатчик: ");
                if(data[1]==0)  mik[nMik].textCommand.append("выкл ");
                if(data[1]==1)  mik[nMik].textCommand.append("вкл ");
                if(data[1]==2)  mik[nMik].textCommand.append("тест ");
                break;

            case MIK_CLEAN :
                mik[nMik].textCommand.append("Прочистка: ");
                if(data[1]==0)  mik[nMik].textCommand.append("выкл ");
                if(data[1]==1)  mik[nMik].textCommand.append("вкл ");
                break;

            case MIK_WASHING :
                mik[nMik].textCommand.append("Промывка: ");
                washingType=data[1];
                emit refrParams(ID_WASHING_MIK,0);
                if(data[1]==0)  mik[nMik].textCommand.append("Отключена ");
                if(data[1]==1)  mik[nMik].textCommand.append("левый пистолет ");
                if(data[1]==2)  mik[nMik].textCommand.append("правый пистолет ");
                if(data[1]==3)  mik[nMik].textCommand.append("два пистолета ");
                if(data[1]==4)  mik[nMik].textCommand.append("левая линия пластика ");
                if(data[1]==5)  mik[nMik].textCommand.append("правая линия пластика ");
                if(data[1]==6)  mik[nMik].textCommand.append("левая линия отв-ля ");
                if(data[1]==7)  mik[nMik].textCommand.append("правая линия отв-ля");
                break;

            case MIK_SOURCE_COORD :
                mik[nMik].textCommand.append("Источник координат: ");
                if(data[1]==0)  mik[nMik].textCommand.append("не обновл. ");
                if(data[1]==1)  mik[nMik].textCommand.append("CAN Протокол ");
                if(data[1]==2)  mik[nMik].textCommand.append("Манипулятор");
                break;

            case MIK_LINE_TYPE :
                mik[nMik].textCommand.append("Тип линии: ");
                if(data[1]==0)  mik[nMik].textCommand.append("Нет линий ");
                if(data[1]==1)  mik[nMik].textCommand.append("Одиночная штриховая ");
                if(data[1]==2)  mik[nMik].textCommand.append("Одиночная сплоная ");
                if(data[1]==3)  mik[nMik].textCommand.append("Двойная, штрих справа ");
                if(data[1]==4)  mik[nMik].textCommand.append("Двойная, штрих слева ");
                if(data[1]==5)  mik[nMik].textCommand.append("Двойная сплошная ");
                if(data[1]==6)  mik[nMik].textCommand.append("Двойная штриховая ");
                if(data[1]==7)  mik[nMik].textCommand.append("Тройная сплошная ");
                if(data[1]==8)  mik[nMik].textCommand.append("Тройная штрих центр");
                if(data[1]==9)  mik[nMik].textCommand.append("Тройная штрих края ");
                break;
            case MIK_15_GUNS :
                mik[nMik].textCommand.append("Вывод скорости на 15 пист: ");
                if(data[1]==0)  mik[nMik].textCommand.append("выкл ");
                if(data[1]==1)  mik[nMik].textCommand.append("вкл ");
                break;
            default:  mik[nMik].textCommand.append("неизвестн команда: "+ QString::number(data[0]));

            }
            emit refrParams(ID_COMMAND_MIK,mik[nMik].textCommand);
            break;

        case  MIK_CMD_WRITE_DATA_EEP:
            if(data[0]==0x00)
            {
                temp="Установить LED_0FF= "+QString::number(data[2])+" %";
                toPrintLOG(transmitter,reciever,0,temp,true);
            }
            if(data[0]==0x04)
            {
                temp="Установить LED_0N= "+QString::number(data[2])+" %";
                toPrintLOG(transmitter,reciever,0,temp,true);
            }

        default:
            break;
        }

        if(reciever==CAN_ADR_MBO)
           switch (command) {
               case MIK_CMD_WRITE_DATA_EEP://0xDD
               if(data[0]==0x04)
             {  emit refrParams(ID_SET_QUAL,data[2]);
               temp="Установить  качество= "+QString::number(data[2]);
               toPrintLOG(transmitter,reciever,0,temp,true);
               }
               if(data[0]==0x09)
               {
                   temp="Уст-ть допуск шир.линии в минус ="+QString::number(data[2]);
                   toPrintLOG(transmitter,reciever,0,temp,true);
               }

               break;

           }

        break;

    case CAN_ADR_SCHNEIDR:
        if(reciever==CAN_ADR_DANFOSS_1 && command==SCHN_SET_RPM)
        {
            if(data[0]==0x01 || data[0]==0x0A ||data[0]==0x09){
                setLeftRpm=data[1]+(data[2]<<8);
                 //qDebug()<<"setLeftRpm" <<setLeftRpm;
                emit refrParams(ID_RPM_SHNEIDER,setLeftRpm);
            }

        }

        if(reciever==CAN_ADR_DANFOSS_1 && command==SCHN_SET_RPM)
        {
            temp="Насос: ";
            if((data[0]&0x7)==1)
                temp.append(  "левый ");
            else if((data[0]&0x7)==2)
                 temp.append(  "правый ");
            else temp.append(QString::number( data[0]&0x7)) ;
            temp.append( " вращение: ");
            if(data[0]&0x8)
                temp.append(  "по час.");
            else
                temp.append(  "против час.");
            temp.append(" обороты: ");
            temp.append(QString::number((data[2]<<8)+data[1]));
            toPrintLOG(transmitter,reciever,0,temp,true);
        }

        if(reciever==CAN_ADR_DANFOSS_1 && command==SCHN_SET_PID)
        {
            temp="P:";
            temp.append(QString::number((data[1]<<8)+data[0]));
            temp.append(" I: ");
            temp.append(QString::number((data[3]<<8)+data[2]));
            temp.append(" D :");
            temp.append(QString::number((data[5]<<8)+data[4]));

            temp.append(" Имп: ");
            temp.append(QString::number(data[6]));

            temp.append(" Нас: ");
            if((data[7]&0x7)==1)
                temp.append(  " лев. ");
            else if((data[7]&0x7)==2)
                 temp.append(  "прав. ");
            else temp.append(QString::number( data[7]&0x7)) ;

            temp.append("Режим: ");
            if(((data[7]&0x18)>>3)==0)
                temp.append(  " выкл. ");
            else if(((data[7]&0x18)>>3)==1)
                 temp.append(  "пид ");
            else if(((data[7]&0x18)>>3)==3)
                 temp.append(  "шим ");
            else temp.append(QString::number( (data[7]&0x18)>>3)) ;

            toPrintLOG(transmitter,reciever,0,temp,true);
        }
        break;
    default:
        break;
    }

}

void Port::ParseRequest(int command,int reciever, int transmitter, quint8 *data)
{
//Q_UNUSED (data)
    QString temp="";
    switch (transmitter) {
    case CAN_ADR_SMS:
     if (command==0x10) reqSMS=1;
        break;

    case CAN_ADR_TERMINAL:
     if (command==0xDD &&reciever==CAN_ADR_MBO && data[0]==0x04)
       emit refrParams(ID_REQ_QUAL,0);



        break;
        }
}

/*void Port::timeOut()
{
    countPacketMustSended++;
   // qDebug()<<"countPacketMustSended"<<countPacketMustSended<<"countPacketAnswered"<<countPacketAnswered;//<<(countPacketMustSended<countPacketAnswered);
    if(countPacketMustSended>countPacketAnswered){        
    qDebug()<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz")<<"!!!!!!!Timeout!!!!! Not answer!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
    countPacketMustSended=countPacketAnswered;
    countNotAnswered++;
    emit printStatusBar("!!!Timeout!!!"+QDateTime::currentDateTime().toString("hh:mm:ss.zzz"));
    }
    else {//qDebug()<<"good"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
}

}*/

void Port::speedCalc()
{
    static long tick=0;
    static double tempcountSnifferPacket=0;

    tick++;
    //текущая скорость
    if(tick%10==0) // каждый секунду рассчитываем текущую скорость
    {
        speedCurPacketInMinute=(countSnifferPacket-tempcountSnifferPacket)*60;
        tempcountSnifferPacket= countSnifferPacket;
        //среднняя скорость
        speedMidPacketInMinute=countSnifferPacket*(600.0/tick);
    }
}




void Port :: ReadInPort(){

    static QByteArray data;
    QByteArray data_now = thisPort.readAll();
   if(!enableRead) return;//не  парсим данные
    data.append(data_now);

    outPort(data_now);
//    qDebug() <<"data"<< data<<data_now;
    qint32 i_start = 0;
    qint32 i_end = 0;
    bool   start = false;


    for (qint32 i_s = 0; i_s < data.length(); i_s++){

//**************************************************
//********обработка прямых команд от канхакера******
//**************************************************
        if (data[i_s] == 'v') {
            isversC2C=true;
            versC2C="";
        }
        if (data[i_s] == 'V') {
            isnversC2C=true;
            nversC2C="";
        }


        if(data[i_s] == '\r' && ( isversC2C || isnversC2C))
        {
            isversC2C=false;
            isnversC2C=false;
            data.remove(0, i_s + 1);
            i_start = 0;
            i_end = 0;
            i_s = 0;
             emit refrParams(ID_VERS_C2C,0);
            continue;
        }

        if(isversC2C)
        {
            versC2C+=data[i_s];
          //  qDebug()<<"versC2C"<<versC2C;
            continue;
        }
        if(isnversC2C )
        {
            nversC2C+=data[i_s];
           // qDebug()<<"nversC2C"<<(int)data[i_s]<<nversC2C;
            continue;
        }
//**************************************************
//**конец обработки прямых команд от канхакера******
//**************************************************


        if (data[i_s] == 'T') {
            start = true;
            i_start = i_s;
        }
        if ( (data[i_s] == '\r') && ( start == true) ) { // конец сообщения
            start = false;
            i_end = i_s;
            char ch_ID [9];

            char ch_ID_0 [3];
            char ch_ID_1 [3];
            char ch_ID_2 [3];
            char ch_ID_3 [3];
            char ch_len [2];

            //mess_cnt++;
            i_start++;
            for (qint32 iID = 0; iID < 8; iID++) ch_ID [iID] = data [i_start++];
            ch_ID [8] = 0;

// ------------------------------------------------
//  check for error
            qint8 check_correct = 1;
            for (int iID = 0; iID < 8; iID++)                // коррекция
                if ( ( (ch_ID [iID] >= '0') && (ch_ID [iID] <= '9') ) || \
                     ( (ch_ID [iID] >= 'A') && (ch_ID [iID] <= 'F') ) )
                    ;
                else {
                    check_correct = 0;
                    countErrorPacket++;
                }

            if  ( check_correct == 0 ) // пропускаем левак
                continue;
//  check for error
// ------------------------------------------------

            ch_ID_0 [0] =  ch_ID[0];
            ch_ID_0 [1] =  ch_ID[1];
            ch_ID_0 [2] = 0;

            ch_ID_1 [0] =  ch_ID[2];
            ch_ID_1 [1] =  ch_ID[3];
            ch_ID_1 [2] = 0;

            ch_ID_2 [0] =  ch_ID[4];
            ch_ID_2 [1] =  ch_ID[5];
            ch_ID_2 [2] = 0;

            ch_ID_3 [0] =  ch_ID[6];
            ch_ID_3 [1] =  ch_ID[7];
            ch_ID_3 [2] = 0;

            QString Q_ID = ch_ID;
            QString Q_ID_0 = ch_ID_0;
            int idType = strtol(ch_ID_0,0,16);
            int idCommand = strtol(ch_ID_1,0,16);
            int idReciver = strtol(ch_ID_2,0,16);
            int idTransm = strtol(ch_ID_3,0,16);

            QString Q_ID_1 = ch_ID_1;
            QString Q_ID_2 = ch_ID_2;
            QString Q_ID_3 = ch_ID_3;

            ch_len [0] = data [i_start++];
            ch_len [1] = 0;
            qint8 len = ch_len [0] - '0';
            QString Q_len = ch_len;



            bool ok;
//            quint32 ID = Q_ID.toInt(&ok, 16);
            //if (ok == false)
            //    continue;
            // data
            char ch_data [8][3] = {0};
            QString q__data [8];
            quint8 can__data [8];
            check_correct = 1;
            for (int iDATA = 0; iDATA < len; iDATA++){
                ch_data [iDATA][0] = data [i_start++];
                ch_data [iDATA][1] = data [i_start++];
                ch_data [iDATA][2] = 0;

                // коррекция
                if ( ( (ch_data [iDATA][0] >= '0') && (ch_data [iDATA][0] <= '9') ) || \
                     ( (ch_data [iDATA][0] >= 'A') && (ch_data [iDATA][0] <= 'F') ) )
                     ;
                else
                    check_correct = 0;

               if ( ( (ch_data [iDATA][1] >= '0') && (ch_data [iDATA][1] <= '9') ) || \
                    ( (ch_data [iDATA][1] >= 'A') && (ch_data [iDATA][1] <= 'F') ) )
                    ;
               else
                    check_correct = 0;

               if (check_correct == 0)
                 break;
               q__data [iDATA] =  ch_data [iDATA];

               can__data [iDATA] = q__data [iDATA].toInt(&ok, 16);
               if (ok == false) {
                   check_correct = 0;
                   countErrorPacket++;
                   break;
               }
           }
           if  ( check_correct == 0 ) // пропускаем поврежденные сообщения
           {
               data.remove(0, i_end + 1);
               i_start = 0;
               i_end = 0;
               i_s = 0;
               i_s--;
               continue;
           }
            toPrintLOG(idTransm,idReciver,idType,Q_ID +" "+ Q_len+ " " +q__data[0]+q__data[1]+q__data[2]+q__data[3]+q__data[4]+q__data[5]+q__data[6]+q__data[7],false);

           countSnifferPacket++;
         //  qDebug()<<"countSnifferPacket="<<countSnifferPacket;
           switch (idTransm){
           case CAN_ADR_MIK:
           case CAN_ADR_GYRO:
               mik[0].statMIK[NUM_ALL]++;
               break;
           case CAN_ADR_MIK_2:
               mik[1].statMIK[NUM_ALL]++;
               break;
           case CAN_ADR_TERMINAL:
               statTerm[NUM_ALL]++;
               break;
           case CAN_ADR_SCHNEIDR:
               statTerm[NUM_ALL]++;// одна команда от пульта(якобы от шнайдер)
           case 0x51:// Новый датчик оборотов Danfoss
           case CAN_ADR_SCHNEIDR+1:  //ДАТЧИКИ оборотов Danfoss
           case CAN_ADR_SCHNEIDR+2:
           case CAN_ADR_SCHNEIDR+3:
           case CAN_ADR_SCHNEIDR+4:
           case CAN_ADR_SCHNEIDR+5:
           case CAN_ADR_SCHNEIDR+6:
           case CAN_ADR_SCHNEIDR+7:
           case CAN_ADR_SCHNEIDR+8:
           case CAN_ADR_SCHNEIDR+9:
           case CAN_ADR_SCHNEIDR+10:
               break;
           case CAN_ADR_SMS:
           case CAN_ADR_SMS+1:
           case CAN_ADR_SMS+2:
           case CAN_ADR_SMS+3:
           case CAN_ADR_SMS+4:
           case CAN_ADR_SMS+5:
           case CAN_ADR_SMS+6:
           case CAN_ADR_SMS+7:
           case CAN_ADR_SMS+8:
           case CAN_ADR_SMS+9:
               break;
           case CAN_ADR_CAN2CAN:

               break;
           default:
               qDebug()<<"НЕизвестное утсройство"<<idTransm;
           }
          switch (idType) {
          case CanProt::BROADC_DATA:
              //qDebug()<<"Broadcast";
              ParseBroadCast(idCommand,idTransm,can__data);
              break;
          case CanProt::CMD_ACK_MSG:
            //  qDebug()<<"CMD_ACK_MSG"<<Q_len+ " " +q__data[0]+q__data[1]+q__data[2]+q__data[3]+q__data[4]+q__data[5]+q__data[6]+q__data[7];
              ParseAnswerCommand(idCommand,idReciver,idTransm,can__data,len);
              break;

          case CanProt::EVENT_MSG:
             // qDebug()<<"EVENT_MSG";
              ParseEvent(idCommand,idTransm,can__data);
              break;
          case CanProt::DATA_MSG:
             // qDebug()<<"Data";
              ParseData(idCommand,idReciver,idTransm,can__data);
              break;

          case CanProt::DATA_REQ_MSG:
             // qDebug()<<"Data";
              ParseRequest(idCommand,idReciver,idTransm,can__data);
              break;

          case CanProt::CMD_MSG:
              ParseCommand(idCommand,idReciver,idTransm,can__data);
              break;

          default:
              break;
          }
            data.remove(0, i_end + 1);
            i_start = 0;
            i_end = 0;
            i_s = 0;
            i_s--;
        }
    }
}

