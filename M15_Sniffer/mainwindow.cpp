#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QMessageBox>
#include <QRandomGenerator>
using namespace CanProt;

#define MEASUREMENT_NOISE 0.8  // Шум измерений
#define PROCESS_NOISE 0.1      // Шум процесса

float kalman_filter(float measurement) {
    static float x_estimated = 0;  // Оценка текущего состояния
    static float p = 1;            // Ковариационная матрица оценки
    static float kg;               // Коэффициент Калмана

    // Прогноз
    float x_predicted = x_estimated;
    float p_predicted = p + PROCESS_NOISE;

    // Обновление
    kg = p_predicted / (p_predicted + MEASUREMENT_NOISE);
    x_estimated = x_predicted + kg * (measurement - x_predicted);
    p = (1 - kg) * p_predicted;

    return x_estimated;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QThread *thread_New = new QThread;//Создаем поток для порта платы
    canPort = new Port();//Создаем обьект по классу
    canPort->moveToThread(thread_New);//помешаем класс  в поток
    canPort->thisPort.moveToThread(thread_New);//Помещаем сам порт в поток


    //Сохроняемые настройки
    settings = new QSettings("settings.conf",QSettings::IniFormat);
    //Загружаем настройки
   // initGraphforCP();
    if(settings->value("settings/adrMIK",0).toInt()==0)
    {
       ui->radioButton->setChecked(true);
    }else
    {
      ui->radioButton_2->setChecked(true);
    }


     plPanel= new PlasticPanel();

     if(settings->value("settings/plPanel",0).toInt()==0)
     {
        ui->cbTechPanel->setChecked(false);
     }else
     {
       ui->cbTechPanel->setChecked(true);
     }

    // закрытие панели пластика снимает галку с чекбокса
     QObject::connect(plPanel, &QDialog::rejected, this, [this]() {
         ui->cbTechPanel->setChecked(false);
     });

     QObject::connect(plPanel,  &PlasticPanel::sig_on_pbTpClean_toggled, this,&MainWindow::on_pbTpClean_toggled);
     QObject::connect(plPanel,  &PlasticPanel::sig_on_pbTpNext_clicked, this,&MainWindow::on_pbTpNext_clicked);
     QObject::connect(plPanel,  &PlasticPanel::sig_on_pbTpPrevious_clicked, this,&MainWindow::on_pbTpPrevious_clicked);
     QObject::connect(plPanel,  &PlasticPanel::sig_on_pbTpEncoder_sliderMoved, this,&MainWindow::on_pbTpEncoder_sliderMoved);
     QObject::connect(plPanel,  &PlasticPanel::sig_on_pbTpEncoder_sliderReleased, this,&MainWindow::on_pbTpEncoder_sliderReleased);

       if(ui->radioButton->isChecked()){
        mymik=&(canPort->mik[0]);
        canPort->mikAdr=0;
        qDebug()<<"Init MIK0";
    }
    else{
       mymik=&(canPort->mik[1]);
       canPort->mikAdr=1;
       qDebug()<<"Init MIK1";
        }


    connect(canPort, SIGNAL(error_(QString)), this, SLOT(Print(QString)));//Лог ошибок
    connect(thread_New, SIGNAL(started()), canPort, SLOT(process_Port()));//Переназначения метода run
    connect(canPort, SIGNAL(finished_Port()), thread_New, SLOT(quit()));//Переназначение метода выход
    connect(thread_New, SIGNAL(finished()), canPort, SLOT(deleteLater()));//Удалить поток
    connect(canPort, SIGNAL(finished_Port()), thread_New, SLOT(deleteLater()));//Удалить поток
    connect(this,SIGNAL(savesettings(QString,int,int,int,int,int)),canPort,SLOT(Write_Settings_Port(QString,int,int,int,int,int)));//Слот - ввод настроек!
    connect(ui->BtnConnect, SIGNAL(clicked()),canPort,SLOT(ConnectPort()));
    connect(ui->BtnDisconect, SIGNAL(clicked()),canPort,SLOT(DisconnectPort()));
    connect(canPort, SIGNAL(outPort(QString)), this, SLOT(Print(QString)));//Лог
    connect(canPort, SIGNAL(toPrintLOG(int,int,int,QString,bool)), this, SLOT(PrintLog(int,int,int,QString,bool)));//Лог
    connect(canPort, SIGNAL(readyStartCycle()), this, SLOT(writeMarkCycle()));//подтверждение приема цикла
    connect(this,SIGNAL(writeData(QByteArray)),canPort,SLOT(WriteToPort(QByteArray)));
    connect(this,SIGNAL(writeCanData(QByteArray)),canPort,SLOT(WriteToCanPort(QByteArray)));
   // connect(this,SIGNAL(writeCanData(QByteArray)),this,SLOT(PrintSetCommand(QByteArray)));
    connect(canPort, SIGNAL(printStatusBar(QString)), this, SLOT(PrintStatusBar(QString)));//Вывод ошибок в  СтатуС!
    connect(canPort, SIGNAL(refrParams(int,QVariant)),this,SLOT(RefrParams(int,QVariant)));

   connect(canPort,SIGNAL(setModuleState(int)),this,SLOT(refreshModuleState(int))); //состояние подключения


/*    for(int i=0;i<ALL_GUNS_PAINT;i++){

        connect(canPort->Paintguns[i], SIGNAL(PbSend(int)),this,SLOT(fromGunsToMIk(int)));
     //   connect(this, SIGNAL(toBeadsForm(int)),canPort->Paintguns[i],SLOT(settingBeads(int)));

    }*/
    connect(ui->pbConnectCan,SIGNAL(clicked()),this, SLOT(startCanHacker()) );
    connect(ui->pbDisconnectCan,SIGNAL(clicked()),this, SLOT(stopCanHacker()) );

    thread_New->start();

    this->setWindowTitle("CAN Сниффер системы  ЛИС");
    //----------------Поиск виртуального компорта от канхакера--------------------
    bool isCanChacker=false;
      on_Btn_Serch_clicked();
      foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
            if(info.description()=="USB Serial Port" ||info.description()=="USB-SERIAL CH340"
                    ||info.description()=="STMicroelectronics Virtual COM Port"
                    || info.description()=="Устройство с последовательным интерфейсом USB"){//если портов несколько будет открыт последний
              qDebug()<<"OPen port="<<info.portName()<<info.description();
              ui->PortNameBox->setCurrentText(info.portName());
              isCanChacker=true;
            }

        }

        if(canPort->SettingsPort.name!="" && isCanChacker)
        {//открываем порт
            savesettings(ui->PortNameBox->currentText(), 115200,8,0,1, 0);
            ui->statusBar->showMessage("Открываем порт!"+ ui->PortNameBox->currentText(),3000);
            ui->BtnConnect->clicked();
            QTimer::singleShot(3000, ui->pbConnectCan, SIGNAL(clicked()));// задержка 3 сек для старта канхакераc
        }
        else
        {
            QMessageBox::information(this, tr("Unable to find port"), tr("Кан хакер не найден! Проверьте подключение, установлены ли драйвера  и настройки порта!"));
            qDebug()<<"Порт не найден";
             addToManager("Порт не найден");
        }

        timer = new QTimer();
        connect(timer, SIGNAL(timeout()), this, SLOT(refreshParams()));
        timer->setTimerType(Qt::PreciseTimer);
        timer->start(100); // И запустим таймерrefreshParams



        timerStartMag = new QTimer();
        connect(timerStartMag, SIGNAL(timeout()), this, SLOT(startAfterTimer()));
        timerStartMag->setTimerType(Qt::PreciseTimer);
        timerStartMag->setSingleShot(true);

        timerStopMag = new QTimer();
        connect(timerStopMag, SIGNAL(timeout()), this, SLOT(stopAfterTimer()));
        timerStopMag->setTimerType(Qt::PreciseTimer);
        timerStopMag->setSingleShot(true);

        timerAutoClick = new QTimer();
        connect(timerAutoClick, SIGNAL(timeout()), this, SLOT(autoClick()));
        timerAutoClick->setTimerType(Qt::PreciseTimer);
        timerAutoClick->setSingleShot(false);

        timerTenzo = new QTimer();
        timerTenzo->setTimerType(Qt::PreciseTimer);
        timerTenzo->setSingleShot(false);
        connect(timerTenzo, SIGNAL(timeout()), this, SLOT(emultenzoToCAN()));

        periodTimerTenzo=settings->value("settings/periodTimerTenzo",1500).toInt(); //по умолчанию 1,5 секунд

        connect(ui->cbWeightMat1, SIGNAL(stateChanged(int) ), this, SLOT(checkTenzo()));
        connect(ui->cbWeightMat2, SIGNAL(stateChanged(int) ), this, SLOT(checkTenzo()));
        connect(ui->cbWeightSog, SIGNAL(stateChanged(int) ), this, SLOT(checkTenzo()));
        connect(ui->lePeriodTenzoMs, SIGNAL(editingFinished()), this, SLOT(checkTenzo()));
        checkTenzo();

        windowTheme=settings->value("settings/WindowTheme",0).toInt(); //по умолчанию 0 светлая
        connect(ui->cBDarkTheme, SIGNAL(stateChanged(int)),this, SLOT(setThemeWindow(int)));
        if(windowTheme==DARK_THEME) ui->cBDarkTheme->setChecked(true);

        ui->lbIsSetVersC2C->hide();


    // Мои графики!!!

        //** Статистика************************************************************************************

        customPlot = new QCustomPlot(); // Инициализируем графическое полотно
        ui->gLSpeed->addWidget(customPlot,0,0,1,1);  // Устанавливаем customPlot в окно проложения

        customPlot->addGraph(); // blue line
        customPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
        customPlot->addGraph(); // red line
        customPlot->graph(1)->setPen(QPen(QColor(255, 110, 40)));

        QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
        timeTicker->setTimeFormat("%h:%m:%s");
        customPlot->xAxis->setTicker(timeTicker);
        customPlot->axisRect()->setupFullAxesBox();
        customPlot->yAxis->setRange(-1.2, 1.2);

        // make left and bottom axes transfer their ranges to right and top axes:
        connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
        connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

        QTimer *dataTimer = new QTimer();
        // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
       connect(dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
       dataTimer->start(100); // Interval 0 means to refresh as fast as possible
       timerG.start();

  //** Гироскоп************************************************************************************


       cPlGyro = new QCustomPlot(); // Инициализируем графическое полотно
       ui->gLGyro->addWidget(cPlGyro,0,0,1,1);  // Устанавливаем customPlot в окно проложения

       cPlGyro->setInteraction(QCP::iRangeZoom,true);   // Включаем взаимодействие удаления/приближения
       cPlGyro->setInteraction(QCP::iRangeDrag, true);  // Включаем взаимодействие перетаскивания графика
      // cPlGyro->axisRect()->setRangeDrag(Qt::Horizontal);   // Включаем перетаскивание только по горизонтальной оси
      // cPlGyro->axisRect()->setRangeZoom(Qt::Horizontal);   // Включаем удаление/приближение только по горизонтальной оси

       cPlGyro->addGraph(); // blue line
       cPlGyro->graph(0)->setPen(QPen(QColor(40, 110, 255)));
       cPlGyro->addGraph(); // red line
       cPlGyro->graph(1)->setPen(QPen(QColor(255, 110, 40)));
       cPlGyro->addGraph(); // brown line
       cPlGyro->graph(2)->setPen(QPen(QColor(34, 234, 55)));
       cPlGyro->addGraph(); // black line
       cPlGyro->graph(3)->setPen(QPen(Qt::black));
       cPlGyro->graph(3)->setLineStyle(QCPGraph::LineStyle::lsLine);
       cPlGyro->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

       QSharedPointer<QCPAxisTickerTime> timeTicker2(new QCPAxisTickerTime);
       timeTicker2->setTimeFormat("%h:%m:%s.%z");
       cPlGyro->xAxis->setTicker(timeTicker2);
       cPlGyro->axisRect()->setupFullAxesBox();
       cPlGyro->yAxis->setRange(-1.2, 1.2);

       // make left and bottom axes transfer their ranges to right and top axes:
       connect(cPlGyro->xAxis, SIGNAL(rangeChanged(QCPRange)), cPlGyro->xAxis2, SLOT(setRange(QCPRange)));
       connect(cPlGyro->yAxis, SIGNAL(rangeChanged(QCPRange)), cPlGyro->yAxis2, SLOT(setRange(QCPRange)));

// Выод коридора МБО

       cPlMBO = new QCustomPlot(); // Инициализируем графическое полотно
       ui->gLCorridor->addWidget(cPlMBO,0,0,1,1);  // Устанавливаем customPlot в окно проложения

       cPlMBO->addGraph(); // blue line
       cPlMBO->graph(0)->setPen(QPen(QColor(40, 110, 255)));
       cPlMBO->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
       cPlMBO->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));
       cPlMBO->addGraph(); // red line
       cPlMBO->graph(1)->setPen(QPen(QColor(255, 110, 40)));
       cPlMBO->graph(1)->setLineStyle(QCPGraph::LineStyle::lsLine);
       cPlMBO->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

       QSharedPointer<QCPAxisTickerTime> timeTicker3(new QCPAxisTickerTime);
       timeTicker3->setTimeFormat("%h:%m:%s");
       cPlMBO->xAxis->setTicker(timeTicker3);
       cPlMBO->axisRect()->setupFullAxesBox();
       cPlMBO->yAxis->setRange(-1.2, 1.2);

       // make left and bottom axes transfer their ranges to right and top axes:
       connect(cPlMBO->xAxis, SIGNAL(rangeChanged(QCPRange)), cPlMBO->xAxis2, SLOT(setRange(QCPRange)));
       connect(cPlMBO->yAxis, SIGNAL(rangeChanged(QCPRange)), cPlMBO->yAxis2, SLOT(setRange(QCPRange)));

// Настрока пистолетов
     readMode=1;  // режим чтения включен
     for(int i=0;i<COL_ALL;i++) colred[i]=0;// Инициализация цветов выделения
     statusCAN= 0;//

  // Настройка страницы разборки логов
     connect(ui->pBOpenLogfile,SIGNAL(clicked()),this, SLOT(openLogFile()) );
     //------------------------------Строим график скорости-----------------------------------------------------
     customPlotLog = new QCustomPlot(); // Инициализируем графическое полотно
     ui->gLLOG->addWidget(customPlotLog,0,0,1,1);  // Устанавливаем customPlot в окно проложения

     customPlotLog->setInteraction(QCP::iRangeZoom,true);   // Включаем взаимодействие удаления/приближения
     customPlotLog->setInteraction(QCP::iRangeDrag, true);  // Включаем взаимодействие перетаскивания графика
     //customPlotSpeed->axisRect()->setRangeDrag(Qt::Horizontal);   // Включаем перетаскивание только по горизонтальной оси
     //customPlotSpeed->axisRect()->setRangeZoom(Qt::Horizontal);   // Включаем удаление/приближение только по горизонтальной оси


     // Настраиваем шрифт по осям координат
     customPlotLog->xAxis->setTickLabelFont(QFont(QFont().family(), 8));

    // customPlotLog->axisRect()->setupFullAxesBox();
   //  customPlotLog->yAxis->setRange(-1.2, 1.2);

     customPlotLog->xAxis->setLabel("Время");
     customPlotLog->yAxis->setLabel("Уровень");
     customPlotLog->legend->setVisible(true);
     QFont legendFont = font();
     legendFont.setPointSize(10);
     customPlotLog->legend->setFont(legendFont);
     customPlotLog->legend->setSelectedFont(legendFont);
     customPlotLog->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    // Инициализируем график и привязываем его к Осям
     customPlotLog->addGraph(); // blue line X11
     customPlotLog->graph(0)->setName("X11");

     customPlotLog->graph(0)->setPen(QPen(QColor(86, 90, 110)));
     customPlotLog->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

     customPlotLog->addGraph(); // red line X12
     customPlotLog->graph(1)->setName("X12");
     customPlotLog->graph(1)->setPen(QPen(QColor(26, 29, 43)));
     customPlotLog->graph(1)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));


     customPlotLog->addGraph(); // blue line LED
     customPlotLog->graph(2)->setName("LED_ON");
     customPlotLog->graph(2)->setPen(QPen(Qt::green));
     customPlotLog->graph(2)->setLineStyle(QCPGraph::LineStyle::lsNone);
     customPlotLog->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,10));

     customPlotLog->addGraph(); // V
     customPlotLog->graph(3)->setName("V");
     customPlotLog->graph(3)->setPen(QPen(QColor(255, 110, 233)));
     customPlotLog->graph(3)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

     customPlotLog->addGraph(); // HR
     customPlotLog->graph(4)->setName("ω");
     customPlotLog->graph(4)->setPen(QPen(QColor(12, 51, 245)));
     customPlotLog->graph(4)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

     customPlotLog->addGraph(); // blue line LED
     customPlotLog->graph(5)->setName("LED_OFF");
     customPlotLog->graph(5)->setPen(QPen(Qt::red));
     customPlotLog->graph(5)->setLineStyle(QCPGraph::LineStyle::lsNone);
     customPlotLog->graph(5)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,10));


     customPlotLog->addGraph(); // DrPos
     customPlotLog->graph(6)->setName("DrPos");
     customPlotLog->graph(6)->setPen(QPen(Qt::darkYellow));
     customPlotLog->graph(6)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(6)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

     customPlotLog->addGraph(); // DrSet
     customPlotLog->graph(7)->setName("DrSet");
     customPlotLog->graph(7)->setPen(QPen(Qt::green));
     customPlotLog->graph(7)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(7)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

     customPlotLog->addGraph(); // Ximu
     customPlotLog->graph(8)->setName("Ximu");
     customPlotLog->graph(8)->setPen(QPen(Qt::darkRed));
     customPlotLog->graph(8)->setLineStyle(QCPGraph::LineStyle::lsLine);
     customPlotLog->graph(8)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,3));
     // Инициализируем трассировщик
     tracer = new QCPItemTracer(customPlotLog);
//     tracer->setGraph(customPlotLog->graph(0));   // Трассировщик будет работать с графиком

     //tracer->setStyle(QCPItemTracer:: tsPlus);

     connect(customPlotLog,SIGNAL(mousePress(QMouseEvent*)),this,SLOT(clickOnCPLOG(QMouseEvent*)));

  //   connect(customPlot,SIGNAL(mouseWheel(QMouseEvent*)),customPlotSpeed,SIGNAL(mouseWheel(QMouseEvent*)));
   //  connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(ChangeSpeedRange(QCPRange)));
    ui->pBLoad->hide();

    //** Графики************************************************************************************


    cPlgraph = new QCustomPlot(); // Инициализируем графическое полотно
    ui->gLGrafs->addWidget(cPlgraph,0,0,1,1);  // Устанавливаем customPlot в окно проложения

    cPlgraph->setInteraction(QCP::iRangeZoom,true);   // Включаем взаимодействие удаления/приближения
    cPlgraph->setInteraction(QCP::iRangeDrag, true);  // Включаем взаимодействие перетаскивания графика
    // cPlGyro->axisRect()->setRangeDrag(Qt::Horizontal);   // Включаем перетаскивание только по горизонтальной оси
    // cPlGyro->axisRect()->setRangeZoom(Qt::Horizontal);   // Включаем удаление/приближение только по горизонтальной оси

    cPlgraph->xAxis->setLabel("Время");
    cPlgraph->yAxis->setLabel("Обороты/мин");
    cPlgraph->legend->setVisible(true);
    cPlgraph->legend->setFont(legendFont);
    cPlgraph->legend->setSelectedFont(legendFont);
    cPlgraph->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    cPlgraph->addGraph(); // blue line
    cPlgraph->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    cPlgraph->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    cPlgraph->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));
    cPlgraph->addGraph(); // red line
    cPlgraph->graph(1)->setPen(QPen(QColor(255, 110, 40)));
    cPlgraph->graph(1)->setLineStyle(QCPGraph::LineStyle::lsLine);
    cPlgraph->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));
    cPlgraph->addGraph(); // brown line
    cPlgraph->graph(2)->setPen(QPen(QColor(34, 234, 55)));
    cPlgraph->graph(2)->setLineStyle(QCPGraph::LineStyle::lsLine);
    cPlgraph->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));
    cPlgraph->addGraph(); // black line
    cPlgraph->graph(3)->setPen(QPen(Qt::black));
    cPlgraph->graph(3)->setLineStyle(QCPGraph::LineStyle::lsLine);
    cPlgraph->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,3));

    QSharedPointer<QCPAxisTickerTime> timeTicker4(new QCPAxisTickerTime);
    timeTicker4->setTimeFormat("%h:%m:%s.%z");
    cPlgraph->xAxis->setTicker(timeTicker4);
    cPlgraph->axisRect()->setupFullAxesBox();
    cPlgraph->yAxis->setRange(-10, 10);

    // Инициализируем трассировщик
    tracerL = new QCPItemTracer(cPlgraph);
    tracerR = new QCPItemTracer(cPlgraph);

    tracerL->setStyle(QCPItemTracer:: tsCircle);
    tracerR->setStyle(QCPItemTracer:: tsPlus);



    //tracer->setStyle(QCPItemTracer:: tsPlus);

    connect(cPlgraph,SIGNAL(mousePress(QMouseEvent*)),this,SLOT(clickOnCPGraph(QMouseEvent*)));

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(cPlgraph->xAxis, SIGNAL(rangeChanged(QCPRange)), cPlgraph->xAxis2, SLOT(setRange(QCPRange)));
    connect(cPlgraph->yAxis, SIGNAL(rangeChanged(QCPRange)), cPlgraph->yAxis2, SLOT(setRange(QCPRange)));
    ui->cmbMarkerLeft->addItem("Гафик 1");
    ui->cmbMarkerLeft->addItem("Гафик 2");
    ui->cmbMarkerLeft->addItem("Гафик 3");
    ui->cmbMarkerLeft->addItem("Гафик 4");
    ui->cmbMarkerRight->addItem("Гафик 1");
    ui->cmbMarkerRight->addItem("Гафик 2");
    ui->cmbMarkerRight->addItem("Гафик 3");
    ui->cmbMarkerRight->addItem("Гафик 4");


    //Эмуляторы CAN панели
    connect(ui->pbJoyUp, &QPushButton::clicked, this, &MainWindow::emulJoyCanLIS);
    connect(ui->pbJoyLeft, &QPushButton::clicked, this, &MainWindow::emulJoyCanLIS);
    connect(ui->pbJoyDown, &QPushButton::clicked, this, &MainWindow::emulJoyCanLIS);
    connect(ui->pbJoyRight, &QPushButton::clicked, this, &MainWindow::emulJoyCanLIS);
    connect(ui->pbJoyCenter, &QPushButton::clicked, this, &MainWindow::emulJoyCanLIS);



     initForm();
     oldSquare=0;
     oldSquareCur=0;
     techTerminalEncoder=0;


}

void MainWindow::addToManager(QString text)
{
    static int str_num = 0;
    str_num++;
  //  ui->textManager->moveCursor(QTextCursor::End);//Scroll
  //  ui->textManager->textCursor().insertText(QString::number(str_num) + ")  " +QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + ":  " +text+'\r'); // Вывод текста в консоль
  //  ui->textManager->moveCursor(QTextCursor::End);//Scroll
    //   ui->textManager->setText( QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + ":  " +text);
}

//+++++++++++++[Процедура вывода данных в консоль]++++++++++++++++++++++++++++++++++++++++
void MainWindow::Print(QString data)
{
    static int str_num = 0;
    str_num++;
    ui->consol->moveCursor(QTextCursor::End);//Scroll
    ui->consol->textCursor().insertText(/*QString::number(str_num) + " " + */data+'\r'); // Вывод текста в консоль
    ui->consol->moveCursor(QTextCursor::End);//Scroll
}
//+++++++++++++[Процедура вывода данных в лог]++++++++++++++++++++++++++++++++++++++++
void MainWindow::PrintLog(int from, int to, int type, QString data, bool isComment)
{
   // static int str_num = 0;
   // str_num++;
static    QString colMik = "<font color=#0000ff>";
static    QString colMikBc = "<font color=#118afa>";
static    QString colGyro = "<font color=#55fa78>";
static    QString colPult = "<font color=#008000>";
static    QString colfakePult = "<font color=#2fa2a0>";
static    QString colPultBC = "<font color=#056935>";

static    QString colSensorSOG = "<font color=#5582aa>";

static    QString another = "<font color=#fa111d>";
static    QString endHtml = "</font>";



    if(ui->cBLog->isChecked()==false) return;

        if(from==CAN_ADR_MIK && ui->cBMik->isChecked()==false ) return;
        if(from==CAN_ADR_MIK && type==BROADC_DATA && ui->cBMikBrodcast->isChecked()==false ) return;
        if(from==CAN_ADR_MIK && ui->cBMikOther->isChecked()==false && type!=BROADC_DATA) return;

        if(from==CAN_ADR_MIK_2 && ui->cBMik_2->isChecked()==false ) return;
        if(from==CAN_ADR_MIK_2 && type==BROADC_DATA && ui->cBMikBrodcast_2->isChecked()==false ) return;
        if(from==CAN_ADR_MIK_2 && ui->cBMikOther_2->isChecked()==false && type!=BROADC_DATA) return;

        if(from==CAN_ADR_MBO && ui->cBMBO->isChecked()==false ) return;
        if(from==CAN_ADR_MBO && type==BROADC_DATA && ui->cBMBOBrodcast->isChecked()==false ) return;
        if(from==CAN_ADR_MBO && type==EVENT_MSG && ui->cBMBOEvents->isChecked()==false ) return;
        if(from==CAN_ADR_MBO && ui->cBMBOOther->isChecked()==false && type!=BROADC_DATA) return;


        if(from==CAN_ADR_TERMINAL && ui->cbPult->isChecked()==false ) return;
        if(from==CAN_ADR_TERMINAL && type==BROADC_DATA && ui->cbPultBroadcast->isChecked()==false) return;
        if(from==CAN_ADR_TERMINAL && ui->CbPultOther->isChecked()==false && type!=BROADC_DATA) return;

        if(from==CAN_ADR_UBUP && ui->cbUbup->isChecked()==false ) return;
        if(from==CAN_ADR_UBUP && type==BROADC_DATA && ui->cbUbupBroadcast->isChecked()==false) return;
        if(from==CAN_ADR_UBUP && ui->CbUbupOther->isChecked()==false && type!=BROADC_DATA) return;

        if(from==CAN_ADR_UBUP_2 && ui->cbUbup_2->isChecked()==false ) return;
        if(from==CAN_ADR_UBUP_2 && type==BROADC_DATA && ui->cbUbupBroadcast_2->isChecked()==false) return;
        if(from==CAN_ADR_UBUP_2 && ui->CbUbupOther_2->isChecked()==false && type!=BROADC_DATA) return;

        if(from==CAN_ADR_SCHNEIDR && ui->cbShneider->isChecked()==false ) return;
        if(from==CAN_ADR_SCHNEIDR && type==BROADC_DATA && ui->cbShneiderBroadcast->isChecked()==false) return;
        if(from==CAN_ADR_SCHNEIDR && ui->CbShneiderOther->isChecked()==false && type!=BROADC_DATA) return;


        if(from==CAN_ADR_GYRO  && ui->cBGyro->isChecked()==false) return;

        if(from==CAN_ADR_CANHUB  && ui->cBCanHub->isChecked()==false) return;
        if((from==CAN_ADR_DANFOSS_1 ||from==CAN_ADR_DANFOSS_2 || from==CAN_ADR_DANFOSS_3 ) && ui->cBDanfoss->isChecked()==false) return;

        if((from==0xC1 ||from==0xC2 || from==0xC3 ||from==0xC4 || from==0xC5||from==0xC6 || from==0xC7||from==0xC8 || from==0xC9) && ui->cBRpmSensor->isChecked()==false) return;

        if(((from & 0xf0)==0x80)&& ui->cBSensorSOG->isChecked()==false) return;


        if(isComment){
            ui->pTPrintLog->insertPlainText(" " +data); // Вывод текста в консоль
            return;
        }


        switch (from) {
        case CAN_ADR_MIK:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - МИК0");
            break;
        case CAN_ADR_MIK_2:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - МИК1");
            break;
        case CAN_ADR_TERMINAL:
             if(type==BROADC_DATA)
             data = colPult % data;
             else
             data = colPultBC % data;
             data.append(" - Пульт");
            break;

        case CAN_ADR_GYRO:
             data = colGyro % data;
             data.append(" - Гироскоп");
            break;

        case CAN_ADR_FAKE_TERMINAL:
             data = colfakePult % data;
             data.append(" - Cниффер");
            break;

        case CAN_ADR_UBUP:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - УБУП0");
            break;

        case CAN_ADR_UBUP_2:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - УБУП1");
            break;


        case CAN_ADR_MBO:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - МБО");
            break;

        case CAN_ADR_SCHNEIDR:
             if(type==BROADC_DATA)
             data.prepend( colMikBc);
             else
             data = colMik % data;
             data.append(" - Шн-р");
            break;

        case CAN_ADR_SMS:
        case CAN_ADR_SMS+1:
        case CAN_ADR_SMS+2:
        case CAN_ADR_SMS+3:
        case CAN_ADR_SMS+4:
        case CAN_ADR_SMS+5:
        case CAN_ADR_SMS+6:
        case CAN_ADR_SMS+7:
        case CAN_ADR_SMS+8:
        case CAN_ADR_SMS+9:
             data.prepend( colSensorSOG);
             data.append(" - датчик протока СОГ");
            break;


        default:
             data.prepend(another);
             data.append(" - ХЗ");
            break;
        }

        if(!isComment)
        switch (to) {
        case CAN_ADR_MIK:
             data.append("->МИК0");
            break;
        case CAN_ADR_MIK_2:
             data.append("->МИК1");
            break;
        case CAN_ADR_TERMINAL:
             data.append("->Пульт");
            break;

        case CAN_ADR_GYRO:
             data.append("->Гироскоп");
            break;

        case CAN_ADR_FAKE_TERMINAL:
             data.append("->Программа сниффер");
            break;

        case CAN_ADR_UBUP:
             data.append("->УБУП0");
            break;

        case CAN_ADR_UBUP_2:
             data.append("->УБУП1");
            break;
        case CAN_ADR_MBO:
             data.append("->МБО");
            break;

        case CAN_ADR_SCHNEIDR:
             data.append("->Шнайдер");
            break;

        case CAN_ADR_DANFOSS_1:
             data.append("->Данф1");
            break;
        case CAN_ADR_DANFOSS_2:
             data.append("->Данф2");
            break;
        case 0xFF:
             data.append(" -> ВСЕМ");
            break;




        default:
            data.append(" -> ХЗ");
            break;
        }


        data.append(endHtml);



          ui->pTPrintLog->append(/*QString::number(str_num) + " " + */data); // Вывод текста в консоль
//qDebug()<<data;
       ui->pTPrintLog->moveCursor(QTextCursor::End);//Scroll

}
//++++++++[Процедура определения подключенных портов]+++++++++++++++++++++++++++++++++++
void MainWindow::on_Btn_Serch_clicked()
{
    ui->PortNameBox->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
        ui->PortNameBox->addItem(info.portName());
        }
}

void MainWindow::on_PortNameBox_currentIndexChanged(const QString &arg1)
{
    Q_UNUSED (arg1);
    qDebug()<<"SaveSettings";
    savesettings(ui->PortNameBox->currentText(), 115200,8,0,1, 0);
}

void MainWindow::PrintStatusBar(QString text)
{
    ui->statusBar->showMessage(text,3000);
}

MainWindow::~MainWindow()
{
    delete canPort ;
    delete ui;
    delete plPanel;
}

int MainWindow::stringToMask(QString string)
{
    int  mask=0;
    QStringList guns= string.split(" ");
    if(guns.size()==0) return 0;

    for(int i=0;i<guns.size();i++)
    {
        QString my=guns.at(i);
        bool ok;
        my.toInt(&ok);
        if(ok && (my.toInt(&ok)>=1)){
            mask=  (0x01<<(my.toInt(&ok)-1))|mask;
        }
    }
   // qDebug()<<"String & mask"<<string <<mask;
    return mask;
}

QString MainWindow::maskToString(int mask)
{
    QString  mylist="";
  //  qDebug()<<"mask"<<mask;
    for(int i=0;i<16;i++)
    {
        if((mask>>i)&0x01)
        {
            mylist.append(QString::number(i+1)+" ");
        }

    }
    return mylist;

}

void MainWindow::initForm()
{
/*
    WM_NONE=-1,
    WM_AUT0=0,
    WM_SEMI,
    WM_MANUAL,
    WM_AS_IS,
    WM_SEMI_LIS,
    WM_AUTO_LIS*/
    ui->cBWorkMode->addItem("Нет");
    ui->cBWorkMode->addItem("Автомат");
    ui->cBWorkMode->addItem("Полуавтомат");
    ui->cBWorkMode->addItem("Ручной");
    ui->cBWorkMode->addItem("Лис как есть");
    ui->cBWorkMode->addItem("ЛИС ПА");
    ui->cBWorkMode->addItem("ЛИС Авто");
    ui->cBWorkMode->setCurrentIndex(mymik->workMode+1);

    ui->cBSpeedSource->addItem("Счетное колесо");
    ui->cBSpeedSource->addItem("Энкодер");
    ui->cBSpeedSourceCP->addItem("Счетное колесо");
    ui->cBSpeedSourceCP->addItem("Энкодер");

    ui->cbLineTypeCP->addItem("Гладкая");
    ui->cbLineTypeCP->addItem("Точечная");
    ui->cbLineTypeCP->addItem("Хаотичная");

    ui->cBWorkModul->addItem("Пистолеты");
    ui->cBWorkModul->addItem("Спрей 98к2");
   /* ui->pbSend1->hide();
    ui->pbSend2->hide();
    ui->pbSend3->hide();*/

    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
         ui->GunsLayout->addLayout(canPort->Paintguns[i]);

         if(canPort->Paintguns[i]->lisGuns.isVisible==true || canPort->Paintguns[i]->beadsguns.isVisible)
           canPort->Paintguns[i]->showGun();
         else
             canPort->Paintguns[i]->hideGun();
    }


ui->GunsLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding));


    //  ui->verticalLayout->addItem(spacer);

ui->lbCANStatusTerminal->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusTerminal->setScaledContents(true);
ui->lbCANStatusMik->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusMik->setScaledContents(true);
ui->lbCANStatusGyro->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusGyro->setScaledContents(true);
ui->lbCANStatusUbup->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusUbup->setScaledContents(true);
ui->lbCANStatusMBO->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusMBO->setScaledContents(true);
ui->lbCANStatusShneider->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusShneider->setScaledContents(true);
ui->lbCANStatusCanHub->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
ui->lbCANStatusCanHub->setScaledContents(true);


//Настройка таблицы параметров мик
//qDebug()<<"223324";

ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
ui->tableWidget->setRowCount(BroadCastMIKParms:: MIK_PAR_ALL_PARAMS);
ui->tableWidget->setColumnCount(2);
ui->tableWidget->setShowGrid(true); // Включаем сетку
// Разрешаем выделение только одного элемента
ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
// Разрешаем выделение построчно
ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Параметры МИК"   << "Значение");
for(int i=0;i<BroadCastMIKParms:: MIK_PAR_ALL_PARAMS;i++)
{
    ui->tableWidget->setItem(i,0, new QTableWidgetItem(mymik-> mikBcParams[0][i]));
    ui->tableWidget->setItem(i,1, new QTableWidgetItem(mymik-> mikBcParams[1][i]));
}
ui->tableWidget->setColumnWidth(0,ui->tableWidget->width()*6/10);
ui->tableWidget->setColumnWidth(1,ui->tableWidget->width()*3/10);

//вторая таблицы для модульных МДР
ui->tableWidgetMUK->horizontalHeader()->setStretchLastSection(true);
ui->tableWidgetMUK->setRowCount(BroadCastMUKParms:: MUK_PAR_ALL_PARAMS);
ui->tableWidgetMUK->setColumnCount(2);
ui->tableWidgetMUK->setShowGrid(true); // Включаем сетку
// Разрешаем выделение только одного элемента
ui->tableWidgetMUK->setSelectionMode(QAbstractItemView::SingleSelection);
// Разрешаем выделение построчно
ui->tableWidgetMUK->setSelectionBehavior(QAbstractItemView::SelectRows);
ui->tableWidgetMUK->setHorizontalHeaderLabels(QStringList() << "Параметры МИК Модулей:"   << "Значение");
for(int i=0;i<BroadCastMUKParms:: MUK_PAR_ALL_PARAMS;i++)
{
    ui->tableWidgetMUK->setItem(i,0, new QTableWidgetItem(mymik-> mukBcParams[0][i]));
    ui->tableWidgetMUK->setItem(i,1, new QTableWidgetItem(mymik-> mukBcParams[1][i]));
}
ui->tableWidgetMUK->setColumnWidth(0,ui->tableWidgetMUK->width()*6/10);
ui->tableWidgetMUK->setColumnWidth(1,ui->tableWidgetMUK->width()*3/10);

// таблицы статистики МИК
ui->tWMIKstat->horizontalHeader()->setStretchLastSection(true);
ui->tWMIKstat->setRowCount(NUMBER_FOR_MIK_STATISTIC);
ui->tWMIKstat->setColumnCount(16);
//ui->tWMIKstat->setShowGrid(true); // Включаем сетку
// Разрешаем выделение только одного элемента
//ui->tWMIKstat->setSelectionMode(QAbstractItemView::SingleSelection);
// Разрешаем выделение построчно
//ui->tWMIKstat->setSelectionBehavior(QAbstractItemView::SelectRows);
ui->tWMIKstat->setHorizontalHeaderLabels(QStringList() << "№" << "ID"<<"время"<<"ширина"<<"Пист.\nспл"<<"Пист.\nштрих"<<"Пист.\nшары"<<"Пробел"<<"Штрих"<<"Цикл"<<"Длина всех\nпистол.см"<<"Длина \nшага.см"<<"Помпа"<<"Помпа\nкг расчет"<<"Ср.\nскор-ть"<<"Ген-р");
//вывод нулевой строки
addmikStatToTable(0,0);
//Настройка ширины
ui->tWMIKstat->setColumnWidth(0,20);
ui->tWMIKstat->setColumnWidth(1,20);
ui->tWMIKstat->setColumnWidth(2,100);
ui->tWMIKstat->setColumnWidth(3,55);
ui->tWMIKstat->setColumnWidth(4,60);
ui->tWMIKstat->setColumnWidth(5,60);
ui->tWMIKstat->setColumnWidth(6,60);
ui->tWMIKstat->setColumnWidth(7,50);
ui->tWMIKstat->setColumnWidth(8,50);
ui->tWMIKstat->setColumnWidth(9,50);
ui->tWMIKstat->setColumnWidth(10,100);
ui->tWMIKstat->setColumnWidth(11,100);
ui->tWMIKstat->setColumnWidth(12,50);
ui->tWMIKstat->setColumnWidth(13,80);
ui->tWMIKstat->setColumnWidth(14,50);

//настройка ХП

ui->cbWorkModeLM->addItem("отключен");
ui->cbWorkModeLM->addItem("ручной пид");
ui->cbWorkModeLM->addItem("авто пид");
ui->cbWorkModeLM->addItem("шим");
ui->cbWorkModeLM->addItem("ручной пид (Старт_СТОП)");
ui->cbWorkModeLM->setCurrentIndex(3);//ШИМ
ui->cbDirectionLM->addItem("по ч.с.");
ui->cbDirectionLM->addItem("пр-в ч.с.");
ui->cbDirectionLM->setCurrentIndex(0);//по ч.c

ui->cbWorkModeRM->addItem("отключен");
ui->cbWorkModeRM->addItem("ручной пид");
ui->cbWorkModeRM->addItem("авто пид");
ui->cbWorkModeRM->addItem("шим");
ui->cbWorkModeRM->addItem("ручной пид (Старт_СТОП)");
ui->cbWorkModeRM->setCurrentIndex(3);//ШИМ
ui->cbDirectionRM->addItem("по ч.с.");
ui->cbDirectionRM->addItem("пр-в ч.с.");
ui->cbDirectionRM->setCurrentIndex(0);//по ч.c

ui->cbWorkModeLP->addItem("отключен");
ui->cbWorkModeLP->addItem("ручной пид");
ui->cbWorkModeLP->addItem("авто пид");
ui->cbWorkModeLP->addItem("шим");
ui->cbWorkModeLP->addItem("ручной пид (Старт_СТОП)");
ui->cbWorkModeLP->setCurrentIndex(3);//ШИМ
ui->cbDirectionLP->addItem("по ч.с.");
ui->cbDirectionLP->addItem("пр-в ч.с.");
ui->cbDirectionLP->setCurrentIndex(0);//по ч.c

ui->cbWorkModeRP->addItem("отключен");
ui->cbWorkModeRP->addItem("ручной пид");
ui->cbWorkModeRP->addItem("авто пид");
ui->cbWorkModeRP->addItem("шим");
ui->cbWorkModeRP->addItem("ручной пид (Старт_СТОП)");
ui->cbWorkModeRP->setCurrentIndex(3);//ШИМ
ui->cbDirectionRP->addItem("по ч.с.");
ui->cbDirectionRP->addItem("пр-в ч.с.");
ui->cbDirectionRP->setCurrentIndex(0);//по ч.c

ui->cbWorkModePHP->addItem("отключен");
ui->cbWorkModePHP->addItem("ручной пид");
ui->cbWorkModePHP->addItem("авто пид");
ui->cbWorkModePHP->addItem("шим");
ui->cbWorkModePHP->addItem("ручной пид (Старт_СТОП)");
ui->cbWorkModePHP->setCurrentIndex(3);//ШИМ
ui->cbDirectionPHP->addItem("по ч.с.");
ui->cbDirectionPHP->addItem("пр-в ч.с.");
ui->cbDirectionPHP->setCurrentIndex(0);//по ч.c

ui->lbStateVPL->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVPL->setScaledContents(true);

ui->lbStateVPR->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVPR->setScaledContents(true);

ui->lbStateVHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVHL->setScaledContents(true);

ui->lbStateVHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVHR->setScaledContents(true);

ui->lbStateSHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateSHL->setScaledContents(true);

ui->lbStateSHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateSHR->setScaledContents(true);

ui->lbStateVSP1L->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVSP1L->setScaledContents(true);
ui->lbStateVSP2L->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVSP2L->setScaledContents(true);


ui->lbStateVSP1R->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVSP1R->setScaledContents(true);
ui->lbStateVSP2R->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVSP2R->setScaledContents(true);


ui->lbStateVCLL->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVCLL->setScaledContents(true);

ui->lbStateVCLR->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVCLR->setScaledContents(true);

ui->lbStateVWSHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVWSHL->setScaledContents(true);
ui->lbStateVWSHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
ui->lbStateVWSHR->setScaledContents(true);

ui->cbWashingType->addItem("Отключена");
ui->cbWashingType->addItem("левый пистолет");
ui->cbWashingType->addItem("правый пистолет");
ui->cbWashingType->addItem("два пистолета");
ui->cbWashingType->addItem("левая линия пластика");
ui->cbWashingType->addItem("правая линия пластика");
ui->cbWashingType->addItem("левая линия отв-ля");
ui->cbWashingType->addItem("правая линия отв-ля");
//ui->cbWashingType->setCurrentIndex(canPort->workMode+1);

ui->cbModeADC->addItem("Передача отключена");
ui->cbModeADC->addItem("Передача Напряжения, В");
ui->cbModeADC->addItem("Передача Тока, мA");
ui->cbModeADC->addItem("Передача Процентов");
ui->cbModeADC->addItem("Передача Значения рег");

//Настройки для магистральной
pwmSpace=settings->value("settings/pwmSpace",0).toInt();
pwmStroke=settings->value("settings/pwmStroke",0).toInt();
ui->lePwmStroke->setText(QString::number(pwmStroke));
ui->lePwmSpace->setText(QString::number(pwmSpace));
ui->SlStroke->setValue(pwmStroke);
ui->SlSpase->setValue(pwmSpace);

pidP=settings->value("settings/pidP",2000).toInt();
pidI=settings->value("settings/pidI",300).toInt();
pidD=settings->value("settings/pidD",0).toInt();
ui->lePidP->setText(QString::number(pidP));
ui->lePidI->setText(QString::number(pidI));
ui->lePidD->setText(QString::number(pidD));
ui->SlStroke->setValue(pwmStroke);
ui->SlSpase->setValue(pwmSpace);

impPPM=settings->value("settings/impPPM",60).toInt();
ui->leimpRPM->setText(QString::number(impPPM));


delayStartMg=settings->value("settings/delayStartMg",0).toInt();
delayStopMg=settings->value("settings/delayStopMg",0).toInt();
ui->leDelayStartMg->setText(QString::number(delayStartMg));
ui->leDelayStopMg->setText(QString::number(delayStopMg));

// Настройки помпы
pumpValue=settings->value("settings/pumpValue",210).toInt();
ui->lEPumpValue->setText(QString::number(pumpValue/1000.0,'f',2));
paintDens=settings->value("settings/paintDens",160).toInt();
ui->lePaintDens->setText(QString::number(paintDens/100.0,'f',2));

//настройки термо тензо

consMat1=settings->value("settings/consMat1",5.5).toFloat();
ui->leConsMat1->setText(QString::number(consMat1,'f',1));

consMat2=settings->value("settings/consMat2",5.6).toFloat();
ui->leConsMat2->setText(QString::number(consMat2,'f',1));

consSOG=settings->value("settings/consSOG",2.3).toFloat();
ui->leConsSOG->setText(QString::number(consSOG,'f',1));


canalMat1=settings->value("settings/canalMat1",0).toInt();
ui->leCanMat1->setText(QString::number(canalMat1));

canalMat2=settings->value("settings/canalMat2",1).toInt();
ui->leCanMat2->setText(QString::number(canalMat2));

canalSOG=settings->value("settings/canalSOG",2).toInt();
ui->leCanSog->setText(QString::number(canalSOG));

lengForConsuption=settings->value("settings/lengForConsuption",50).toInt();
ui->leLenForConsuption->setText(QString::number(lengForConsuption));

labelForCons=0;

for(int i=0;i<NUMBER_SENSOR_MOTION;i++)
{
     ui->SensorLayout->addLayout(canPort->sms[i]);

     if(canPort->sms[i]->isVisible==true)
       canPort->sms[i]->showGun();
     else
         canPort->sms[i]->hideGun();
       connect(canPort->sms[i], SIGNAL(sreqEprom(int)),this,SLOT(requestSetEprSms(int)));
       connect(canPort->sms[i], SIGNAL(sreqSetAlarm(int)),this,SLOT(requestSetAlarmSms(int)));
       connect(canPort->sms[i], SIGNAL(sreqSetAdr(int)),this,SLOT(requestSetAdr(int)));
}
ui->SensorLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding));

for(int i=0;i<NUMBER_SENSOR_DANFOSS;i++)
{
     ui->SensorDanfossLayout->addLayout(canPort->sensDanf[i]);

     if(canPort->sensDanf[i]->isVisible==true)
       canPort->sensDanf[i]->showSens();
     else
         canPort->sensDanf[i]->hideSens();
       connect(canPort->sensDanf[i], SIGNAL(transmitAddres(int)),this,SLOT(adrDanfToLabel(int)));
       connect(canPort->sensDanf[i], SIGNAL(transmitSpeed(int)),this,SLOT(setSpeedDanfoss200ms(int)));
}
ui->SensorDanfossLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding));

}


//static int counttenzo=0;
void MainWindow::RefrParams(int params, QVariant par1)
{
 QString parsStatusMik="";
 QString startFrom="";
    switch (params) {
    case ID_WORK_MODE:
        ui->cBWorkMode->setCurrentIndex(mymik->workMode+1);
        ui->lEtransitionType->setText(QString::number(mymik->transitionType));
        startFrom.append("Кц.пр: ");
        if (((mymik->transitionType >>4) & 0x0f )== 0) startFrom.append("Пр-л");
        if (((mymik->transitionType >>4) & 0x0f ) == 1) startFrom.append("Шт-х");
        if (((mymik->transitionType >>4) & 0x0f )== 2) startFrom.append("2Пр-л");
        startFrom.append(" Нч.нов: ");
        if ((mymik->transitionType & 0x0f) == 0) startFrom.append("Пр-л");
        if ((mymik->transitionType & 0x0f) == 1) startFrom.append("Шт-х");
        if ((mymik->transitionType & 0x0f) == 2) startFrom.append("2Пр-л");
        ui->lbtransitionType->setText(startFrom);
        ui->lESolid->setText(maskToString(mymik->solidGuns));
        ui->lEBroke-> setText(maskToString(mymik->brokenGuns));
        for(int i=0;i<ALL_GUNS_PAINT;i++)
        {
            if(canPort->Paintguns[i]->lisGuns.isVisible)
            {
            if((mymik->workGuns>>i)&0x01) canPort->Paintguns[i]->leBrokeOrSolid->setPixmap(QPixmap(":/PNG/solid_line.png"));
            if((mymik->brokenGuns>>i)&0x01) canPort->Paintguns[i]->leBrokeOrSolid->setPixmap(QPixmap(":/PNG/broken_line.png"));
            }
        }
        colred[COL_MODE]=5;
        addGun();
        break;
    case ID_CONF_BASE:
        ui->lEMarkerPath->setText(QString::number(mymik->markerPath));
        ui->lEScanerPath->setText(QString::number(mymik->scanerParth));
        ui->lECorrection-> setText(QString::number(mymik->coefCorr));
        colred[COL_CONF]=5;
        break;

    case ID_CONF_MODUL:
        //for HP & paint
        ui->cBWorkModul->setCurrentIndex(canPort->workModul);
        ui->cBSpeedSource->setCurrentIndex(canPort->speedSensorType);
        ui->lEcoefSpSens->setText(QString::number(canPort->speedSensorCoef));
        //for CP
        ui->cBWorkModulCP->setCurrentIndex(canPort->workModul);
        ui->cBSpeedSourceCP->setCurrentIndex(canPort->speedSensorType);
        ui->lEcoefSpSensCP->setText(QString::number(canPort->speedSensorCoef));
        ui->cbLineTypeCP->setCurrentIndex(canPort->lineTypeCP);
        ui->lEdistBetweenDots->setText(QString::number(canPort->distBetweenDots));
        break;

     case   ID_SETUP_HYDRO:
        switch (par1.toInt()) {
        case SCREW:
            ui->cbWorkModePHP->setCurrentIndex(canPort->hydroMode[SCREW]);
            ui->cbDirectionPHP->setCurrentIndex(canPort->hydroDirection[SCREW]);
            ui->lERPMPHP->setText(QString::number(canPort->hydroRpm[SCREW]));
            ui->lEWidthPHP->setText(QString::number(canPort->hydroFists[SCREW]));

            if((canPort->hydroMode[SCREW]==HYD_SSPID) ||  (canPort->hydroMode[SCREW]==HYD_PWM)){
             QString temp="";
             for(int i=0;i<11;i++)
             {
               temp.append(QString::number(i)+"=" + QString::number(canPort->presetsSCREW[i])+ " | " )   ;
             }
             ui->lEpresetsPHP->setText(temp);
            }
            colred[COL_RPM_SCREW]=5;
            break;
        case PUMPLEFT:
            for(int i=0;i<4;i++)
            {
                if(saveValuePosGR[i]==GR_SET_PPL)
                    realtimeDataGRAPH(i,canPort->hydroRpm[PUMPLEFT]);
            }
            ui->cbWorkModeLP->setCurrentIndex(canPort->hydroMode[PUMPLEFT]);
            ui->cbDirectionLP->setCurrentIndex(canPort->hydroDirection[PUMPLEFT]);
            ui->lERPMLP->setText(QString::number(canPort->hydroRpm[PUMPLEFT]));            
           // colred[COL_RPM_SCREW]=5;
            break;
        case PUMPRIGHT:
            for(int i=0;i<4;i++)
            {
                if(saveValuePosGR[i]==GR_SET_PPR)
                    realtimeDataGRAPH(i,canPort->hydroRpm[PUMPRIGHT]);
            }
            ui->cbWorkModeRP->setCurrentIndex(canPort->hydroMode[PUMPRIGHT]);
            ui->cbDirectionRP->setCurrentIndex(canPort->hydroDirection[PUMPRIGHT]);
            ui->lERPMRP->setText(QString::number(canPort->hydroRpm[PUMPRIGHT]));
           // colred[COL_RPM_SCREW]=5;
            break;
        case HARDLEFT:
            ui->lERPMLH->setText(QString::number(canPort->hydroRpm[HARDLEFT]));
           // colred[COL_RPM_SCREW]=5;
            break;
        case HARDRIGHT:

            ui->lERPMRH->setText(QString::number(canPort->hydroRpm[HARDRIGHT]));
           // colred[COL_RPM_SCREW]=5;
            break;
        case MIXLEFT:
            ui->cbWorkModeLM->setCurrentIndex(canPort->hydroMode[MIXLEFT]);
            ui->cbDirectionLM->setCurrentIndex(canPort->hydroDirection[MIXLEFT]);
            ui->lERPMLM->setText(QString::number(canPort->hydroRpm[MIXLEFT]));
           // colred[COL_RPM_SCREW]=5;
            break;
        case MIXRIGHT:
            ui->cbWorkModeRM->setCurrentIndex(canPort->hydroMode[MIXRIGHT]);
            ui->cbDirectionRM->setCurrentIndex(canPort->hydroDirection[MIXRIGHT]);
            ui->lERPMRM->setText(QString::number(canPort->hydroRpm[MIXRIGHT]));
           // colred[COL_RPM_SCREW]=5;
            break;

        case DRUM:

            for(int i=0;i<4;i++)
            {
                if(saveValuePosGR[i]==GR_SET_MDRUM)
                {
                        realtimeDataGRAPH(i,canPort->hydroRpm[DRUM]);
                }
            }
            break;
           // colred[COL_RPM_SCREW]=5;
            break;

        default:
            break;
        }
        break;
    case   ID_SETUP_HYDRO_1:
       switch (par1.toInt()) {
       /*case SCREW:
           ui->cbWorkModePHP->setCurrentIndex(canPort->hydroMode[SCREW]);
           ui->cbDirectionPHP->setCurrentIndex(canPort->hydroDirection[SCREW]);
           ui->lERPMPHP->setText(QString::number(canPort->hydroRpm[SCREW]));
           ui->lEWidthPHP->setText(QString::number(canPort->hydroFists[SCREW]));

           if(canPort->hydroMode[SCREW]==HYD_SSPID){
            QString temp="";
            for(int i=0;i<11;i++)
            {
              temp.append(QString::number(i)+"=" + QString::number(canPort->presetsSCREW[i])+ " | " )   ;
            }
            ui->lEpresetsPHP->setText(temp);
           }
           colred[COL_RPM_SCREW]=5;
           break;*/
       case PUMPLEFT:
           ui->lEPPL_P->setText(QString::number(canPort->hydroPID_P[PUMPLEFT]));
           ui->lEPPL_I->setText(QString::number(canPort->hydroPID_I[PUMPLEFT]));
           break;
       case PUMPRIGHT:
           ui->lEPPR_P->setText(QString::number(canPort->hydroPID_P[PUMPRIGHT]));
           ui->lEPPR_I->setText(QString::number(canPort->hydroPID_I[PUMPRIGHT]));
           break;
     /*  case HARDLEFT:
           ui->lERPMLH->setText(QString::number(canPort->hydroRpm[HARDLEFT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case HARDRIGHT:

           ui->lERPMRH->setText(QString::number(canPort->hydroRpm[HARDRIGHT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case MIXLEFT:
           ui->cbWorkModeLM->setCurrentIndex(canPort->hydroMode[MIXLEFT]);
           ui->cbDirectionLM->setCurrentIndex(canPort->hydroDirection[MIXLEFT]);
           ui->lERPMLM->setText(QString::number(canPort->hydroRpm[MIXLEFT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case MIXRIGHT:
           ui->cbWorkModeRM->setCurrentIndex(canPort->hydroMode[MIXRIGHT]);
           ui->cbDirectionRM->setCurrentIndex(canPort->hydroDirection[MIXRIGHT]);
           ui->lERPMRM->setText(QString::number(canPort->hydroRpm[MIXRIGHT]));
          // colred[COL_RPM_SCREW]=5;
           break;*/

       default:
           break;
       }
       break;
    case   ID_SETUP_HYDRO_2:
       switch (par1.toInt()) {
       /*case SCREW:
           ui->cbWorkModePHP->setCurrentIndex(canPort->hydroMode[SCREW]);
           ui->cbDirectionPHP->setCurrentIndex(canPort->hydroDirection[SCREW]);
           ui->lERPMPHP->setText(QString::number(canPort->hydroRpm[SCREW]));
           ui->lEWidthPHP->setText(QString::number(canPort->hydroFists[SCREW]));

           if(canPort->hydroMode[SCREW]==HYD_SSPID){
            QString temp="";
            for(int i=0;i<11;i++)
            {
              temp.append(QString::number(i)+"=" + QString::number(canPort->presetsSCREW[i])+ " | " )   ;
            }
            ui->lEpresetsPHP->setText(temp);
           }
           colred[COL_RPM_SCREW]=5;
           break;*/
       case PUMPLEFT:
           ui->lEPPL_D->setText(QString::number(canPort->hydroPID_D[PUMPLEFT]));
           ui->lEPPL_IMP->setText(QString::number(canPort->hydroImpOnRpm[PUMPLEFT]));
           break;
       case PUMPRIGHT:
           ui->lEPPR_D->setText(QString::number(canPort->hydroPID_D[PUMPRIGHT]));
           ui->lEPPR_IMP->setText(QString::number(canPort->hydroImpOnRpm[PUMPRIGHT]));
           break;
     /*  case HARDLEFT:
           ui->lERPMLH->setText(QString::number(canPort->hydroRpm[HARDLEFT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case HARDRIGHT:

           ui->lERPMRH->setText(QString::number(canPort->hydroRpm[HARDRIGHT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case MIXLEFT:
           ui->cbWorkModeLM->setCurrentIndex(canPort->hydroMode[MIXLEFT]);
           ui->cbDirectionLM->setCurrentIndex(canPort->hydroDirection[MIXLEFT]);
           ui->lERPMLM->setText(QString::number(canPort->hydroRpm[MIXLEFT]));
          // colred[COL_RPM_SCREW]=5;
           break;
       case MIXRIGHT:
           ui->cbWorkModeRM->setCurrentIndex(canPort->hydroMode[MIXRIGHT]);
           ui->cbDirectionRM->setCurrentIndex(canPort->hydroDirection[MIXRIGHT]);
           ui->lERPMRM->setText(QString::number(canPort->hydroRpm[MIXRIGHT]));
          // colred[COL_RPM_SCREW]=5;
           break;*/

       default:
           break;
       }
       break;



    case   ID_DELAYS_CP:
       switch (par1.toInt()) {
       case VPLASTL:
           ui->lbVPLON->setText(QString::number(canPort->valvDelOn[VPLASTL]));
           ui->lbVPLOFF->setText(QString::number(canPort->valvDelOff[VPLASTL]));
         //  colred[COL_RPM_SCREW]=5;
           break;
       case VPLASTR:
           ui->lbVPRON->setText(QString::number(canPort->valvDelOn[VPLASTR]));
           ui->lbVPROFF->setText(QString::number(canPort->valvDelOff[VPLASTR]));
           break;
       case VHARDL:
           ui->lbVHLON->setText(QString::number(canPort->valvDelOn[VHARDL]));
           ui->lbVHLOFF->setText(QString::number(canPort->valvDelOff[VHARDL]));
         //  colred[COL_RPM_SCREW]=5;
           break;
       case VHARDR:
           ui->lbVHRON->setText(QString::number(canPort->valvDelOn[VHARDR]));
           ui->lbVHROFF->setText(QString::number(canPort->valvDelOff[VHARDR]));
           break;
       case VSHIBL:
           ui->lbVSHLON->setText(QString::number(canPort->valvDelOn[VSHIBL]));
           ui->lbVSHLOFF->setText(QString::number(canPort->valvDelOff[VSHIBL]));
         //  colred[COL_RPM_SCREW]=5;
           break;
       case VSHIBR:
           ui->lbVSHRON->setText(QString::number(canPort->valvDelOn[VSHIBR]));
           ui->lbVSHROFF->setText(QString::number(canPort->valvDelOff[VSHIBR]));
           break;

       case VSP1L:
           ui->lbVSPLON->setText(QString::number(canPort->valvDelOn[VSP1L]));
           ui->lbVSPLOFF->setText(QString::number(canPort->valvDelOff[VSP1L]));
           break;
       case VSP1R:
           ui->lbVSPRON->setText(QString::number(canPort->valvDelOn[VSP1R]));
           ui->lbVSPROFF->setText(QString::number(canPort->valvDelOff[VSP1R]));
           break;

       case PPLL:
           ui->lbPPLon->setText(QString::number(canPort->valvDelOn[PPLL]));
           ui->lbPPLoff->setText(QString::number(canPort->valvDelOff[PPLL]));
           break;

       case PPLR:
           ui->lbPPRon->setText(QString::number(canPort->valvDelOn[PPLR]));
           ui->lbPPRoff->setText(QString::number(canPort->valvDelOff[PPLR]));
           break;

       case PHDL:
           ui->lbPHDLon->setText(QString::number(canPort->valvDelOn[PHDL]));
           ui->lbPHDLoff->setText(QString::number(canPort->valvDelOff[PHDL]));
           break;

       case PHDR:
           ui->lbPHDRon->setText(QString::number(canPort->valvDelOn[PHDR]));
           ui->lbPHDRoff->setText(QString::number(canPort->valvDelOff[PHDR]));
           break;
       case PPPUSH:
           ui->lbPushAmplitude->setText(QString::number(canPort->valvDelOn[PPPUSH]));
           ui->lbPushDelay->setText(QString::number(canPort->valvDelOff[PPPUSH]));
           break;
       case HARDSENSORS:
           ui->lbFlowLimit->setText(QString::number(canPort->valvDelOn[HARDSENSORS]));
           ui->lbFlowDelay->setText(QString::number(canPort->valvDelOff[HARDSENSORS]));
           break;


       default:
           break;
       }

       break;

    case ID_CONF_CYCLE:
        ui->LELenSpace->setText(QString::number(mymik->lenGap));
        ui->LELenMark->setText(QString::number(mymik->lenMark));
        ui->lETolerGap-> setText(QString::number(mymik->lenToleranceGap));
        ui->lETolerMark-> setText(QString::number(mymik->lenToleranceMark));
        colred[COL_CYCLE]=5;
        break;

    case ID_GYRO_XYZ:
        ui->lEX->setText(QString::number(canPort->gyroX));
        ui->lEY->setText(QString::number(canPort->gyroY));
        ui->lEZ-> setText(QString::number(canPort->gyroZ));

        //в график
        if(ui->CBGyroGphEnable->isChecked())
        realtimeDataSlotGyro();
       // colorConf=5;
        break;
    case ID_MBO_CORRIDOR:

        realtimeDataSlotMBO();


        break;



    case ID_LIS_CORR:
        for(int i=0;i<ALL_GUNS_PAINT;i++){
            if(canPort->Paintguns[i]->lisGuns.isVisible)
            {
                canPort->Paintguns[i]->leTi->setText(QString::number(canPort->Paintguns[i]->lisGuns.Ti));
                canPort->Paintguns[i]->leTo->setText(QString::number(canPort->Paintguns[i]->lisGuns.To));
                canPort->Paintguns[i]->leS2->setText(QString::number(canPort->Paintguns[i]->lisGuns.S2));
                canPort->Paintguns[i]->leR2->setText(QString::number(canPort->Paintguns[i]->lisGuns.R2));
                colred[COL_GUNS1]=5;
            }
        }
        break;

    case ID_GUNS_SETUP:
        for(int i=0;i<ALL_GUNS_PAINT;i++){
            if(canPort->Paintguns[i]->lisGuns.isVisible)
            {
                canPort->Paintguns[i]->leBase->setText(QString::number(canPort->Paintguns[i]->lisGuns.Base));
                canPort->Paintguns[i]->lebeads->setText(maskToString(canPort->Paintguns[i]->lisGuns.beads));
                colred[COL_GUNS2]=5;
            }
        }
        break;

    case ID_BEADS_PAR:
        for(int i=0;i<ALL_GUNS_PAINT;i++){
            if(canPort->Paintguns[i]->beadsguns.isVisible)
            {
                canPort->Paintguns[i]->leTi->setText(QString::number(canPort->Paintguns[i]->beadsguns.Ti));
                canPort->Paintguns[i]->leTo->setText(QString::number(canPort->Paintguns[i]->beadsguns.To));
                canPort->Paintguns[i]->leBase->setText(QString::number(canPort->Paintguns[i]->beadsguns.Base));
                colred[COL_BEADS]=5;
            }
        }
        break;



    case ID_COMMAND_MIK:
        ui->tEMikCommand->append(QDateTime::currentDateTime().toString("hh:mm:ss.zzz") +"   "+par1.toString());
        if(par1.toString()==("МИК0 СТAРT!"))
        {qDebug()<<"Нажали СТАРТ!";

            DeltaWeight1=0;
            DeltaWeight2=0;
            DeltaWeight3=0;
         SaveWeight1=CurrWeight1;
         SaveWeight2=CurrWeight2;
         SaveWeight3=CurrWeight3;
         labelForCons=0;
         SaveWeightBegin1=CurrWeight1;
         SaveWeightEnd1=CurrWeight1;
         SaveWeightBegin2=CurrWeight2;
         SaveWeightEnd2=CurrWeight2;
         SaveWeightBegin3=CurrWeight3;
         SaveWeightEnd3=CurrWeight3;




            if(canPort->mukModul==MODULE_PAINT){

                    for(int i=0;i<4;i++)
                    {

                        if(saveValuePosGR[i]==GR_BUTTON_START )
                        {
                                realtimeDataGRAPH(i,0);
                                realtimeDataGRAPH(i,100);
                                realtimeDataGRAPH(i,0);
                        }
            }
        }
        }
        break;
    case ID_EVENT_MIK:
        if(!par1.toString().contains("Соб: перек. пист", Qt::CaseInsensitive))
            ui->tEMikCommand->append(QDateTime::currentDateTime().toString("hh:mm:ss.zzz") +"   "+par1.toString());


        if(ui->cbEventSwich->isChecked() && par1.toString().contains("Соб: перек. пист", Qt::CaseInsensitive))
            ui->tEMikCommand->append(QDateTime::currentDateTime().toString("hh:mm:ss.zzz") +"   "+par1.toString());
        // canPort->textEvent.clear();
        if(canPort->mukModul==MODULE_CP){
            for(int i=0;i<4;i++)
            {

                if(saveValuePosGR[i]==GR_CANAL_L )
                {
                    if( par1.toString().contains("Соб: перек. пист[0]= 0", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,50);
                    if( par1.toString().contains("Соб: перек. пист[0]= 1", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,70);
                }
                if(saveValuePosGR[i]==GR_CANAL_R)
                {
                    if( par1.toString().contains("Соб: перек. пист[1]= 0", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,55);
                    if( par1.toString().contains("Соб: перек. пист[1]= 1", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,75);
                }
            }
        }
        else if(canPort->mukModul==MODULE_PAINT)
        {        for(int i=0;i<4;i++)
            {

                if(saveValuePosGR[i]==GR_CANAL_1 )
                {
                    if( par1.toString().contains("Соб: перек. пист[0]= 0", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,50);
                    if( par1.toString().contains("Соб: перек. пист[0]= 1", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,70);
                }
                if(saveValuePosGR[i]==GR_CANAL_3)
                {
                    if( par1.toString().contains("Соб: перек. пист[2]= 0", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,55);
                    if( par1.toString().contains("Соб: перек. пист[2]= 1", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,75);
                }
            }
        }

        else if(canPort->mukModul==MODULE_TP)
        {        for(int i=0;i<4;i++)
            {

                if(saveValuePosGR[i]==GR_FIST )
                {
                    if( par1.toString().contains("Соб: перек. пист[4]= 0", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,5);
                    if( par1.toString().contains("Соб: перек. пист[4]= 1", Qt::CaseInsensitive) )
                        realtimeDataGRAPH(i,50);
                }

            }
        }

        break;

    case ID_STATUS_TERMINAL:
        ui->lbTimeTerminal->setText(canPort->timeTerminal);
        ui->lbAdressMik->setText(QString::number(canPort->pultAdrMik,16));
        ui->lbAdressUBUP->setText(QString::number(canPort->pultAdrUBUP,16));
        ui->lbAdressMBO->setText(QString::number(canPort->pultAdrMBO,16));

        break;
    case ID_STATUS_MIK:
        if(par1==1) makeConsuption();

        if(par1<=5)
        for(int i=0;i<BroadCastMIKParms:: MIK_PAR_ALL_PARAMS;i++)
        {
        ui->tableWidget->setItem(i,1, new QTableWidgetItem(mymik-> mikBcParams[1][i]));
        }

      if(par1==6 || par1==7 || par1==8)
        for(int i=0;i<BroadCastMUKParms:: MUK_PAR_ALL_PARAMS;i++)
        {
         if((i==BroadCastMUKParms::MUK_ADC_VOLT_1 ||i==BroadCastMUKParms::MUK_ADC_VOLT_2||i==BroadCastMUKParms::MUK_ADC_VOLT_3||i==BroadCastMUKParms::MUK_ADC_VOLT_4) &&(ui->cbModeADC->currentIndex()==1 || ui->cbModeADC->currentIndex()==2))
         {
            int temp=mymik-> mukBcParams[1][i].toInt();
            ui->tableWidgetMUK->setItem(i,1, new QTableWidgetItem(QString::number(temp/100.0,'f',2)));
         } else
            ui->tableWidgetMUK->setItem(i,1, new QTableWidgetItem(mymik-> mukBcParams[1][i]));

        }

        ui->lbSpeed->setText(mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_SPEED]+" км/ч");



        if(par1==3){        //parsing mik status
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>0)&0x01) { parsStatusMik.append("Переход в стоп!\n"); }
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>1)&0x01) { parsStatusMik.append("Отрисовка в ручном режиме!\n"); }
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>2)&0x01) { parsStatusMik.append("Режим пауза!\n"); }
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>8)&0x01) { parsStatusMik.append("Блокировка-Нет УБУП! \n"); }
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>9)&0x01) { parsStatusMik.append("Блокировка-Нет Терминала !\n"); }
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>10)&0x01) { parsStatusMik.append("Блокировка-Нет скорости!\n");}
            if(((mymik-> mikBcParams[1][BroadCastMIKParms::MIK_PAR_STATUS].toInt())>>12)&0x01) { parsStatusMik.append("Привод не в коридоре!\n");}
            ui->lbStatusMIK->setText(parsStatusMik);
        }



        if(ui->tabWidget->currentIndex()==TAB_COLD_PLASTIC && par1==5)
        {
           SetValveCPSate(mymik->mikGunState);
        }

       //qDebug()<<"canPort->mukModul"<<canPort->mukModul<<oldMukModul;

        if(canPort->mukModul==MODULE_CP && oldMukModul!=MODULE_CP)// если придет сигнал что подключен Холодный пластик
        {
           initGraphforCP();
        }

        if(canPort->mukModul==MODULE_PAINT && oldMukModul!=MODULE_PAINT)// если придет сигнал что подключен модуль краски
        {
           initGraphforPaint();
        }

        if(canPort->mukModul==MODULE_TP && oldMukModul!=MODULE_TP)// если придет сигнал что подключен ТП
        {
          qDebug()<<"ЗАшли  в ТП"<<mymik->mukBcParams[1][MUK_WORK_MODE];
            if(mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.прав" ||  mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.лев")
            initGraphforMagistral();
        }

        oldMukModul=canPort->mukModul;

        if(par1==6){
            for(int i=0;i<4;i++)
            {
                if(saveValuePosGR[i]==GR_RPM_PPL)
                    realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_RPM_HYDRO_1].toInt());
                if(saveValuePosGR[i]==GR_RPM_PPR)
                    realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_RPM_HYDRO_2].toInt());

            }
        }
        if(par1==7){
            for(int i=0;i<4;i++)
            {
                if(saveValuePosGR[i]==GR_HARD_L)
                    realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_L].toInt());
                if(saveValuePosGR[i]==GR_HARD_R)
                    realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_R].toInt());
                if(saveValuePosGR[i]==GR_RPM_PUMP_DRUM)
                realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_RPM_HYDRO_2].toInt());
                if(saveValuePosGR[i]==GR_SET_MDRUM) // чтобы уставка красиво рисовалась
                {
                        realtimeDataGRAPH(i,canPort->hydroRpm[DRUM]);
                }
            }
        }


        break;
    case ID_STATUS_UBUP:
        break;
    case ID_STATUS_MBO:
        break;
    case ID_STATUS_SHNEIDER:
        break;

    case ID_STATUS_CANHUB:
        break;
    case ID_MIK_ACK:
        ui->lbcntMikAskCmdBad2->setText("MIK_ans \n true/false: \n" +QString::number(mymik->statMIK[NUM_ACK_GOOD])+"/"+QString::number(mymik->statMIK[NUM_ACK_BAD]));
        break;

    case ID_DATA_MIK:
        ui->lEPercoff->setText(QString::number(canPort-> percOff));
        ui->lELr->setText(QString::number(canPort-> lr));
        ui->lELmark->setText(QString::number(canPort-> lmark));
        ui->lEPercDet->setText(QString::number(canPort-> percDet));
        ui->lElimStroke->setText(QString::number(canPort-> limStroke));
        ui->lELimSpace->setText(QString::number(canPort-> limSpace));
        ui->lEcounterSpeed->setText(QString::number(canPort-> counterSpeed));
        break;
    case ID_FOR_STATISTIC:
        ui->lEStatForMIK->setText(QString("%1").arg(mymik->standart, 8, 16, QLatin1Char( '0' ))+" "+QString("%1").arg(mymik->modul, 2, 16, QLatin1Char( '0' )));
        ui->lbWidthSt->setText(QString::number(mymik->setWidth));
        break;
    case ID_DATA_MIK_STATISTIC:
        qDebug()<<"counterMikStat"<<mymik->counterMikStat<<par1.toInt()<<canPort->mikAdr;
        if(par1.toInt()==canPort->mikAdr){
        addmikStatToTable(mymik->counterMikStat-1,mymik->counterMikStat);
        addmikStatToTable(mymik->counterMikStat,0);//общая
        }
        break;

    case ID_WASHING_MIK:
         ui->cbWashingType->setCurrentIndex(canPort->washingType);
        break;

    case ID_MBO_ANS_CMD:
        ui->lEContrast->setText(QString::number(canPort->mboContrast));
        ui->lEShum->setText(QString::number(canPort->mboShum));
        ui->lEIznos->setText(QString::number(canPort->mboIznos));
        break;

    case ID_MBO_ANS_QUALITY:
        ui->lEQuality->setText(QString::number(canPort->mboQuality));
        break;
    case ID_RPM_MIX_L:
        ui->lbMixerLeft->setText(QString::number(canPort->rpmMixerL));
        break;
    case ID_RPM_MIX_R:
        ui->lbMixerRight->setText(QString::number(canPort->rpmMixerR));
        break;
    case ID_RPM_DANFOSS:
        ui->lbLeftDanfoss->setText(QString::number(canPort->rpmDanfossL));
        ui->lbRightDanfoss->setText(QString::number(canPort->rpmDanfossR));
        for(int i=0;i<4;i++)
        {
        if(saveValuePosGR[i]==GR_RPM_PUMP)
        realtimeDataGRAPH(i,canPort->rpmDanfossL);
        }
        break;
    case ID_PESSURE_SHNEIDER:
        ui->lbPressure->setText(QString::number(canPort->pressure));

            for(int i=0;i<4;i++)
            {
            if(saveValuePosGR[i]==GR_PRESSURE)
            realtimeDataGRAPH(i,canPort->pressure/10);
            }
        break;
    case ID_RPM_SHNEIDER:
        if(ui->cbEventSwich->isChecked())
        {
            ui->tEMikCommand->append(QDateTime::currentDateTime().toString("hh:mm:ss.zzz") +" Уставка оборотов: "+par1.toString());
        }
        for(int i=0;i<4;i++)
        {
            if(saveValuePosGR[i]==GR_SET_MPUMP)
            {
                if(canPort->setLeftRpm>100)  // для ШИМ
                    realtimeDataGRAPH(i,canPort->setLeftRpm/100);
                else
                    realtimeDataGRAPH(i,canPort->setLeftRpm);
            }
        }
        break;

    case ID_SENSOR_MS:
        if(ui->tabWidget->currentIndex()==TAB_SMS)
        {

            if(canPort->sms[par1.toInt()]->isVisible==true)
            {
                canPort->sms[par1.toInt()]->showGun();
                canPort->sms[par1.toInt()]->lbguns->setText(QString::number(canPort->sms[par1.toInt()]->numGuns));
                canPort->sms[par1.toInt()]->lbAlarm->setText(canPort->sms[par1.toInt()]->alarm?"Вкл":"Выкл" );
                canPort->sms[par1.toInt()]->btsendM2->setText(canPort->sms[par1.toInt()]->alarm?"Отключить":"Включить" );
                canPort->sms[par1.toInt()]->lbErrCAN->setText(canPort->sms[par1.toInt()]->errCan?"Есть":"Нет");
                canPort->sms[par1.toInt()]->lbErrEpr->setText(canPort->sms[par1.toInt()]->errEprom?"Есть":"Нет");
                canPort->sms[par1.toInt()]->lbEprSwitch->setText(QString::number(canPort->sms[par1.toInt()]->onOff));
                canPort->sms[par1.toInt()]->lbEprChangeAdr->setText(QString::number(canPort->sms[par1.toInt()]->changeAdr));
                canPort->sms[par1.toInt()]->lbVers->setText(QString::number(canPort->sms[par1.toInt()]->version));
         //       canPort->sms[par1.toInt()->leTo->setText(QString::number(canPort->Paintguns[i]->lisGuns.To));
                //canPort->sms[i]->leS2->setText(QString::number(canPort->Paintguns[i]->lisGuns.S2));
                //canPort->sms[i]->leR2->setText(QString::number(canPort->Paintguns[i]->lisGuns.R2));
                //qDebug()<<"canPort->sms[par1.toInt()]->motion"<<canPort->sms[par1.toInt()]->motion;


            }
            else
                canPort->sms[par1.toInt()]->hideGun();

        }
        break;

    case ID_SENSOR_DANFOSS:
        if(ui->tabWidget->currentIndex()==TAB_DANF_SENS)
        {

            if(canPort->sensDanf[par1.toInt()]->isVisible==true)
            {
                canPort->sensDanf[par1.toInt()]->showSens();
                canPort->sensDanf[par1.toInt()]->lbAdress->setText(QString::number(canPort->sensDanf[par1.toInt()]->addres));
                canPort->sensDanf[par1.toInt()]->lbRpm->setText(QString::number(canPort->sensDanf[par1.toInt()]->rpm));
                canPort->sensDanf[par1.toInt()]->lbName->setText(canPort->sensDanf[par1.toInt()]->name);
            }
            else
               canPort->sensDanf[par1.toInt()]->hideSens();
        }
        break;
    case ID_VERS_C2C:
     ui->lbVersC2C->setText( canPort->versC2C+ " " +canPort->nversC2C );

        break;

    case ID_BC_C2C:
     ui->lbbroadCastC2C->setText( par1.toString() );
        break;
    case ID_EVENT_C2C:
     ui->lbIsSetVersC2C->show();
     QTimer::singleShot(3000, ui->lbIsSetVersC2C, SLOT(hide())); // задержка 3 сек
        break;

    case ID_REQ_QUAL:
    if(ui->cbQualiy->isChecked())     writeToCANatID(ID_SEND_FAKE_QUAL);
        break;

    case ID_SET_QUAL:
    if(ui->cbQualiy->isChecked())     ui->leQuality2->setText(par1.toString() );
        break;


    default:
        break;
    }
}

void MainWindow::fromGunsToMIk(int gun)
{
 qDebug()<<"!!!!!!!!!!!!";
 currentGun=gun;
 writeToCANatID(ID_SEND_GUNS);
}

void MainWindow::requestSetEprSms(int sens)
{
   currentSensor=sens-1;
   writeToCANatID(ID_SEND_SMS_REQ_EEP);
}

void MainWindow::requestSetAlarmSms(int sens)
{
    currentSensor=sens-1;
    writeToCANatID(ID_SEND_SMS_REQ_ALARM);
}

void MainWindow::requestSetAdr(int sens)
{
    currentSensor=sens-1;
    writeToCANatID(ID_SEND_SMS_CH_ADRES);
}

void MainWindow::adrDanfToLabel(int adr)
{
    ui->leCurrAddres->setText(QString::number(adr));
    ui->sBNewAddr_2->setValue(adr);
}

void MainWindow::setSpeedDanfoss200ms(int adr)
{
    currSensDanf=adr;
    writeToCANatID(ID_SEND_SPEED_DANFOSS);
}

void MainWindow::startCanHacker()
{
    QByteArray data; // Текстовая переменная
    //data = ui->cEnterText->text().toLocal8Bit().toHex() + '\r'; // Присвоение "data" значения из EnterText
    data ="CxS6xZ1xOx";
    data [1] = 13;
    data [4] = 13;
    data [7] = 13;
    data [9] = 13;
    writeData(data); // Отправка данных в порт
//    qDebug()<<"wrdata"<<data;
    Print(data); // Вывод данных в консоль
}

void MainWindow::openLogFile()
{

    QSharedPointer<QCPAxisTickerText> textTicker4(new QCPAxisTickerText);
    customPlotLog->xAxis->setTicker(textTicker4);


    QString str = QFileDialog::getOpenFileName(this, "Выбрать файл", "", "*.log");
    QFileInfo info1 (str);
    QFile file(str);
    if (str!=0)
    {
        //  QMessageBox::information(this, "ОК", "Имя файла: " + info1.fileName()+" Размер файла: " + QString::number(info1.size()/1024) + " Кб"+" Дата изменения: " + info1.lastModified().toString());
        /*   ui->label->setText("Имя файла: " + info1.fileName());
          ui->label_2->setText("Размер файла: " + QString::number(info1.size()/1024) + " Кб");
          ui->label_5->setText("Дата изменения: " + info1.lastModified().toString());*/
        ui-> lbfilename->setText("Файл: " + info1.fileName()+" Размер: " + QString::number(info1.size()/1024) + " Кб");
    }
    else {
        QMessageBox::information(this, "Ошибка", "Для начала нужно выбрать файл");
        return;
    }
   timelist.clear();
   X2list.clear();
   Vlist.clear();
   Hrlist.clear();
   lenghtlist.clear();
   DrPoslist.clear();
   DrSetlist.clear();
   Ximulist.clear();

   customPlotLog->graph(0)->data()->clear();
   customPlotLog->graph(1)->data()->clear();
   customPlotLog->graph(2)->data()->clear();
   customPlotLog->graph(3)->data()->clear();
   customPlotLog->graph(4)->data()->clear();
   customPlotLog->graph(5)->data()->clear();
   customPlotLog->graph(6)->data()->clear();
   customPlotLog->graph(7)->data()->clear();
   customPlotLog->graph(8)->data()->clear();

    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не могу открыть файл"));
        return;
    }
    QString data = file.readAll();
    QStringList list = data.split("\n");// получаем к-во точек
    QStringList listdata;// парсим данные
    file.close();


    qDebug()<<"data"<<data.size();
    qDebug()<<"list"<<list.size();

    // Проходимся по всем строкам ...
    // Шапка
    //  stringList <<"Время"<<"X1"<<"X2"<<"LED1"<<"V"<<"Hr";
    //  textStream << stringList.join( ';' )+"\n";
    QTime time = QTime::fromString("10:33:21.112", "h:m:s.z");
    //fromString("1mm12car00", "m'mm'hcarss");
    qDebug()<<"time"<<time.toString("hh:mm:ss.zzz")<<time.msecsSinceStartOfDay() ;



    double lenght=0,key =0,oldKey=0,key1=QTime::fromString(list.at(0).mid(0, 12), "h:m:s.z").msecsSinceStartOfDay();
    if(key1==0)
     key1=QTime::fromString(list.at(1).mid(0, 12), "h:m:s.z").msecsSinceStartOfDay();
    if(key1==0)
     key1=QTime::fromString(list.at(2).mid(0, 12), "h:m:s.z").msecsSinceStartOfDay();



    ui->pBLoad->setRange(0, list.size()-1);
    ui->pBLoad->setValue(0);
    ui->pBLoad->show();


    for( int row = 0; row < list.size()-1; row++ )
    {

        ui->pBLoad->setValue(row);
        listdata = list.at(row).split(";");// получаем данные
        //    stringList << list.at(row).mid(0, 12);
        if(listdata.size()<500) continue;
        time=QTime::fromString(list.at(row).mid(0, 12), "h:m:s.z");
        key= time.msecsSinceStartOfDay()-key1;

        time = QTime::fromString(list.at(row).mid(0, 12), "h:m:s.z");

        timelist.insert (key,time.toString("hh:mm:ss.zzz"));
        if(row%10==0)
            textTicker4->addTick(key, time.toString("hh:mm:ss.zzz")+"\n"+QString::number(lenght,'f', 1) + " см");

        for(int i =650;i<listdata.size();i++)
        {
            if(ui->cBX11->isChecked())
                if(listdata.at(i).contains("X11 ="))
                {
                    int begin =  listdata.at(i).indexOf('=');
                    //   stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);

                    double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble()+450;
                    //   key ++; // time elapsed since start of demo, in seconds
                    customPlotLog->graph(0)->addData(key,value);


                    // qDebug()<<key<<value;

                }
            if(ui->cBX12->isChecked())
            if( listdata.at(i).contains("X12 ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                // stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble()+450;

                customPlotLog->graph(1)->addData(key,value);
                X2list.insert (key,value);
            }

            if(ui->cBLED->isChecked())
            if(listdata.at(i).contains("LED"))
            {
                if(listdata.at(i).contains("on"))
                {
                    //  customPlotLog->graph(0)->setPen(QPen(Qt::green);
                    customPlotLog->graph(2)->addData(key,5);

                    /*stringList <<"1";*/
                }
                if(listdata.at(i).contains("off"))
                {
                    //  customPlotLog->graph(0)->setPen(QPen(Qt::green);
                    customPlotLog->graph(5)->addData(key,5);
                    /* stringList <<"0";*/
                }
            }
            if(ui->cBV->isChecked())
            if(listdata.at(i).contains("V ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                //stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble();

                customPlotLog->graph(3)->addData(key,value);
                Vlist.insert (key,value);
            }

            if(ui->cBHR->isChecked())
            if(listdata.at(i).contains("Hr ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble();
                //  stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);

                customPlotLog->graph(4)->addData(key,value);
                Hrlist.insert (key,value);
            }

            if(listdata.at(i).contains("DrPOS ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble();
                //  stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);

                customPlotLog->graph(6)->addData(key,value);
                DrPoslist.insert (key,value);
            }

            if(listdata.at(i).contains("DrSET ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble();
                //  stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);

                customPlotLog->graph(7)->addData(key,value);
                DrSetlist.insert (key,value);
            }

            if(listdata.at(i).contains("Ximu ="))
            {
                int begin =  listdata.at(i).indexOf('=');
                double value=listdata.at(i).mid(begin+1,listdata.at(i).size()-begin).toDouble();
                //  stringList << listdata.at(i).mid(begin+1,listdata.at(i).size()-begin);

                customPlotLog->graph(8)->addData(key,value);
                Ximulist.insert (key,value);
            }


        }

     lenght+=  (((key-oldKey)/1000)* (Vlist.value(key)));
     qDebug()<<"time2"<<time.toString("hh:mm:ss.zzz")<<key<< " v="<<  Vlist.value(key)<<" leng="<<  ((key-oldKey)* (Vlist.value(key)/36.0))<<"сумм="<<lenght;
    lenghtlist.insert(key,lenght);
    oldKey=key;

    }


    customPlotLog->replot();
    ui->pBLoad->hide();

    qDebug()<<"График построен";
}

void MainWindow::clickOnCPLOG(QMouseEvent  *event)
{
   // QPoint point=event->pos();
  //  qDebug()<<"Mouse coordinat on x"<<customPlotLog->xAxis->pixelToCoord(point.x());
   //  qDebug()<<"Mouse coordinat on y"<<customPlotLog->xAxis->pixelToCoord(point.y());
tracer->setGraph(customPlotLog->graph(0));   // Трассировщик будет работать с графиком
     // Определяем координату X на графике, где был произведён клик мышью
        double coordX = customPlotLog->xAxis->pixelToCoord(event->pos().x());

        // Подготавливаем координаты по оси X для переноса вертикальной линии
        QVector<double> x(2), y(2);
        x[0] = coordX;
        y[0] = -50;
        x[1] = coordX;
        y[1] = 50;

        // Устанавливаем новые координаты
     //   verticalLine->setData(x, y);

        // По координате X клика мыши определим ближайшие координаты для трассировщика
        tracer->setGraphKey(coordX);
        // Выводим координаты точки графика, где установился трассировщик, в lineEdit
         customPlotLog->replot(); // Перерисовываем содержимое полотна графика


        // qDebug()<<"X11= " + QString::number(tracer->position->value())+ " X12= "+QString::number(X2list.value(tracer->position->key()))
        //           + " HR= "+QString::number(Hrlist.value(tracer->position->key()))+ " V= "+QString::number(Vlist.value(tracer->position->key()))
       //           +" Time:"+timelist.value(tracer->position->key());
       // customPlotLog->replot(); // Перерисовываем содержимое полотна графика
         ui->lbData->setText(timelist.value(tracer->position->key())+" X11=" + QString::number(tracer->position->value())+ " X12="+QString::number(X2list.value(tracer->position->key()))
                             + " ω‎="+QString::number(Hrlist.value(tracer->position->key()))+ " V="+QString::number(Vlist.value(tracer->position->key()))+" см/с"
                             + " DrPos‎="+QString::number(DrPoslist.value(tracer->position->key()))+ " DrSet‎="+QString::number(DrSetlist.value(tracer->position->key()))+  " Ximu‎="+QString::number(Ximulist.value(tracer->position->key()))
                             +" Len="+QString::number(lenghtlist.value(tracer->position->key())/100,'f',2) +" м");
}

void MainWindow::clickOnCPGraph(QMouseEvent *event)
{
   // qDebug()<<"clickOnCPGraph"<<event;
    tracerL->setGraph(cPlgraph->graph(ui->cmbMarkerLeft->currentIndex()));   // Трассировщик будет работать с графиком
    tracerR->setGraph(cPlgraph->graph(ui->cmbMarkerRight->currentIndex()));   // Трассировщик будет работать с графиком

        QVector<double> x(2), y(2);
    if(event->buttons()==Qt::LeftButton){
    // Определяем координату X на графике, где был произведён клик мышью
       double coordX = cPlgraph->xAxis->pixelToCoord(event->pos().x());

       // Подготавливаем координаты по оси X для переноса вертикальной линии

       x[0] = coordX;
       y[0] = -50;
       x[1] = coordX;
       y[1] = 50;

       // Устанавливаем новые координаты
    //   verticalLine->setData(x, y);

       // По координате X клика мыши определим ближайшие координаты для трассировщика
       tracerL->setGraphKey(coordX);}
    else
        if(event->buttons()==Qt::RightButton){
        // Определяем координату X на графике, где был произведён клик мышью
           double coordX = cPlgraph->xAxis->pixelToCoord(event->pos().x());

           // Подготавливаем координаты по оси X для переноса вертикальной линии
           x[0] = coordX;
           y[0] = -50;
           x[1] = coordX;
           y[1] = 50;

           // Устанавливаем новые координаты
        //   verticalLine->setData(x, y);

           // По координате X клика мыши определим ближайшие координаты для трассировщика
           tracerR->setGraphKey(coordX);}
     cPlgraph->replot(); // Перерисовываем содержимое полотна графика
    ui->lbMarkL->setText("Время: "+QString::number(tracerL->position->key())+"  Значение: "+QString::number(tracerL->position->value()));
    ui->lbMarkR->setText("Время: "+QString::number(tracerR->position->key())+"  Значение: "+QString::number(tracerR->position->value()));
    ui->lbMarkDist->setText( "Разность времени: "+ QString::number(tracerR->position->key()-tracerL->position->key()) + " значения: "+QString::number(tracerR->position->value()-tracerL->position->value()));

}






void MainWindow::exportCSV()
{

 /*   /* Создаём объекта файла CSV и указываем путь к этому файлу
        * Не забудьте указать валидный путь и расширение .csv
        * */

}

static int bump=0;

void MainWindow::refreshParams()
{

   // qDebug()<<"sizeof canPort"<<sizeof(Port)<<sizeof(Guns);
    static long tikcolor=0;
    // qDebug()<<"Curtab"<< ui->tabWidget->currentIndex()<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    tikcolor++;

    switch (ui->tabWidget->currentIndex()) {
    case TAB_PISTOLS://пистолеты
    {
        if(colred[COL_MODE]>0)
        {
            setColorComboBox( ui->cBWorkMode,colred[COL_MODE]);
            setColorLineEdit( ui->lEtransitionType,colred[COL_MODE]);
            setColorLineEdit( ui->lESolid,colred[COL_MODE]);
            setColorLineEdit( ui->lEBroke,colred[COL_MODE]);

            if(tikcolor%10==0)
            { colred[COL_MODE]--;
            }
            //  qDebug()<<tikcolor<<colorMode;
        }


        if(colred[COL_CONF]>0)
        {
            setColorLineEdit( ui->lEMarkerPath,colred[COL_CONF]);
            setColorLineEdit( ui->lEScanerPath,colred[COL_CONF]);
            setColorLineEdit( ui->lECorrection,colred[COL_CONF]);

            if(tikcolor%10==0)
            {
                colred[COL_CONF]--;
            }
            // qDebug()<<tikcolor<<colorConf;
        }


        if(colred[COL_CYCLE]>0)
        {
            setColorLineEdit( ui->LELenMark,colred[COL_CYCLE]);
            setColorLineEdit( ui->LELenSpace,colred[COL_CYCLE]);
            setColorLineEdit( ui->lETolerGap,colred[COL_CYCLE]);
            setColorLineEdit( ui->lETolerMark,colred[COL_CYCLE]);

            if(tikcolor%10==0)
            {
                colred[COL_CYCLE]--;
            }
            // qDebug()<<tikcolor<<colorConf;
        }

        if(colred[COL_GUNS1]>0)
        {
            for(int i=0;i<ALL_GUNS_PAINT;i++){
                if(canPort->Paintguns[i]->lisGuns.isVisible)
                {

                    setColorLineEdit(canPort->Paintguns[i]->leTi,colred[COL_GUNS1]);
                    setColorLineEdit(canPort->Paintguns[i]->leTo,colred[COL_GUNS1]);
                    setColorLineEdit(canPort->Paintguns[i]->leS2,colred[COL_GUNS1]);
                    setColorLineEdit(canPort->Paintguns[i]->leR2,colred[COL_GUNS1]);
                    setColorLineEdit(canPort->Paintguns[i]->leNumber,colred[COL_GUNS1]);
                }
                if(canPort->Paintguns[i]->beadsguns.isVisible)
                {
                    setColorLineEdit(canPort->Paintguns[i]->leNumber,colred[COL_GUNS1]);
                }

            }

            if(tikcolor%10==0)
            {
                colred[COL_GUNS1]--;
            }
            // qDebug()<<tikcolor<<colorConf;
        }


        if(colred[COL_GUNS2]>0)
        {
            for(int i=0;i<ALL_GUNS_PAINT;i++){
                if(canPort->Paintguns[i]->lisGuns.isVisible)
                {
                    setColorLineEdit(canPort->Paintguns[i]->leBase,colred[COL_GUNS2]);
                    setColorLineEdit(canPort->Paintguns[i]->lebeads,colred[COL_GUNS2]);
                }
            }
            if(tikcolor%10==0)
            {
                colred[COL_GUNS2]--;
            }
            // qDebug()<<tikcolor<<colorConf;
        }

        if(colred[COL_BEADS]>0)
        {

            for(int i=0;i<ALL_GUNS_PAINT;i++){
                if(canPort->Paintguns[i]->beadsguns.isVisible)
                {
                    setColorLineEdit(canPort->Paintguns[i]->leTi,colred[COL_BEADS]);
                    setColorLineEdit(canPort->Paintguns[i]->leTo,colred[COL_BEADS]);
                    setColorLineEdit(canPort->Paintguns[i]->leBase,colred[COL_BEADS]);
                }
            }
            if(tikcolor%10==0)
            {
                colred[COL_BEADS]--;
            }
            // qDebug()<<tikcolor<<colorConf;
        }

        break;
    }

    case TAB_STATISTIC_CAN: //Статистика

        if(canPort->countSnifferPacket==0) break; // что бы не было деления на 0

        ui->lbPackAll->setText(QString::number(canPort->countSnifferPacket)+" Повреждены:" + QString::number(canPort->countErrorPacket));
        ui->lbSpeedAv->setText(QString::number(canPort->speedMidPacketInMinute));
        ui->lbSpeedCur->setText(QString::number(canPort->speedCurPacketInMinute));


        if( mymik->statMIK[NUM_ALL]!=0){
        ui->lbCountMikPacket->setText(QString::number(mymik->statMIK[NUM_ALL])+" ("+QString::number(mymik->statMIK[NUM_ALL]*100/canPort->countSnifferPacket)+"%)");
        ui->lbcntMikBroadCast->setText(QString::number(mymik->statMIK[NUM_BC])+" ("+QString::number(mymik->statMIK[NUM_BC]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntMikData->setText(QString::number(mymik->statMIK[NUM_DATA])+" ("+QString::number(mymik->statMIK[NUM_DATA]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntMikEvent->setText(QString::number(mymik->statMIK[NUM_EVENT])+" ("+QString::number(mymik->statMIK[NUM_EVENT]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntMikAskCmdGood->setText(QString::number(mymik->statMIK[NUM_ACK_GOOD])+" ("+QString::number(mymik->statMIK[NUM_ACK_GOOD]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntMikAskCmdBad->setText(QString::number(mymik->statMIK[NUM_ACK_BAD])+" ("+QString::number(mymik->statMIK[NUM_ACK_BAD]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntMIkCommand->setText(QString::number(mymik->statMIK[NUM_CMD])+" ("+QString::number(mymik->statMIK[NUM_CMD]*100/mymik->statMIK[NUM_ALL])+"%)");
        ui->lbcntGyro->setText(QString::number(mymik->statMIK[NUM_BC_GYRO])+" ("+QString::number(mymik->statMIK[NUM_BC_GYRO]*100/mymik->statMIK[NUM_ALL])+"%)");
        }

        if(canPort->statTerm[NUM_ALL]!=0){
        ui->lbCountPultPacket->setText(QString::number(canPort->statTerm[NUM_ALL])+" ("+QString::number(canPort->statTerm[NUM_ALL]*100/canPort->countSnifferPacket)+"%)");
        ui->lbcntPultBroadCast->setText(QString::number(canPort->statTerm[NUM_BC])+" ("+QString::number(canPort->statTerm[NUM_BC]*100/canPort->statTerm[NUM_ALL])+"%)");
        ui->lbcntPultCommand->setText(QString::number(canPort->statTerm[NUM_CMD])+" ("+QString::number(canPort->statTerm[NUM_CMD]*100/canPort->statTerm[NUM_ALL])+"%)");
        ui->lbcntPultData->setText(QString::number(canPort->statTerm[NUM_DATA])+" ("+QString::number(canPort->statTerm[NUM_DATA]*100/canPort->statTerm[NUM_ALL])+"%)");
        ui->lbcntPultEvent->setText(QString::number(canPort->statTerm[NUM_EVENT])+" ("+QString::number(canPort->statTerm[NUM_EVENT]*100/canPort->statTerm[NUM_ALL])+"%)");
        ui->lbcntPultAskCmdGood->setText(QString::number(canPort->statTerm[NUM_ACK_GOOD])+" ("+QString::number(canPort->statTerm[NUM_ACK_GOOD]*100/canPort->statTerm[NUM_ALL])+"%)");
        ui->lbcntPultAskCmdBad->setText(QString::number(canPort->statTerm[NUM_ACK_BAD])+" ("+QString::number(canPort->statTerm[NUM_ACK_BAD]*100/canPort->statTerm[NUM_ALL])+"%)");
        // ui->lbcntMikEvent->setText(QString::number(canPort->cntMIKEvent)+"("+QString::number(canPort->cntMIKEvent*100/canPort->countSnifferPacket)+"%)");
        }
        break;

   case TAB_HOT_PLASTIC:

        if(colred[COL_RPM_SCREW]>0)
        {
            setColorComboBox( ui->cbWorkModePHP,colred[COL_RPM_SCREW]);
            setColorComboBox( ui->cbDirectionPHP,colred[COL_RPM_SCREW]);
            setColorLineEdit( ui->lERPMPHP,colred[COL_RPM_SCREW]);
            setColorLineEdit( ui->lEWidthPHP,colred[COL_RPM_SCREW]);

            if(tikcolor%10==0)
            {
                colred[COL_RPM_SCREW]--;
            }
        }

        if(ui->LEModulType->text()!=mymik->mukBcParams[1][BroadCastMUKParms::MUK_MODUL])
        {

            //canPort->mukModul=1;
            ui->LEModulType->setText( mymik->mukBcParams[1][BroadCastMUKParms::MUK_MODUL]);
            //меняются типыui->cBSpeedSource-> меняем режимы работы

            ui->cBWorkModul->clear();
            switch (canPort->mukModul) {
            case 0x00://краска
                ui->cBWorkModul->addItem("Пистолеты");
                ui->cBWorkModul->addItem("Спрей 98к2");
                break;
            case 0x01://гп
                ui->cBWorkModul->addItem("не определен");
                ui->cBWorkModul->addItem("Экструдер");
                ui->cBWorkModul->addItem("Мультидот");
                ui->cBWorkModul->addItem("Магистр. лев");
                ui->cBWorkModul->addItem("Магистр. лев. спрей");
                ui->cBWorkModul->addItem("Магистр. прав.");
                ui->cBWorkModul->addItem("Магистр. прав. спрей");
                ui->cBWorkModul->addItem("З.Р.Б");
                ui->cBWorkModul->addItem("400 спрей");
                break;
            case 0x02://хп
                ui->cBWorkModul->addItem("Без рециркуляции");
                ui->cBWorkModul->addItem("Рецирк. 1");
                ui->cBWorkModul->addItem("Рецирк. 2");
                ui->cBWorkModul->addItem("Спрей ХП");
                break;
            default:
                break;
            }
            ui->cBWorkModul->setCurrentIndex(canPort->workModul);
        }
        break;

    case TAB_COLD_PLASTIC:
        ui->lbHardPercL->setText(mymik->mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_L] +"%");
        ui->lbHardPercR->setText(mymik->mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_R]+"%");

        if(ui->LEModulTypeCP->text()!=mymik->mukBcParams[1][BroadCastMUKParms::MUK_MODUL])
        {

            //canPort->mukModul=1;
            ui->LEModulTypeCP->setText( mymik->mukBcParams[1][BroadCastMUKParms::MUK_MODUL]);
            //меняются типыui->cBSpeedSource-> меняем режимы работы

            ui->cBWorkModulCP->clear();
            switch (canPort->mukModul) {
            case 0x00://краска
                ui->cBWorkModulCP->addItem("Пистолеты");
                ui->cBWorkModulCP->addItem("Спрей 98к2");
                break;
            case 0x01://гп
                ui->cBWorkModulCP->addItem("Нет");
                ui->cBWorkModulCP->addItem("Экструдер");
                ui->cBWorkModulCP->addItem("Мультидот");
                ui->cBWorkModulCP->addItem("Магистр. лев");
                ui->cBWorkModulCP->addItem("Магистр. лев. спрей");
                ui->cBWorkModulCP->addItem("Магистр. прав.");
                ui->cBWorkModulCP->addItem("Магистр. прав. спрей");
                ui->cBWorkModulCP->addItem("З.Р.Б");
                ui->cBWorkModulCP->addItem("400 спрей");
                break;
            case 0x02://хп
                ui->cBWorkModulCP->addItem("Без рециркуляции");
                ui->cBWorkModulCP->addItem("Рецирк. 1");
                ui->cBWorkModulCP->addItem("Рецирк. 2");
                ui->cBWorkModulCP->addItem("Спрей ХП");
                break;
            default:
                break;
            }
            ui->cBWorkModulCP->setCurrentIndex(canPort->workModul);
        }
        break;
    case TAB_SMS:
        if(canPort->reqSMS)
        {
            ui-> pBSetAdressSMS->show();
            if( tikcolor%10==0 ) bump++;
            if (bump>30)
            {
                bump=0;
                canPort->reqSMS=0;
            }
           // qDebug()<<"bump"<<bump;
        }
        else
        {
          ui-> pBSetAdressSMS->hide();
          bump=0;
        }

        if( tikcolor%10==0 ) // раз в секунду меняем  activity через 5 секунд чистим
        {
            for(int i=0;i<NUMBER_SENSOR_MOTION;i++)
            {
                if(canPort->sms[i]->activity)
                {
                    canPort->sms[i]->activity--;
                    if(canPort->sms[i]->activity==0)
                    {
                        canPort->sms[i]->isVisible=false;
                        canPort->sms[i]->hideGun();
                    }
                }
            }
        }


        break;


    case TAB_DANF_SENS:

        if( tikcolor%10==0 ) // раз в секунду меняем  activity через 5 секунд чистим
        {
            for(int i=0;i<NUMBER_SENSOR_DANFOSS;i++)
            {


                if(canPort->sensDanf[i]->activity)
                {
                    canPort->sensDanf[i]->activity--;
                    if( canPort->sensDanf[i]->activity==0)
                    {
                        canPort->sensDanf[i]->isVisible=false;
                        canPort->sensDanf[i]->hideSens();
                    }
                }
            }
        }


        break;
    default:
        break;
    }

//Эмуляторы датчика оборотов CAN
if(ui->cbMixerLeft->isChecked() && tikcolor%2==0 ) // передача каждые 200 мс
  {
    writeToCANatID(ID_SEND_MIXER_LEFT_RPM);
    //qDebug()<<"LeftRPM";
   }
if(ui->cbMixerRight->isChecked() && tikcolor%2==0)
  {
    writeToCANatID(ID_SEND_MIXER_RIGHT_RPM);
    //qDebug()<<"RightRPM";
  }
if(ui->cbMixerTank->isChecked() && tikcolor%2==0)
  {
    writeToCANatID(ID_SEND_MIXER_TANK_RPM);
    //qDebug()<<"RightRPM";
  }

if(ui->cbDrum->isChecked() && tikcolor%2==0)
  {
    writeToCANatID(ID_SEND_DRUM_RPM);
    //qDebug()<<"RightRPM";
  }

if(ui->cbClodSprayPlastcic->isChecked() && tikcolor%5==0)
  {
    writeToCANatID(ID_SEND_COLD_SPRAY);
    //qDebug()<<"RightRPM";
  }


//Эмуляторы датчика оборотов DANFOSS для магистральной
  if(ui->cbDanfos->isChecked() && tikcolor%2==0 ) // передача каждые 200 мс
  {
    writeToCANatID(ID_SEND_DANFOS_MAG_RPM);
    //qDebug()<<"LeftRPM";
    for(int i=0;i<4;i++)
    {
    if(saveValuePosGR[i]==GR_RPM_PUMP)
    realtimeDataGRAPH(i,ui->lEDanfosLeft->text().toInt());
    }
   }


  //Эмуляторы широковещательного с Ubup0 IDC2
    if(ui->cbBcUbup0->isChecked() && tikcolor%2==0 ) // передача каждые 200 мс
    {
      writeToCANatID(ID_SEND_BC_UBUP_0);
     }

    //Эмуляторы широковещательного с Ubup1 IDC2
      if(ui->cbBcUbup1->isChecked() && tikcolor%2==0 ) // передача каждые 200 мс
      {
        writeToCANatID(ID_SEND_BC_UBUP_1);
       }

      //Эмуляторы широковещательного канХаб
        if(ui->cbBcCanHub->isChecked() && tikcolor%2==0 ) // передача каждые 200 мс
        {
          writeToCANatID(ID_SEND_BC_CANHUB);
         }

//Эмуляторы параметров с МБО cBemParMbo(события)

  if(ui->cBemParMbo->isChecked() && tikcolor%2==0  ) // передача каждые 200 мс
  {
    writeToCANatID(ID_SEND_EMUL_PAR_MBO);
    //qDebug()<<"LeftRPM";
   }

  //Эмуляторы параметров с МБО cBemParMbo

    if(ui->cBemParMbo_2->isChecked() && tikcolor%2==0  ) // передача каждые 200 мс
    {
      writeToCANatID(ID_SEND_EMUL_PAR_MBO_2);
      //qDebug()<<"LeftRPM";
     }

    //Эмуляторы широковещательныx с МБО

      if(ui->cbMboBC->isChecked() && tikcolor%5==0  ) // передача каждые 500 мс
      {
        writeToCANatID(ID_SEND_EMUL_MBO_BC);
        //qDebug()<<"LeftRPM";
       }


      if(ui->cbEventLed->isChecked() && tikcolor%5==0  ) // передача каждые 500 мс
      {
        writeToCANatID(ID_SEND_EMUL_MBO_EVENT_LED);
        //qDebug()<<"LeftRPM";
       }



/*if(ui->cbWeightPlastic->isChecked() && tikcolor%10==0)// передача каждую секунду
  {
    writeToCANatID(ID_SEND_WEIGHT_PLASTIC);
    if(tikcolor%30==0) //раз в 3 секунды =100мс*30 умешаем вес
    {
        int temp= ui->lEWeightPlastic->text().toInt();
        if(temp>3)
        ui->lEWeightPlastic->setText(QString::number(temp-3));
    }

  }

if(ui->cbWeightSog->isChecked() && tikcolor%10==0)// передача каждую секунду
  {
    writeToCANatID(ID_SEND_WEIGHT_SOG);
    if(tikcolor%30==0) //раз в 3 секунды 100мс*30
    {
        int temp= ui->lEWeightSog->text().toInt();
        if(temp>0)
        ui->lEWeightSog->setText(QString::number(temp-1));
    }
  }
*/
if( tikcolor%100==0)// передача каждые 10 секунд
  {
if(canPort->mukModul==MODULE_CP){
    for(int i=0;i<4;i++)
    {
        if(saveValuePosGR[i]==GR_SET_PPL)
            realtimeDataGRAPH(i,canPort->hydroRpm[PUMPLEFT]);
        if(saveValuePosGR[i]==GR_SET_PPR)
            realtimeDataGRAPH(i,canPort->hydroRpm[PUMPRIGHT]);
        if( (saveValuePosGR[i]==GR_CANAL_L) && (!((mymik->brokenGuns>>0)&0x01))){ // не штриховая
            if((mymik->mikGunStateOld>>0)&0x01)
                realtimeDataGRAPH(i,70);
            else
                realtimeDataGRAPH(i,50);
        }
        if( (saveValuePosGR[i]==GR_CANAL_R) && (!((mymik->brokenGuns>>1)&0x01))){// не штриховая
            if((mymik->mikGunStateOld>>1)&0x01)
                realtimeDataGRAPH(i,75);
            else
                realtimeDataGRAPH(i,55);

            if(saveValuePosGR[i]==GR_HARD_L)
                realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_L].toInt());
            if(saveValuePosGR[i]==GR_HARD_R)
                realtimeDataGRAPH(i,mymik-> mukBcParams[1][BroadCastMUKParms::MUK_HARD_PERC_R].toInt());
        }

    }
} else if(canPort->mukModul==MODULE_PAINT)
        for(int i=0;i<4;i++)
        {

            if( (saveValuePosGR[i]==GR_CANAL_1) /*&& (!((canPort->brokenGuns>>0)&0x01))*/){ // не штриховая
                if((mymik->mikGunStateOld>>0)&0x01)
                    realtimeDataGRAPH(i,70);
                else
                    realtimeDataGRAPH(i,50);
            }
            if( (saveValuePosGR[i]==GR_CANAL_3) /*&& (!((canPort->brokenGuns>>2)&0x01))*/){// не штриховая
                if((mymik->mikGunStateOld>>2)&0x01)
                    realtimeDataGRAPH(i,75);
                else
                    realtimeDataGRAPH(i,55);

           }

        }

  }

 //  qDebug()<<"saveValuePosGR"<<saveValuePosGR[0]<<saveValuePosGR[1]<<saveValuePosGR[2]<<saveValuePosGR[3];
}

void MainWindow::realtimeDataSlot()
{
    // calculate two new data points:
    double key = timerG.elapsed()/1000.0; // time elapsed since start of demo, in seconds

//   qDebug()<<"keys"<<key;
    static double lastPointKey = 0;
    if (key-lastPointKey > 0.002) // at most add point every 2 ms
    {
       // qDebug()<<"keys"<<key;
      // add data to lines:
      customPlot->graph(0)->addData(key,canPort->speedMidPacketInMinute );
      customPlot->graph(1)->addData(key,canPort->speedCurPacketInMinute  );
      // rescale value (vertical) axis to fit the current data:
      customPlot->graph(0)->rescaleValueAxis();
      customPlot->graph(1)->rescaleValueAxis(true);
      lastPointKey = key;
    }
    // make key axis range scroll with the data (at a constant range size of 8):
    customPlot->xAxis->setRange(key, 50, Qt::AlignRight);
    customPlot->replot();

    // calculate frames per second:
    static double lastFpsKey;
    static int frameCount;
    ++frameCount;
    if (key-lastFpsKey > 2) // average fps over 2 seconds
    {
      ui->lbFPS->setText(QString("%1 FPS, Total Data points: %2")
            .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
            .arg(customPlot->graph(0)->data()->size()+customPlot->graph(1)->data()->size()));
      lastFpsKey = key;
      frameCount = 0;

   //  qDebug()<< customPlot->graph(1)->data()->size()<<key;
     if(customPlot->graph(1)->data()->size()>1000)
     {
      //qDebug() <<"!!!!!!" ;
      customPlot->graph(1)->data()->removeBefore(key-50);
      customPlot->graph(0)->data()->removeBefore(key-50);
     }

    }
}

void MainWindow::realtimeDataSlotGyro()
{
    // calculate two new data points:
    double key = timerG.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    static int countRefresh=0;
    //   qDebug()<<"keys"<<key;
    // static double lastPointKey = 0;
    // if (key-lastPointKey > 0.002) // at most add point every 2 ms
    //  {*/
    //  qDebug()<<"keys"<<key;
    //  qDebug()<< customPlot->graph(1)->data()->size()<<key;
    if(cPlGyro->graph(0)->data()->size()>1000)
    {
        //qDebug() <<"!!!!!!" ;
        cPlGyro->graph(0)->data()->removeBefore(key-50);
        cPlGyro->graph(1)->data()->removeBefore(key-50);
        cPlGyro->graph(2)->data()->removeBefore(key-50);
        cPlGyro->graph(3)->data()->removeBefore(key-50);
    }


    // add data to lines:
    cPlGyro->graph(0)->addData(key,canPort->gyroX );
    cPlGyro->graph(1)->addData(key,canPort->gyroY  );
    cPlGyro->graph(2)->addData(key,canPort->gyroZ  );
    cPlGyro->graph(3)->addData(key,canPort->gyroSpeed  );


    //lastPointKey = key;
    //   }
    // make key axis range scroll with the data (at a constant range size of 8):

  // Из-за зависание на нетбуке  обновление уменьшили
    countRefresh++;
    if(countRefresh>=25){
       // rescale value (vertical) axis to fit the current data:
        cPlGyro->graph(0)->rescaleValueAxis(true);
        cPlGyro->graph(1)->rescaleValueAxis(true);
        cPlGyro->graph(2)->rescaleValueAxis(true);
        cPlGyro->graph(3)->rescaleValueAxis(true);
        cPlGyro->xAxis->setRange(key, 50, Qt::AlignRight);
        cPlGyro->replot();
        countRefresh=0;
    }

}

void MainWindow::realtimeDataSlotMBO()
{

    // calculate two new data points:
    double key = timerG.elapsed()/1000.0; // time elapsed since start of demo, in seconds
//   qDebug()<<"keys"<<key;
   // static double lastPointKey = 0;
   // if (key-lastPointKey > 0.002) // at most add point every 2 ms
  //  {*/
       // qDebug()<<"keys"<<key;
      // add data to lines:
      cPlMBO->graph(0)->addData(key,canPort->leftcoord );
      cPlMBO->graph(1)->addData(key,canPort->rightcoord  );

      // rescale value (vertical) axis to fit the current data:
      cPlMBO->graph(0)->rescaleValueAxis(true);
      cPlMBO->graph(1)->rescaleValueAxis(true);
      //lastPointKey = key;
 //   }
    // make key axis range scroll with the data (at a constant range size of 8):
    cPlMBO->xAxis->setRange(key, 50, Qt::AlignRight);
    cPlMBO->replot();
}

void MainWindow::startAfterTimer()
{
    if(delayStartMg>=0){
      ui-> pBTestFisits->click();
    }
    else{
      ui-> pBStartStroke->click();
    }
}

void MainWindow::stopAfterTimer()
{
    if(delayStopMg>=0){
      ui-> pBTestOfffists->click();
    }
    else{
         ui-> pBStartSpace->click();
    }
}

void MainWindow::startValveMag()
{

    qDebug()<<"Старт клапан "<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
   //writeToCANatID(ID_SEND_START);
   ui-> pBTestFisits->click();
}

void MainWindow::stopValveMag()
{
    qDebug()<<"Стоп клапан "<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
     ui->pBTestOfffists  ->click();
}



void MainWindow::autoClick()
{
    if(!flagIsStroke)
    {
        ui->pBStartMagistral->click();
        flagIsStroke=true;
    }
    else
    {
         ui->pBStopMagistral->click();
         flagIsStroke=false;
    }
}

void MainWindow::checkTenzo()
{
    qDebug()<<"Check Tenzo";

    if(periodTimerTenzo!= ui->lePeriodTenzoMs->text().toInt())
    {
        periodTimerTenzo= ui->lePeriodTenzoMs->text().toInt();
        settings->setValue("settings/periodTimerTenzo",periodTimerTenzo);
        settings->sync(); //записываем период
    }

    if(ui->cbWeightMat1->isChecked()==true || ui->cbWeightMat2->isChecked()==true || ui->cbWeightSog->isChecked()==true)
    {
      timerTenzo->setInterval(periodTimerTenzo);
      timerTenzo->start();
    }
    else
    {
         timerTenzo->stop();
    }

}

void MainWindow::emultenzoToCAN()
{
    if(ui->cbWeightMat1->isChecked()==true)  writeToCANatID(ID_SEND_WEIGHT_MAT1);
    if(ui->cbWeightMat2->isChecked()==true)  writeToCANatID(ID_SEND_WEIGHT_MAT2);
    if(ui->cbWeightSog->isChecked()==true)   writeToCANatID(ID_SEND_WEIGHT_SOG);

}

void MainWindow::emulJoyCanLIS()
{
    //  qDebug()<<"_________________________________";

    QPushButton *button = qobject_cast<QPushButton *>(sender());
    if (button) {

        QString buttonName = button->objectName();


        if(buttonName=="pbJoyDown"|| buttonName=="pbJoyUp"|| buttonName=="pbJoyLeft"|| buttonName=="pbJoyRight")
        {
            if(button->isChecked())
            {
                if(   ui->pbJoyCenter->isChecked())
                {
                    ui->pbJoyCenter->setChecked(false);
                    if(buttonName=="pbJoyDown") writeToCANatID(ID_SEND_JOY_DOWN_PRESS);
                    if(buttonName=="pbJoyUp") writeToCANatID(ID_SEND_JOY_UP_PRESS);
                    if(buttonName=="pbJoyLeft") writeToCANatID(ID_SEND_JOY_LEFT_PRESS);
                    if(buttonName=="pbJoyRight") writeToCANatID(ID_SEND_JOY_RIGHT_PRESS);

                }else
                {
                    button->setChecked(false);
                }
            }
            else
            {
                button->setChecked(true);

            }
        }


        if (buttonName=="pbJoyCenter" )
        {

            if(button->isChecked())
            {
                if(ui->pbJoyDown->isChecked())
                {
                    ui->pbJoyDown->setChecked(false);
                    writeToCANatID(ID_SEND_JOY_DOWN_REALESE);
                }
                if(ui->pbJoyUp->isChecked())
                {
                    ui->pbJoyUp->setChecked(false);
                    writeToCANatID(ID_SEND_JOY_UP_REALESE);
                }
                if(ui->pbJoyLeft->isChecked())
                {

                    ui->pbJoyLeft->setChecked(false);
                    writeToCANatID(ID_SEND_JOY_LEFT_REALESE);
                }
                if(ui->pbJoyRight->isChecked())
                {
                    ui->pbJoyRight->setChecked(false);
                    writeToCANatID(ID_SEND_JOY_RIGHT_REALESE);
                }
            }
            else
            {
                button->setChecked(true);

            }
        }

        //   QString buttonState = button->isChecked() ? "Pressed" : "Released";
        //   qDebug() << "Button:" << buttonName << "State:" << buttonState;
    }
}

void MainWindow::makeConsuption()
{

   if(ui->lbWidthSt->text().toInt()<5) ui->lbWidthSt->setText("10");// по умолчанию


    if( (ui->cbWeightMat1->isChecked()|| ui->cbWeightMat2->isChecked()  || ui->cbWeightSog->isChecked()   )) //умный расчет расхода пластика / шариков
    {

        //Эмулятор расхода. Отнимает от текущего веса заданный расход***************************************************************
        if(mymik-> mikBcParams[1][MIK_PAR_ALL_MARK_SM].toInt()< oldSquare) oldSquare=0;
        ui->lbLenAllPistSm->setText(mymik-> mikBcParams[1][MIK_PAR_ALL_MARK_SM]);


        currSquare=mymik-> mikBcParams[1][MIK_PAR_ALL_MARK_SM].toFloat()*ui->lbWidthSt->text().toInt()/10000;
        ui->lbSquare->setText(QString::number(currSquare,'f',2));


        qDebug()<<"Длина всех пистолетов"<< mymik-> mikBcParams[1][MIK_PAR_ALL_MARK_SM]<<" дельта площадь= "<<currSquare-oldSquare;

        float temp= ui->lEWeightMat1->text().toFloat();
        qDebug()<<"Вес кг до "<<temp <<" после" <<(temp-(currSquare-oldSquare)* ui->leConsMat1->text().toFloat());
        if(temp>5 && (temp-(currSquare-oldSquare)* ui->leConsMat1->text().toFloat())>0)
            ui->lEWeightMat1->setText(QString::number(temp-(currSquare-oldSquare)* ui->leConsMat1->text().toFloat()));


        float temp3= ui->lEWeightMat2->text().toFloat();
        qDebug()<<"Вес кг до "<<temp3 <<" после" <<(temp3-(currSquare-oldSquare)* ui->leConsMat2->text().toFloat());
        if(temp3>5 && (temp3-(currSquare-oldSquare)* ui->leConsMat2->text().toFloat())>0)
            ui->lEWeightMat2->setText(QString::number(temp3-(currSquare-oldSquare)* ui->leConsMat2->text().toFloat()));


        float temp2= ui->lEWeightSog->text().toFloat();
        qDebug()<<"Вес сог кг до "<<temp2 <<" после" <<(temp2-(currSquare-oldSquare)* ui->leConsSOG->text().toFloat());
        if(temp2>5 && (temp2-(currSquare-oldSquare)* ui->leConsSOG->text().toFloat())>0)
            ui->lEWeightSog->setText(QString::number(temp2-(currSquare-oldSquare)* ui->leConsSOG->text().toFloat()));

        oldSquare= currSquare;
        //РАССЧЕТ  РАСХОДА ЗА УКАЗННЫЙ ПРОМЕЖУТОК ***************************************************************

        if(labelForCons!= mymik->mikBcParams[1][MIK_PAR_ALL_MARK_SM].toInt()/(lengForConsuption*100))
        {

            qDebug()<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!рассчет!!!!!!!!!!!!!!!!!!!!!!"<<labelForCons;




         if(currSquare< oldSquareCur) oldSquareCur=0;

          SaveWeightEnd1=SaveWeightBegin1;
          SaveWeightBegin1=CurrWeight1;

          SaveWeightEnd2=SaveWeightBegin2;
          SaveWeightBegin2=CurrWeight2;

          SaveWeightEnd3=SaveWeightBegin3;
          SaveWeightBegin3=CurrWeight3;


          qDebug()<<"currSquare"<<currSquare<<oldSquareCur;
          qDebug()<<"SaveWeightEnd1"<<SaveWeightEnd1<<"SaveWeightBegin1"<<SaveWeightBegin1;



         consCurMat1=(SaveWeightEnd1-SaveWeightBegin1)/(currSquare-oldSquareCur);
         ui->lbConsCurM1->setText(QString::number(consCurMat1,'f',2));

         consCurMat2=(SaveWeightEnd2-SaveWeightBegin2)/(currSquare-oldSquareCur);
         ui->lbConsCurM2->setText(QString::number(consCurMat2,'f',2));

         ui->lbConsCurM12->setText(QString::number(consCurMat1+ consCurMat2,'f',2));

         consCurSOG=(SaveWeightEnd3-SaveWeightBegin3)/(currSquare-oldSquareCur);
         ui->lbConsCurSog->setText(QString::number(consCurSOG,'f',2));


         labelForCons= mymik->mikBcParams[1][MIK_PAR_ALL_MARK_SM].toInt()/(lengForConsuption*100);
         oldSquareCur=currSquare;
        }


    }
}

void MainWindow::realtimeDataGRAPH(int numberGraph, int data)
{

   if(numberGraph==0 && ui->cbGraph1->isChecked()==false) return;
   if(numberGraph==1 && ui->cbGraph2->isChecked()==false) return;
   if(numberGraph==2 && ui->cbGraph3->isChecked()==false) return;
   if(numberGraph==3 && ui->cbGraph4->isChecked()==false) return;


    // calculate two new data points:
    double key = timerG.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    //qDebug()<<"realtimeDataGRAPH"<<numberGraph<<data;
   // static double lastPointKey = 0;
   // if (key-lastPointKey > 0.002) // at most add point every 2 ms
  //  {*/
       // qDebug()<<"keys"<<key;
      // add data to lines:
      cPlgraph->graph(numberGraph)->addData(key,data);
     // cPlMBO->graph(1)->addData(key,canPort->rightcoord  );
    if(ui->pBStopGr->isChecked()==true)return;//кнопка стоп
      // rescale value (vertical) axis to fit the current data:
      cPlgraph->graph(numberGraph)->rescaleValueAxis(true);
     // cPlMBO->graph(1)->rescaleValueAxis(true);
      //lastPointKey = key;
 //   }
    // make key axis range scroll with the data (at a constant range size of 8):
    cPlgraph->xAxis->setRange(key, 50, Qt::AlignRight);
    cPlgraph->replot();
}

void MainWindow::addGun()
{
   //qDebug()<<"Добавляем пистолет";
   for(int i=0;i<ALL_GUNS_PAINT;i++)
   {
       if(canPort->Paintguns[i]->lisGuns.isVisible==true ||canPort->Paintguns[i]->beadsguns.isVisible==true)
         canPort->Paintguns[i]->showGun();
       else
           canPort->Paintguns[i]->hideGun();

      // qDebug()<<"Добавляем пистолет"<<i<<(mymik->Paintguns[i]->lisGuns.isVisible==true ||mymik->Paintguns[i]->beadsguns.isVisible==true);
   }
}

void MainWindow::addmikStatToTable(int rowTable, int indData)
{
    if(rowTable>NUMBER_FOR_MIK_STATISTIC ||indData>NUMBER_FOR_MIK_STATISTIC) return;
     ui->tWMIKstat->setItem(rowTable,0, new QTableWidgetItem(QString::number(mymik->mikStat[indData].number)));
     ui->tWMIKstat->setItem(rowTable,1, new QTableWidgetItem(QString::number(mymik->mikStat[indData].type)));
     ui->tWMIKstat->setItem(rowTable,2, new QTableWidgetItem(mymik->mikStat[indData].time));
     ui->tWMIKstat->setItem(rowTable,3, new QTableWidgetItem(QString::number(mymik->mikStat[indData].width)));
     ui->tWMIKstat->setItem(rowTable,4, new QTableWidgetItem(maskToString(mymik->mikStat[indData].gunsSolid)));
     ui->tWMIKstat->setItem(rowTable,5, new QTableWidgetItem(maskToString(mymik->mikStat[indData].gunsBroke)));
     ui->tWMIKstat->setItem(rowTable,6, new QTableWidgetItem(maskToString(mymik->mikStat[indData].gunsBeads)));
     ui->tWMIKstat->setItem(rowTable,7, new QTableWidgetItem(QString::number(mymik->mikStat[indData].spaseSm)));
     ui->tWMIKstat->setItem(rowTable,8, new QTableWidgetItem(QString::number(mymik->mikStat[indData].strokeSm)));
     ui->tWMIKstat->setItem(rowTable,9, new QTableWidgetItem(QString::number(mymik->mikStat[indData].markCycle)));
     ui->tWMIKstat->setItem(rowTable,10, new QTableWidgetItem(QString::number(mymik->mikStat[indData].lenAllGunsSm)));
     ui->tWMIKstat->setItem(rowTable,11, new QTableWidgetItem(QString::number(mymik->mikStat[indData].lenStepSm)));
     //mymik->mikStat[indData].pumpCycle=15;
     ui->tWMIKstat->setItem(rowTable,12, new QTableWidgetItem(QString::number(mymik->mikStat[indData].pumpCycle)));
     //расчитываем в кг
     double r;
     r = paintDens/ 100.0 * pumpValue/ 1000.0;
     ui->tWMIKstat->setItem(rowTable,13, new QTableWidgetItem(QString::number(r*mymik->mikStat[indData].pumpCycle)));
     ui->tWMIKstat->setItem(rowTable,14, new QTableWidgetItem(QString::number(mymik->mikStat[indData].averSpeed/10.0)));
     ui->tWMIKstat->setItem(rowTable,15, new QTableWidgetItem(mymik->mikStat[indData].isGenerator?"+":"-"));
}

void MainWindow::writeToCANatID(int id)
{
    QByteArray data,idData; // Текстовая переменная
     //bool bStatus = false;
     int16_t var1 = 0;
     int16_t var1_twos_complement=0;
     int temp=0x00;
     int temp2=0x00;
    switch (id) {
    case ID_SEND_STOP:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"1";
        data =IntToByteCAN(MIK_STOP);
        break;
    case ID_SEND_START:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"1";
        data =IntToByteCAN(MIK_START);
        break;
    case ID_SEND_START_CORR:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(MIK_START)+"01";
        break;

    case ID_SEND_CLEANING_START:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(MIK_CLEAN)+"01";
        break;

    case ID_SEND_CLEANING_STOP:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(MIK_CLEAN)+"00";
        break;
    case ID_SEND_WASHING:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(MIK_WASHING)+IntToByteCAN(ui->cbWashingType->currentIndex());
        break;

    case ID_SEND_GUNS:
        switch (canPort->Paintguns[currentGun-1]->type) {
        case GUN_TYPE_NONE:
        case GUN_TYPE_LIS:
            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CONF_DIRECT)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"7";
            data =IntToByteCAN(currentGun-1)+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leS2->text().toUShort())+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leR2->text().toUShort())
                    + IntTo2ByteCANLM(canPort->Paintguns[currentGun-1]->leTi->text().toInt())+IntTo2ByteCANLM( canPort->Paintguns[currentGun-1]->leTo->text().toInt());
            emit writeCanData(idData+data); // Отправка данных в порт
            PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
            Print(idData+data); // Вывод данных в консоль

            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
            data =IntTo2ByteCANLM(COR_LIS_PARAMS+currentGun-1)+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leS2->text().toUShort())+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leR2->text().toUShort())
                    + IntTo2ByteCANLM(canPort->Paintguns[currentGun-1]->leTi->text().toInt())+IntTo2ByteCANLM( canPort->Paintguns[currentGun-1]->leTo->text().toInt());
            emit writeCanData(idData+data); // Отправка данных в порт
            PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
            Print(idData+data); // Вывод данных в консоль

            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
            data =IntTo2ByteCANLM(GUNS_SETUP+currentGun-1)+ IntTo2ByteCANLM(stringToMask(canPort->Paintguns[currentGun-1]->lebeads->text()))
                    + IntToByteCAN(canPort->Paintguns[currentGun-1]->leBase->text().toInt())+"000000";
            break;
        case GUN_TYPE_SIMPLE:
            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
            data =IntTo2ByteCANLM(COR_GUNS_PARAMS + currentGun-1)+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leTi->text().toUShort())+"0000000000";
            emit writeCanData(idData+data); // Отправка данных в порт
            Print(idData+data); // Вывод данных в консоль
            PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
            data =IntTo2ByteCANLM(GUNS_SETUP+currentGun-1)+ IntTo2ByteCANLM(stringToMask(canPort->Paintguns[currentGun-1]->lebeads->text()))
                    + IntToByteCAN(canPort->Paintguns[currentGun-1]->leBase->text().toInt())+"000000";
            break;
        case GUN_TYPE_BEADS:

            idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
            data =IntTo2ByteCANLM(BEADS_PARAMS+currentGun-1)+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leTi->text().toUShort())
                    + IntToByteCAN(canPort->Paintguns[currentGun-1]->leTo->text().toUShort())+ IntToByteCAN(canPort->Paintguns[currentGun-1]->leBase->text().toInt())+"000000";
            break;
        default:
            break;
        }
       // if(canPort->Paintguns[currentGun-1])




        break;
    case  ID_SEND_WORK_MODE:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CIKL_MODE)+ IntToByteCAN(ui->cBWorkMode->currentIndex()-1)+ IntToByteCAN(ui->lEtransitionType->text().toInt())
                + IntTo2ByteCANLM( stringToMask(ui->lESolid->text()))+IntTo2ByteCANLM( stringToMask(ui->lEBroke->text()));
        break;
    case  ID_SEND_CONF_BASE:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_BASE)+IntTo2ByteCANLM(ui->lEMarkerPath->text().toInt())
                + IntTo2ByteCANLM( ui->lEScanerPath->text().toInt())+IntTo2ByteCANLM( ui->lECorrection->text().toInt());
        break;
    case  ID_SEND_GYRO_ON:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(GYRO_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(GYRO_SWITCH)+IntToByteCAN(GYRO_ON);
        break;
    case  ID_SEND_GYRO_OFF:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(GYRO_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(GYRO_SWITCH)+IntToByteCAN(GYRO_OFF);
        break;
    case  ID_SEND_CYCLE:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_CYCLE) + IntTo2ByteCANLM(ui->LELenSpace->text().toInt()) + IntTo2ByteCANLM( ui->LELenMark->text().toInt())
                +IntToByteCAN( ui->lETolerGap->text().toInt())+IntToByteCAN( ui->lETolerMark->text().toInt());
        break;


    case ID_SEND_GUN_TEST:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_DIRECT_GUN)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"5";
        data =IntTo2ByteCANLM(MIK_DIRECT_GUN_ON)+ IntTo2ByteCANLM(1<<testGun);
        if(testGun>=16) data+=IntToByteCAN(1<<(testGun-16));
        else data+="00";
        break;

    case ID_SEND_GUN_TEST_MASK:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_DIRECT_GUN)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"5";
        data =IntTo2ByteCANLM(MIK_DIRECT_GUN_ON)+ IntTo2ByteCANLM(maskTestGun);
        if(maskTestGun>=16) data+=IntToByteCAN(maskTestGun>>16);
        else data+="00";
        break;
    case ID_SEND_STOP_GUN_TEST:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_DIRECT_GUN)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntTo2ByteCANLM(MIK_DIRECT_GUN_OFF);
        break;

    case ID_SEND_READ_MODE_ADC:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_ADC_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"1";
        data =IntToByteCAN(ui->cbModeADC->currentIndex());
        break;

    case ID_SEND_SPEED_AUTO:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"4";
        data =IntToByteCAN(MIK_SPEED_GEN);
        if(ui->pbSpeedAuto->isChecked())
        {
          data+="01"+ IntTo2ByteCANLM(ui->leSpeedAuto->text().toInt());
        }
        else{
          data+="000000";
        }
        break;
    case ID_SEND_READ_DATA_MIK:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_READ_DATA)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"0";
        //data =IntTo2ByteCANLM(MIK_DIRECT_GUN_OFF);
        break;
    case ID_WRITE_EEP_PERC_LED_OFF:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD_WRITE_DATA_EEP)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"6";
        data =IntTo2ByteCANLM(MIK_DATA_PERC_OFF)+IntTo2ByteCANLM(ui->lEPercoff->text().toLong()&0xffff)+IntTo2ByteCANLM((ui->lEPercoff->text().toLong()>>16)&0xffff);
        break;
    case ID_WRITE_EEPROM_LR:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD_WRITE_DATA_EEP)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"6";
        data =IntTo2ByteCANLM(MIK_DATA_LR)+IntTo2ByteCANLM(ui->lELr->text().toLong()&0xffff)+IntTo2ByteCANLM((ui->lELr->text().toLong()>>16)&0xffff);
        break;
    case ID_WRITE_EEPROM_LMARK:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD_WRITE_DATA_EEP)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"6";
        data =IntTo2ByteCANLM(MIK_DATA_LMARK)+IntTo2ByteCANLM(ui->lELmark->text().toLong()&0xffff)+IntTo2ByteCANLM((ui->lELmark->text().toLong()>>16)&0xffff);
        break;
    case ID_WRITE_EEPROM_PERC_DET:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MIK_CMD_WRITE_DATA_EEP)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"6";
        data =IntTo2ByteCANLM(MIK_DATA_PERC_DET)+IntTo2ByteCANLM(ui->lEPercDet->text().toLong()&0xffff)+"0000";
        break;
    case ID_SEND_START_CYCLE:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING_START)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_FAKE_TERMINAL)+"2";
        data ="02"+IntToByteCAN(calcComandInCycle());
        break;
    case ID_SEND_STANDART:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_FOR_STATISTIC) + IntTo2ByteCANLM(mymik->standart&0xffff) + IntTo2ByteCANLM(( mymik->standart>>16)&0xffff)
                +IntToByteCAN( mymik->modul)+"00";
        break;
    case ID_SEND_CYCLE_STOP:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_FAKE_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CIKL_MARKING_STOP) +"000000000000";
        break;
    case ID_SEND_SETUP_MODUL:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_MODUL) +IntToByteCAN(ui->cBWorkModul->currentIndex())+IntToByteCAN(ui->cBSpeedSource->currentIndex())+IntTo2ByteCANLM(ui->lEcoefSpSens->text().toInt())+"0000";
        break;
    case ID_SEND_SETUP_MODUL_CP:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_MODUL) +IntToByteCAN(ui->cBWorkModulCP->currentIndex())+IntToByteCAN(ui->cBSpeedSourceCP->currentIndex())
                +IntTo2ByteCANLM(ui->lEcoefSpSensCP->text().toInt())+IntToByteCAN(ui->cbLineTypeCP->currentIndex())+  IntToByteCAN(ui->lEdistBetweenDots->text().toInt());
        break;

    case ID_SEND_MIXER_LEFT_RPM:

        idData ="18FF20C28";
         var1 = ui->lEMixerLeft->text().toInt();
         var1_twos_complement = ~(var1*10) + 1;
        data ="000000"+IntTo2ByteCANLM(var1_twos_complement)+"000000";
        break;
    case ID_SEND_MIXER_RIGHT_RPM:

        idData ="18FF20C38";
        var1 = ui->lEMixerRight->text().toInt();
        var1_twos_complement = ~(var1*10) + 1;
        data ="000000"+IntTo2ByteCANLM(var1_twos_complement)+"000000";
        break;
    case ID_SEND_MIXER_TANK_RPM:
        idData ="18FF20C58";
        data ="000000"+IntTo2ByteCANLM(ui->lEMixerTank->text().toInt())+"000000";
        break;
    case ID_SEND_DRUM_RPM:
        idData ="18FF20C48";
        data ="000000"+IntTo2ByteCANLM(ui->lEDrumCan->text().toInt())+"000000";
        break;
    case ID_SEND_DANFOS_MAG_RPM:
        idData ="1CF8FFD18";
        data =IntTo2ByteCANLM(ui->lEDanfosLeft->text().toInt())+IntTo2ByteCANLM(ui->lEDanfosRight->text().toInt())+"00000000";
        break;
    case ID_SEND_SET_RPM_MIXL:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(MIXLEFT)+IntToByteCAN(ui->cbWorkModeLM->currentIndex())+IntToByteCAN(ui->cbDirectionLM->currentIndex())+IntTo2ByteCANLM(ui->lERPMLM->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_MIXR:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(MIXRIGHT)+IntToByteCAN(ui->cbWorkModeRM->currentIndex())+IntToByteCAN(ui->cbDirectionRM->currentIndex())+IntTo2ByteCANLM(ui->lERPMRM->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_PUMPL:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(PUMPLEFT)+IntToByteCAN(ui->cbWorkModeLP->currentIndex())+IntToByteCAN(ui->cbDirectionLP->currentIndex())+IntTo2ByteCANLM(ui->lERPMLP->text().toInt())+"00";
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
        //Print(idData+data); // Вывод данных в консоль
        //задержки насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(PPLL)+IntTo2ByteCANLM(ui->lbPPLon->text().toInt())+IntTo2ByteCANLM(ui->lbPPLoff->text().toInt())+"00";
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
        //толчек насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(PPPUSH)+IntTo2ByteCANLM(ui->lbPushAmplitude->text().toInt())+IntTo2ByteCANLM(ui->lbPushDelay->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_PUMPR:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(PUMPRIGHT)+IntToByteCAN(ui->cbWorkModeRP->currentIndex())+IntToByteCAN(ui->cbDirectionRP->currentIndex())+IntTo2ByteCANLM(ui->lERPMRP->text().toInt())+"00";
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
        //Print(idData+data); // Вывод данных в консоль
        //задержки насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(PPLR)+IntTo2ByteCANLM(ui->lbPPRon->text().toInt())+IntTo2ByteCANLM(ui->lbPPRoff->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_HARDL:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(HARDLEFT)+"0300"+IntTo2ByteCANLM(ui->lERPMLH->text().toInt())+"00";
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
        //Print(idData+data); // Вывод данных в консоль
        //задержки насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(PHDL)+IntTo2ByteCANLM(ui->lbPHDLon->text().toInt())+IntTo2ByteCANLM(ui->lbPHDLoff->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_HARDR:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(HARDRIGHT)+"0300"+IntTo2ByteCANLM(ui->lERPMRH->text().toInt())+"00";
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
        //Print(idData+data); // Вывод данных в консоль
        //задержки насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(PHDR)+IntTo2ByteCANLM(ui->lbPHDRon->text().toInt())+IntTo2ByteCANLM(ui->lbPHDRoff->text().toInt())+"00";
        break;
    case ID_SEND_SET_RPM_PUMP_HP:
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(SCREW)+IntToByteCAN(ui->cbWorkModePHP->currentIndex())+IntToByteCAN(ui->cbDirectionPHP->currentIndex())+IntTo2ByteCANLM(ui->lERPMPHP->text().toInt())+IntToByteCAN(ui->lEWidthPHP->text().toInt());
        break;
    case ID_SEND_WEIGHT_MAT1:
        temp2=ui->leNoiseM1->text().toInt();
        if(temp2)
        temp = QRandomGenerator::global()->bounded(-temp2, temp2+1);
        else
        temp=0;
        CurrWeight1=(int)ui->lEWeightMat1->text().toFloat()+temp;

        idData ="1CF"+ui->leCanMat1->text().toUtf8()+"FFC03";//1cf0ffc0
        data =IntTo2ByteCANLM(CurrWeight1)+"00";

        DeltaWeight1=SaveWeight1-CurrWeight1;
        ui->lbDeltaW1->setText(QString::number(DeltaWeight1));
        consTotMat1=DeltaWeight1/currSquare;

        ui->lbConsTotalM1->setText(QString::number(consTotMat1,'f',2));
        if(consTotMat1>0  &&consTotMat1<20)
        ui->lbConsTotalM1F->setText(QString::number(kalman_filter(consTotMat1),'f',2));
        break;
    case ID_SEND_WEIGHT_MAT2:

        temp2=ui->leNoiseM2->text().toInt();
        if(temp2)
        temp = QRandomGenerator::global()->bounded(-temp2, temp2+1);
        else
        temp=0;

        CurrWeight2=(int)ui->lEWeightMat2->text().toFloat()+temp;

        idData ="1CF"+ui->leCanMat2->text().toUtf8()+"FFC03";//1cf0ffc0
        data =IntTo2ByteCANLM(CurrWeight2)+"00";
        DeltaWeight2=SaveWeight2-CurrWeight2;
        consTotMat2=DeltaWeight2/currSquare;

        ui->lbDeltaW2->setText(QString::number(DeltaWeight2));
        ui->lbDeltaW12->setText(QString::number(DeltaWeight1+DeltaWeight2));

        ui->lbConsTotalM2->setText(QString::number(consTotMat2,'f',2));
        ui->lbConsTotalM12->setText(QString::number(consTotMat1+consTotMat2,'f',2));
        break;
    case ID_SEND_WEIGHT_SOG:


        temp2=ui->leNoiseSOG->text().toInt();
        if(temp2)
        temp = QRandomGenerator::global()->bounded(-temp2, temp2+1);
        else
        temp=0;

        CurrWeight3=(int)ui->lEWeightSog->text().toFloat()+temp;

        idData ="1CF"+ui->leCanSog->text().toUtf8()+"FFC03";//
        data =IntTo2ByteCANLM(CurrWeight3)+"00";

        DeltaWeight3=SaveWeight3-CurrWeight3;
        ui->lbDeltaWSog->setText(QString::number(DeltaWeight3));
        consTotSOG=DeltaWeight3/currSquare;
        ui->lbConsTotalSog->setText(QString::number(consTotSOG,'f',2));

        break;
    case ID_SEND_LIMIT_HARDENER:
        //толчек насоса
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_DELAYS_CP)+IntToByteCAN(HARDSENSORS)+IntTo2ByteCANLM(ui->lbFlowLimit->text().toInt())+IntTo2ByteCANLM(ui->lbFlowDelay->text().toInt())+"00";
        break;
    case ID_SEND_READ_MBO:
        //чтение типа  качества
        idData =IntToByteCAN(DATA_REQ_MSG)+IntToByteCAN(0xDD)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(0x04)+IntToByteCAN(0x00);
        emit writeCanData(idData+data); // Отправка данных в порт
        PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
         //чтение параметров качества
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"3";
        data =IntToByteCAN(0x25)+IntToByteCAN(0x00)+"00";
        break;

    case ID_SEND_WRITE_MBO_1:
        //записи котрастности параметров
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"3";
        data =IntToByteCAN(0x25)+IntToByteCAN(0x01)+IntToByteCAN(ui->lEContrast->text().toInt());
        break;
    case ID_SEND_WRITE_MBO_2:
        //записи износ параметров
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"3";
        data =IntToByteCAN(0x25)+IntToByteCAN(0x02)+IntToByteCAN(ui->lEIznos->text().toInt());
        break;
    case ID_SEND_WRITE_MBO_3:
        //записи шум параметров
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"3";
        data =IntToByteCAN(0x25)+IntToByteCAN(0x03)+IntToByteCAN(ui->lEShum->text().toInt());
        break;


    case ID_SEND_WRITE_MBO_QUALITY:
        //записи параметра качества
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(0xDD)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_TERMINAL)+"6";
        data =IntTo2ByteCANLM(0x04)+IntTo2ByteCANLM(ui->lEQuality->text().toInt())+"0000";
        break;

    case ID_SEND_EMUL_PAR_MBO:
        //записи параметра качества
        idData ="08A9FF506";
        data ="645A50463C32";
        break;
    case ID_SEND_EMUL_PAR_MBO_2:
        //записи параметра качества
        idData ="0C5020504";
        data ="00014F00";
        break;
    case ID_SEND_EMUL_PAR_MBO_3:
        //записи параметра качества
        idData ="18DD20506";
        data ="0B0000000000";
        break;
    case ID_SEND_EMUL_MBO_BC:
        // эмуляция ШВ №01
        /*

    [0 .. 1] биты состояния работы бос/мбо;
    - 0х0001 – состояние связи со сканером (1 или 0);
    - 0х0002 – включен или выключен лазер (1 или 0);
    - 0х0004 – включена или выключена запись данных со сканера (1 или 0);
- 0х0008 – включена или выключена  блокировка данных наведения привода (1 или 0);
    - 0х0010 – есть подключение к мик для получения параметров (1 или 0);
    - 0х0020 – подключение гироскопа (1 или 0);
- 0х0040 – есть данные по линиям от видеокамеры (1 или 0);
      -0x0080-запись результатов обработки сканера
      -0x0100-видеокамера передает данные по линиям
 - 0x0200   -  включена запись данных сканера в энергонезависимую память
- 0х0400 – подключение через удаленный терминал;
- 0х0800 – статус открытия видеоустройства;
- 0х1000 – подключен удаленный терминал через Web;
- 0х2000 – идет копирование сырых данных сканера;
- 0x4000  - включена автоматическая настройка видео;
- 0x8000  - состояние записи  видео;
    [2 .. 3] – координата X1 сканера;
    [4 .. 5] – координата X2 сканера;
    [6] – количество линий на входе мбо;
    [7] - битовое поле состояний LED.

        */
        idData ="1C01FF508";

        if(ui->cbStateScaner->isChecked())
         temp|=0x01;

        if(ui->cbStateLaser->isChecked())
         temp|=0x02 ;


      //  data ="FF22334455667788";  IntTo2ByteCANLM(ui->lEPercoff->text().toLong()&0xffff
        data =IntTo2ByteCANLM(temp) + "000000000000";
       emit writeCanData(idData+data); // Отправка данных в порт
        // эмуляция ШВ №04
        /*

        [0…1] – заданная ширина линии;
        [2…3] – фактическая задержка включения пистолета;
        [4…7] – длина штриха ,см.

        */
        idData ="1C04FF508";
      //  data ="FF22334455667788";  IntTo2ByteCANLM(ui->lEPercoff->text().toLong()&0xffff
        data =IntTo2ByteCANLM(ui->lESetWidth->text().toInt()) + IntTo2ByteCANLM(ui->lEXDelSwPist->text().toInt()) +IntTo2ByteCANLM(ui->lELenStroke->text().toLong()&0xffff)+IntTo2ByteCANLM((ui->lELenStroke->text().toLong()>>16)&0xffff);
       emit writeCanData(idData+data); // Отправка данных в порт

        // эмуляция ШВ №02
        /*

        [0] – номер работающего фильтра обработки наведения;
        [1 .. 2] – целевая координата X1 привода;
        [3 .. 4] – целевая координата X2 привода;
        [5...6] – количество переключений LED после нажатия кнопки старт
        [7] – текущая чувствительность сканера
        */

       idData ="1C02FF508";
       data =IntToByteCAN(ui->lENumFiltr->text().toInt()) + IntTo2ByteCANLM(ui->lEX1->text().toInt()) + IntTo2ByteCANLM(ui->lEX2->text().toInt())+IntTo2ByteCAN(ui->lESwitchLed->text().toInt())+IntToByteCAN(ui->leQuality2->text().toInt());
        break;
    case ID_SEND_EMUL_MBO_EVENT_LED:
        //записи параметра качества
        idData ="08AAFF506";
        if(ui->cbRaccoonError->isChecked())
        temp|=0x01;
        data =IntTo2ByteCANLM(ui->leLed0->text().toInt())+IntTo2ByteCANLM(ui->lEfreq->text().toInt())+IntTo2ByteCANLM(temp);
        break;
    case ID_SEND_ENABLE_BLOCK:
        //разрешение работы левого блока для магистральной
        idData ="1CEAFFC07";
        data ="000000000000";
        temp=0x00;
        if (ui->cbEnableLBlock->isChecked())
             temp+=0x08;
        if (ui->cbEnableRBlock->isChecked())
             temp+=0x10;
        data.append(QString("%1").arg(temp,2,16,QChar('0')));
        break;
    case ID_SEND_START_STROKE:
        // шим штриха
        temp=0x00;
        idData ="04F5D1C03";
        data ="";
        if (ui->rBLeftBlock->isChecked())
             temp+=0x01;
        else
             temp+=0x02;

        if (ui->rBonCW->isChecked())
            ;
            else
             temp+=0x08;

        data.append(QString("%1").arg(temp,2,16,QChar('0')));
        data.append(IntTo2ByteCANLM(ui->lePwmStroke->text().toInt()));
        break;

    case ID_SEND_START_SPACE:
        // шим пробела
        temp=0x00;
        idData ="04F5D1C03";
        data ="";
        if (ui->rBLeftBlock->isChecked())
             temp+=0x01;
        else
             temp+=0x02;

       // qDebug()<< "retruct"<< ui->lePwmSpace->text().toInt();
        if (ui->rBonCW->isChecked())
        {
            if (ui->lePwmSpace->text().toInt()<0)
                temp+=0x08;
        }

        else
        {
            if (ui->lePwmSpace->text().toInt()<0)
                temp+=0x00;
            else
                temp+=0x08;
        }

        data.append(QString("%1").arg(temp,2,16,QChar('0')));
        data.append(IntTo2ByteCANLM(abs(ui->lePwmSpace->text().toInt())));
        break;


    case ID_SEND_PID_MAGISTRAL:
        //   режим работы и пид для магистральной
        temp=0x00;
        idData ="04F4D1C08";
        data =IntTo2ByteCANLM(ui->lePidP->text().toInt())+IntTo2ByteCANLM(ui->lePidI->text().toInt())+IntTo2ByteCANLM(ui->lePidD->text().toInt())
                + IntToByteCAN(ui->leimpRPM->text().toInt());
        if (ui->rBLeftBlock->isChecked())
             temp+=0x01;
        else
             temp+=0x02;

        if (ui->rBPWM->isChecked())
            temp+=0x18;
            else if (ui->rBPID->isChecked())
             temp+=0x08;

        data.append(QString("%1").arg(temp,2,16,QChar('0')));
        break;
    case ID_SEND_SWITCH_ON_LOG:
         //чтение параметров качества
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(0x02)+IntToByteCAN(0x01);
        break;
    case ID_SEND_SWITCH_OFF_LOG:
         //чтение параметров качества
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(MBO_COMMAND)+IntToByteCAN(CAN_ADR_MBO)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(0x02)+IntToByteCAN(0x00);
        break;
    case ID_SEND_SWITCH_ON_TEST_VALVES:
         //чтение параметров качества
        idData =IntToByteCAN(CMD_MSG)+IntToByteCAN(CIKL_MARKING)+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+IntToByteCAN(CAN_ADR_TERMINAL)+"8";
        data =IntTo2ByteCANLM(CONF_HYDRO_RPM)+IntToByteCAN(SCREW)+IntToByteCAN(ui->cbWorkModePHP->currentIndex())+IntToByteCAN(ui->cbDirectionPHP->currentIndex())+IntTo2ByteCANLM(ui->lERPMPHP->text().toInt())+IntToByteCAN(ui->lEWidthPHP->text().toInt());
        break;

    case ID_SEND_COLD_SPRAY:
        temp=0x00;
        if (ui->cbLsSolventUp->isChecked())
             temp+=0x01;
        if (ui->cbLsSolventDown->isChecked())
             temp+=0x02;
        idData =IntToByteCAN(BROADC_DATA)+IntToByteCAN(0x09)+"FF"+IntToByteCAN(CAN_ADR_MIK+canPort->mikAdr)+"8";
        data=  IntTo2ByteCANLM(ui->lePressurePlastic->text().toInt())+IntTo2ByteCANLM(ui->lePressureHardener->text().toInt())
                +IntTo2ByteCANLM(ui->lePressureSolvent->text().toInt());
        data.append( QString("%1").arg(temp,2,16,QChar('0')));
        data.append( "00");
        break;

    case ID_SEND_TP_CLEAN_ON:
         //кнопка прочистки нажата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(TP_KEY_CLEAN);
        break;
    case ID_SEND_TP_CLEAN_OFF:
         //кнопка прочистки нажата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(TP_KEY_CLEAN);
        break;

    case ID_SEND_TP_PREVIOUS:
         //кнопка пред параметр
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(TP_KEY_PREV_PARAM);
        break;
    case ID_SEND_TP_NEXT:
         //кнопка след параметр
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(TP_KEY_NEXT_PARAM);
        break;

    case ID_SEND_TP_ENCODER:
         //кнопка след параметр
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        if(techTerminalEncoder < 0)
        data =IntToByteCAN(EVENT_ENCODER_LEFT)+IntToByteCAN(qAbs(techTerminalEncoder));
        else if(techTerminalEncoder > 0)
        data =IntToByteCAN(EVENT_ENCODER_RIGHT)+IntToByteCAN(techTerminalEncoder);
        else  if(techTerminalEncoder == 0) return;
        techTerminalEncoder=0;
        break;

    case ID_SEND_BC_UBUP_0:
         //отправка ШВ от ubup0
        idData =IntToByteCAN(BROADC_DATA)+"02"+"FF"+IntToByteCAN(CAN_ADR_UBUP)+"8";
        //1c02ff10 dlc:8 data: 0x64 0x00 0x64 0x00 0x64 0x00 0x64 0x00
        temp= ui->hSCoordUbup0->value();
        ui->lbCoord0->setText("Положение каретки: "+QString::number(temp));
        data =IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp);
        break;
    case ID_SEND_BC_UBUP_1:
         //отправка ШВ от ubup0
        idData =IntToByteCAN(BROADC_DATA)+"02"+"FF"+IntToByteCAN(CAN_ADR_UBUP+1)+"8";
        //1c02ff10 dlc:8 data: 0x64 0x00 0x64 0x00 0x64 0x00 0x64 0x00
        temp= ui->hSCoordUbup1->value();
        ui->lbCoord1->setText("Положение каретки: "+QString::number(temp));
        data =IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp)+IntTo2ByteCANLM(temp);
        break;
    case ID_SEND_BC_CANHUB:
         //отправка ШВ от CANHUB
//        /0x1C 01 FF 60
        idData =IntToByteCAN(BROADC_DATA)+"01"+"FF"+IntToByteCAN(CAN_ADR_CANHUB)+"1";
        //1c02ff10 dlc:8 data: 0x64 0x00 0x64 0x00 0x64 0x00 0x64 0x00
        data ="00";
        break;

    case ID_SEND_SMS_ADRES_NEW:
         //отправка адреса ддс
        idData =IntToByteCAN(CMD_MSG)+"11"+"80"+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(ui->sBNumberSOG->value())+IntToByteCAN(ui->sBNewAddr->value()+0x80);
        break;

    case ID_SEND_DANF_ADRES_NEW:
         //отправка адреса данфосу
        idData = "1BC788"+IntToByteCAN(ui->leCurrAddres->text().toInt())+"5";
        data ="042E0101"+IntToByteCAN(ui->sBNewAddr_2->value());
        QMessageBox::information(nullptr, "Важная информация", "Выключите и включите питание датчика, чтобы применить новый адрес!");
        break;
    case ID_SEND_SPEED_DANFOSS:
         //отправка скорости 200ms
        idData = "1BC788"+IntToByteCAN(currSensDanf)+"5";
        data ="042E010307";
        QMessageBox::information(nullptr, "Информация", "Период  передачи 200 мс для датчика " +QString::number(currSensDanf) + " установлен!");
        break;

    case ID_SEND_SMS_CH_ADRES:
         //смена адреса ддс
        idData =IntToByteCAN(CMD_MSG)+"11"+IntToByteCAN(currentSensor+0x80)+IntToByteCAN(CAN_ADR_TERMINAL)+"2";
        data =IntToByteCAN(ui->sBNumberSOG->value())+IntToByteCAN(canPort->sms[currentSensor]->sbNewAdress->value() +0x80);
        break;

    case ID_SEND_SMS_REQ_EEP:
         //отправка запроса EEPROM ддс
        idData =IntToByteCAN(CMD_MSG)+"12"+IntToByteCAN(currentSensor+0x80)+IntToByteCAN(CAN_ADR_TERMINAL)+"0";
        break;

    case ID_SEND_SMS_REQ_ALARM:
         //отправка запроса set Alarm ддс
        if(canPort->sms[currentSensor]->alarm)
            idData =IntToByteCAN(CMD_MSG)+"02"+IntToByteCAN(currentSensor+0x80)+IntToByteCAN(CAN_ADR_TERMINAL)+"0";//отключить alarm
        else
            idData =IntToByteCAN(CMD_MSG)+"01"+IntToByteCAN(currentSensor+0x80)+IntToByteCAN(CAN_ADR_TERMINAL)+"0";  //включить alarm

        break;
    case ID_VERS_CAN2CAN:
         //запрос верси CAN2CAN
        data ="vx";
        data [1] = 13;
        writeData(data); // Отправка данных в порт
        Print(data); // Вывод данных в консоль
        data ="Vx";
        data [1] = 13;
        writeData(data); // Отправка данных в порт
        Print(data); // Вывод данных в консоль
        return;
        break;

    case ID_SET_MODE_CAN2CAN:
        //установка режима CAN2CAN
        temp=0;
        idData =IntToByteCAN(CMD_MSG)+"0261"+IntToByteCAN(CAN_ADR_FAKE_TERMINAL)+"1";
        if(ui->rbModeCan1->isChecked())temp|=1;
        if(ui->rbModeCan2->isChecked())temp|=2;
        if(ui->rbModeCan1ToCan2->isChecked())temp|=16;
        if(ui->cbStatusC2C->isChecked())temp|=128;

        data =IntToByteCAN(temp);
      //  qDebug()<< "ID_SET_MODE_CAN2CAN"<<idData<<" "<<data;
        break;

    case ID_SEND_FAKE_QUAL:
        temp=0;
        idData ="18DD"+IntToByteCAN(CAN_ADR_TERMINAL)+IntToByteCAN(CAN_ADR_MBO)+"6";
        data =IntTo2ByteCANLM(0x04)+IntTo2ByteCANLM(ui->leQuality2->text().toInt())+"0000";
        qDebug()<< "ID_SEND_FAKE_QUAL"<<idData<<" "<<data;
        break;

    case ID_SEND_JOY_DOWN_PRESS:
         //кнопка джойстик вниз
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(JOY_DOWN);
        break;
    case ID_SEND_JOY_DOWN_REALESE:
         //кнопка джойстик вниз отжата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(JOY_DOWN);
        break;

    case ID_SEND_JOY_UP_PRESS:
         //кнопка джойстик вверх
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(JOY_UP);
        break;
    case ID_SEND_JOY_UP_REALESE:
         //кнопка джойстик вверх отжата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(JOY_UP);
        break;

    case ID_SEND_JOY_LEFT_PRESS:
         //кнопка джойстик влево
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(JOY_LEFT);
        break;
    case ID_SEND_JOY_LEFT_REALESE:
         //кнопка джойстик влево отжата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(JOY_LEFT);
        break;

    case ID_SEND_JOY_RIGHT_PRESS:
         //кнопка джойстик вправо
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(JOY_RIGHT);
        break;
    case ID_SEND_JOY_RIGHT_REALESE:
         //кнопка джойстик вправо отжата
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(JOY_RIGHT);
        break;

    case ID_SEND_SPEED_PRESS:
         //кнопка черепаха
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_PRESS)+IntToByteCAN(PB_SPEED);
        break;
    case ID_SEND_SPEED_REALESE:
         //кнопка зайчик
        idData =IntToByteCAN(EVENT_MSG)+IntToByteCAN(EVENT_KEYS_FROM_TP)+"FF"+IntToByteCAN(CAN_ADR_TECH_TERMINAL)+"2";
        data =IntToByteCAN(EVENT_KEY_UNPRESS)+IntToByteCAN(PB_SPEED);
        break;


    default:
        qDebug()<<"Неизвестная команда для записи id="<<id;
        return;
    }

    emit writeCanData(idData+data); // Отправка данных в порт
    PrintLog(CAN_ADR_FAKE_TERMINAL,CAN_ADR_MIK+canPort->mikAdr,0,QString(idData.insert(8," ")+" "+data),false); // Вывод данных в лог
}

void MainWindow::writeMarkCycle()
{
    //Отправка команды
    writeToCANatID(ID_SEND_WORK_MODE);
    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
        if(canPort->Paintguns[i]->lisGuns.isVisible || canPort->Paintguns[i]->beadsguns.isVisible )
       {
            currentGun=i+1;
            writeToCANatID(ID_SEND_GUNS);
       }
    }
    writeToCANatID(ID_SEND_CYCLE);
    writeToCANatID(ID_SEND_STANDART);
    writeToCANatID(ID_SEND_CONF_BASE);
    if(canPort->mukModul==MODULE_CP)
    writeToCANatID(ID_SEND_SETUP_MODUL_CP);
    else
    writeToCANatID(ID_SEND_SETUP_MODUL);
    writeToCANatID(ID_SEND_CYCLE_STOP);

}

int MainWindow::calcComandInCycle()
{
    int cmdCount=0;

    //ID_SEND_WORK_MODE  00
    cmdCount++;

   //если меняем пистолеты
    int tempworkGuns=stringToMask(ui->lESolid->text())|stringToMask(ui->lEBroke->text());
    int tempbeadGuns=0;
    qDebug()<<"tempworkGuns:"<<tempworkGuns;
    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
        if(tempworkGuns&(0x01<<i)){
             canPort->Paintguns[i]->lisGuns.isVisible =true;
             canPort->Paintguns[i]->showGun();
             if(ui->cBWorkMode->currentIndex()-1==WM_AUT0 )canPort->Paintguns[i]->type=GUN_TYPE_SIMPLE;
             tempbeadGuns=tempbeadGuns|stringToMask(canPort->Paintguns[i]->lebeads->text());
        }
        else
        {
             canPort->Paintguns[i]->lisGuns.isVisible =false;
             canPort->Paintguns[i]->hideGun();
        }
    }

    //  writeToCANatID(ID_SEND_GUNS);
    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
        if(canPort->Paintguns[i]->lisGuns.isVisible )
        {

            switch (canPort->Paintguns[i]->type) {
            case GUN_TYPE_SIMPLE:
                cmdCount+=2;
                break;
            case GUN_TYPE_LIS:
                cmdCount+=2;
                break;
            case GUN_TYPE_BEADS:
                cmdCount+=1;
                break;
            }
        }


        if((tempbeadGuns>>i)&0x01 )
        {
            canPort->Paintguns[i]->beadsguns.isVisible=true;
            canPort->Paintguns[i]->setBeadsGuns();
            canPort->Paintguns[i]->showGun();
            canPort->Paintguns[i]->type=GUN_TYPE_BEADS;
            cmdCount+=1;
        }
        else{
          canPort->Paintguns[i]->beadsguns.isVisible=false;
         // canPort->Paintguns[i]->type=GUN_TYPE_NONE;
        }

    }

//qDebug()<<"tempbeadGuns"<<tempbeadGuns;


    //ID_SEND_CYLE 41
    cmdCount++;

    //ID_SEND_STANDART
     cmdCount++;
     //ID_SEND_CONF_BASE
      cmdCount++;

    //ID_SEND_SETUP_MODUL
      cmdCount++;

     //ID_SEND_CYCLE_STOP
      cmdCount++;

 /* 04614020 8 0000000100000100 - Пульт
   04614020 8 4200040B00000200 - Пульт
   04614020 8 440000000F000000 - Пульт
   04614020 8 4800000100000000 - Пульт
   04614020 8 4800010000000000 - Пульт
   04614020 8 4800020001000000 - Пульт
   04614020 8 430000005A00E803 - Пульт
   04614020 8 5500000000000000 - Пульт*/

    qDebug()<<"cmdCount"<<cmdCount;
    return cmdCount;
}

void MainWindow::refreshModuleState(int state)
{
//qDebug()<<"state"<<state<<((statusCAN>>STATUS_TERMINAL)&0x01)<<((state>>STATUS_TERMINAL) &0x01);

    if( ((statusCAN>>STATUS_TERMINAL) &0x01)!= ((state>>STATUS_TERMINAL) &0x01))
    {
        if( (state>>STATUS_TERMINAL) &0x01)
            ui->lbCANStatusTerminal->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        else
            ui->lbCANStatusTerminal->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }

    if( ui->radioButton->isChecked())
    {
        if( ((statusCAN>>STATUS_MIK_0) &0x01)!= ((state>>STATUS_MIK_0) &0x01))
        {
            if( (state>>STATUS_MIK_0) &0x01){
                ui->lbCANStatusMik->setPixmap(QPixmap("://PNG/CAN_OK.png"));
            }
            else
                ui->lbCANStatusMik->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
        }
    }
    else
    {
        if( ((statusCAN>>STATUS_MIK_1) &0x01)!= ((state>>STATUS_MIK_1) &0x01))
        {
            if( (state>>STATUS_MIK_1) &0x01){
                ui->lbCANStatusMik->setPixmap(QPixmap("://PNG/CAN_OK.png"));
            }
            else
                ui->lbCANStatusMik->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
        }

    }

    if( ((statusCAN>>STATUS_GYRO) &0x01)!= ((state>>STATUS_GYRO) &0x01))
    {
        if( (state>>STATUS_GYRO) &0x01){
            ui->lbCANStatusGyro->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        }
        else
            ui->lbCANStatusGyro->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }

    if( ((statusCAN>>STATUS_UBUP) &0x01)!= ((state>>STATUS_UBUP) &0x01))
    {
        if( (state>>STATUS_UBUP) &0x01){
            ui->lbCANStatusUbup->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        }
        else
            ui->lbCANStatusUbup->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }

    if( ((statusCAN>>STATUS_MBO) &0x01)!= ((state>>STATUS_MBO) &0x01))
    {
        if( (state>>STATUS_MBO) &0x01){
            ui->lbCANStatusMBO->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        }
        else
            ui->lbCANStatusMBO->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }

    if( ((statusCAN>>STATUS_SHNEIDER) &0x01)!= ((state>>STATUS_SHNEIDER) &0x01))
    {
        if( (state>>STATUS_SHNEIDER) &0x01){
            ui->lbCANStatusShneider->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        }
        else
            ui->lbCANStatusShneider->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }

    if( ((statusCAN>>STATUS_CANHUB) &0x01)!= ((state>>STATUS_CANHUB) &0x01))
    {
        if( (state>>STATUS_CANHUB) &0x01){
            ui->lbCANStatusCanHub->setPixmap(QPixmap("://PNG/CAN_OK.png"));
        }
        else
            ui->lbCANStatusCanHub->setPixmap(QPixmap("://PNG/CAN_ERR.png"));
    }
    statusCAN=state;
}

void MainWindow::setColorLineEdit(QLineEdit *lE, int number)
{
    switch (number) {
    case 5:
       lE->setStyleSheet(" background-color: #FF0000");
        break;
    case 4:
         lE->setStyleSheet(" background-color: #FF5232");

        break;
    case 3:
        lE->setStyleSheet(" background-color: #FF9E81");
        break;
    case 2:
        lE->setStyleSheet(" background-color: #FFDFD4");

        break;
    case 1:
        if(windowTheme==DARK_THEME)
         lE->setStyleSheet(" background-color: #676767");
        else lE->setStyleSheet(" background-color: #FFFFFF");
        break;
    default:
        break;
    }
}

void MainWindow::setColorComboBox(QComboBox *cb, int number)
{
    switch (number) {
    case 5:
        cb->setStyleSheet(" background-color: #FF0000");
        break;
    case 4:
        cb->setStyleSheet(" background-color: #FF5232");
        break;
    case 3:
        cb->setStyleSheet(" background-color: #FF9E81");
        break;
    case 2:
        cb->setStyleSheet(" background-color: #FFDFD4");
        break;
    case 1:
        if(windowTheme==DARK_THEME)
         cb->setStyleSheet(" background-color: #676767");
        else cb->setStyleSheet(" background-color: #FFFFFF");
        break;
    default:
        break;
    }
}

void MainWindow::initGraphforCP()
{
    qDebug()<<"initGraphforCP";
    //загружаем чекбоксы из settings
    saveVisibleGR[0]=settings->value("settings/saveVisibleGR1",0).toInt();
    if( saveVisibleGR[0]) ui->cbGraph1->setChecked(true);
    saveVisibleGR[1]=settings->value("settings/saveVisibleGR2",0).toInt();
    if( saveVisibleGR[1]) ui->cbGraph2->setChecked(true);
    saveVisibleGR[2]=settings->value("settings/saveVisibleGR3",0).toInt();
    if( saveVisibleGR[2]) ui->cbGraph3->setChecked(true);
    saveVisibleGR[3]=settings->value("settings/saveVisibleGR4",0).toInt();
    if( saveVisibleGR[3]) ui->cbGraph4->setChecked(true);

        namesHPForCB[GR_SET_NONE]="нет";
        namesHPForCB[GR_SET_PPL]="Уставка НПЛ";
        namesHPForCB[GR_RPM_PPL]="Обороты НПЛ";
        namesHPForCB[GR_SET_PPR]="Уставка НПП";
        namesHPForCB[GR_RPM_PPR]="Обороты НПП";
        namesHPForCB[GR_RPM_MXL]="Обороты МИКСЛ";
        namesHPForCB[GR_RPM_MXR]="Уставка МИКСП";
        namesHPForCB[GR_SET_PHL]="Уставка НОЛ";
        namesHPForCB[GR_SET_PHR]="Уставка НОП";
        namesHPForCB[GR_HARD_L]="Отвердитель Л";
        namesHPForCB[GR_HARD_R]="Отвердитель П";
        namesHPForCB[GR_CANAL_L]="Левый пистолет";
        namesHPForCB[GR_CANAL_R]="Правый пистолет";


    // заполняем чекбоксы
    for(int i=0;i<GR_CP_ALL;i++)
    {
      ui->cbData1->addItem(namesHPForCB[i]);
      ui->cbData2->addItem(namesHPForCB[i]);
      ui->cbData3->addItem(namesHPForCB[i]);
      ui->cbData4->addItem(namesHPForCB[i]);
    }

    firstSetupCB=false;
    saveValuePosGR[0]=settings->value("settings/saveValuePosGR1",0).toInt();
    ui->cbData1->setCurrentIndex(saveValuePosGR[0]);
    saveValuePosGR[1]=settings->value("settings/saveValuePosGR2",0).toInt();
    ui->cbData2->setCurrentIndex(saveValuePosGR[1]);
    saveValuePosGR[2]=settings->value("settings/saveValuePosGR3",0).toInt();
    ui->cbData3->setCurrentIndex(saveValuePosGR[2]);
    saveValuePosGR[3]=settings->value("settings/saveValuePosGR4",0).toInt();
    ui->cbData4->setCurrentIndex(saveValuePosGR[3]);


    for(int i=0;i<4;i++)
    {
        if(saveValuePosGR[i]==GR_CANAL_L ||saveValuePosGR[i]==GR_CANAL_R  )
            cPlgraph->graph(i)->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
    }


}

void MainWindow::initGraphforPaint()
{

    qDebug()<<"initGraphforPAINT";
    //загружаем чекбоксы из settings
    saveVisibleGR[0]=settings->value("settings/saveVisibleGR1",0).toInt();
    if( saveVisibleGR[0]) ui->cbGraph1->setChecked(true);
    saveVisibleGR[1]=settings->value("settings/saveVisibleGR2",0).toInt();
    if( saveVisibleGR[1]) ui->cbGraph2->setChecked(true);
    saveVisibleGR[2]=settings->value("settings/saveVisibleGR3",0).toInt();
    if( saveVisibleGR[2]) ui->cbGraph3->setChecked(true);
    saveVisibleGR[3]=settings->value("settings/saveVisibleGR4",0).toInt();
    if( saveVisibleGR[3]) ui->cbGraph4->setChecked(true);

        namesHPForCB[GR_SET_NONE_P]="нет";
        namesHPForCB[GR_BUTTON_START]="Старт";
        namesHPForCB[GR_CANAL_1]="Линия 1";
        namesHPForCB[GR_CANAL_3]="Линия 3";
       /* namesHPForCB[GR_RPM_PPR]="Обороты НПП";
        namesHPForCB[GR_RPM_MXL]="Обороты МИКСЛ";
        namesHPForCB[GR_RPM_MXR]="Уставка МИКСП";
        namesHPForCB[GR_SET_PHL]="Уставка НОЛ";
        namesHPForCB[GR_SET_PHR]="Уставка НОП";
        namesHPForCB[GR_HARD_L]="Отвердитель Л";
        namesHPForCB[GR_HARD_R]="Отвердитель П";
        namesHPForCB[GR_CANAL_L]="Левый пистолет";
        namesHPForCB[GR_CANAL_R]="Правый пистолет";*/


    // заполняем чекбоксы
    for(int i=GR_CP_ALL+1;i<GR_PAINT_ALL;i++)
    {
      ui->cbData1->addItem(namesHPForCB[i]);
      ui->cbData2->addItem(namesHPForCB[i]);
      ui->cbData3->addItem(namesHPForCB[i]);
      ui->cbData4->addItem(namesHPForCB[i]);
    }

    firstSetupCB=false;
    saveValuePosGR[0]=settings->value("settings/saveValuePosGR1",0).toInt()-1-GR_CP_ALL;
    ui->cbData1->setCurrentIndex(saveValuePosGR[0]);
    saveValuePosGR[1]=settings->value("settings/saveValuePosGR2",0).toInt()-1-GR_CP_ALL;
    ui->cbData2->setCurrentIndex(saveValuePosGR[1]);
    saveValuePosGR[2]=settings->value("settings/saveValuePosGR3",0).toInt()-1-GR_CP_ALL;
    ui->cbData3->setCurrentIndex(saveValuePosGR[2]);
    saveValuePosGR[3]=settings->value("settings/saveValuePosGR4",0).toInt()-1-GR_CP_ALL;
    ui->cbData4->setCurrentIndex(saveValuePosGR[3]);


    for(int i=0;i<4;i++)
    {
        if(saveValuePosGR[i]==GR_CANAL_1 ||saveValuePosGR[i]==GR_CANAL_3  )
            cPlgraph->graph(i)->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
    }
}

void MainWindow::initGraphforMagistral()
{
    qDebug()<<"initGraphforMagistral";
    //загружаем чекбоксы из settings
    saveVisibleGR[0]=settings->value("settings/saveVisibleGR1",0).toInt();
    if( saveVisibleGR[0]) ui->cbGraph1->setChecked(true);
    saveVisibleGR[1]=settings->value("settings/saveVisibleGR2",0).toInt();
    if( saveVisibleGR[1]) ui->cbGraph2->setChecked(true);
    saveVisibleGR[2]=settings->value("settings/saveVisibleGR3",0).toInt();
    if( saveVisibleGR[2]) ui->cbGraph3->setChecked(true);
    saveVisibleGR[3]=settings->value("settings/saveVisibleGR4",0).toInt();
    if( saveVisibleGR[3]) ui->cbGraph4->setChecked(true);



        namesHPForCB[GR_SET_NONE_M]="нет";
        namesHPForCB[GR_SET_MPUMP]="Уст. насоса";
        namesHPForCB[GR_SET_MDRUM]="Уст. бараб";
        namesHPForCB[GR_RPM_PUMP]="Обороты насос";
        namesHPForCB[GR_PRESSURE]="Давление";
        namesHPForCB[GR_FIST]="Кулачек";
        namesHPForCB[GR_RPM_PUMP_DRUM]="Обороты барабан";



    // заполняем чекбоксы
    for(int i=GR_PAINT_ALL+1;i<GR_MAGISTRAL_ALL;i++)
    {
      ui->cbData1->addItem(namesHPForCB[i]);
      ui->cbData2->addItem(namesHPForCB[i]);
      ui->cbData3->addItem(namesHPForCB[i]);
      ui->cbData4->addItem(namesHPForCB[i]);
    }

    firstSetupCB=false;
    saveValuePosGR[0]=settings->value("settings/saveValuePosGR1",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData1->setCurrentIndex(saveValuePosGR[0]);
    saveValuePosGR[1]=settings->value("settings/saveValuePosGR2",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData2->setCurrentIndex(saveValuePosGR[1]);
    saveValuePosGR[2]=settings->value("settings/saveValuePosGR3",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData3->setCurrentIndex(saveValuePosGR[2]);
    saveValuePosGR[3]=settings->value("settings/saveValuePosGR4",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData4->setCurrentIndex(saveValuePosGR[3]);


    for(int i=0;i<4;i++)
    {
        if(saveValuePosGR[i]==GR_FIST ||saveValuePosGR[i]==GR_SET_MPUMP ||saveValuePosGR[i]==GR_SET_MDRUM)
            cPlgraph->graph(i)->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
    }
}

void MainWindow::initGraphforTermo()
{
    qDebug()<<"initGraphforTermo";
    //загружаем чекбоксы из settings
    saveVisibleGR[0]=settings->value("settings/saveVisibleGR1",0).toInt();
    if( saveVisibleGR[0]) ui->cbGraph1->setChecked(true);
    saveVisibleGR[1]=settings->value("settings/saveVisibleGR2",0).toInt();
    if( saveVisibleGR[1]) ui->cbGraph2->setChecked(true);
    saveVisibleGR[2]=settings->value("settings/saveVisibleGR3",0).toInt();
    if( saveVisibleGR[2]) ui->cbGraph3->setChecked(true);
    saveVisibleGR[3]=settings->value("settings/saveVisibleGR4",0).toInt();
    if( saveVisibleGR[3]) ui->cbGraph4->setChecked(true);



        namesHPForCB[GR_SET_NONE_M]="нет";
        namesHPForCB[GR_SET_MPUMP]="Уст. насоса";
        namesHPForCB[GR_SET_MDRUM]="Уст. бараб";
        namesHPForCB[GR_RPM_PUMP]="Обороты насос";
        namesHPForCB[GR_PRESSURE]="Давление";
        namesHPForCB[GR_FIST]="Кулачек";
        namesHPForCB[GR_RPM_PUMP_DRUM]="Обороты барабан";



    // заполняем чекбоксы
    for(int i=GR_PAINT_ALL+1;i<GR_MAGISTRAL_ALL;i++)
    {
      ui->cbData1->addItem(namesHPForCB[i]);
      ui->cbData2->addItem(namesHPForCB[i]);
      ui->cbData3->addItem(namesHPForCB[i]);
      ui->cbData4->addItem(namesHPForCB[i]);
    }

    firstSetupCB=false;
    saveValuePosGR[0]=settings->value("settings/saveValuePosGR1",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData1->setCurrentIndex(saveValuePosGR[0]);
    saveValuePosGR[1]=settings->value("settings/saveValuePosGR2",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData2->setCurrentIndex(saveValuePosGR[1]);
    saveValuePosGR[2]=settings->value("settings/saveValuePosGR3",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData3->setCurrentIndex(saveValuePosGR[2]);
    saveValuePosGR[3]=settings->value("settings/saveValuePosGR4",0).toInt()-1-GR_PAINT_ALL;
    ui->cbData4->setCurrentIndex(saveValuePosGR[3]);


    for(int i=0;i<4;i++)
    {
        if(saveValuePosGR[i]==GR_FIST ||saveValuePosGR[i]==GR_SET_MPUMP ||saveValuePosGR[i]==GR_SET_MDRUM)
            cPlgraph->graph(i)->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
    }
}

void MainWindow::SetValveCPSate(int state)
{

    if((state>>2)&0x01)//клапан пластика левый
    {
        ui->lbStateVPL->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVPL->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>7)&0x01)//клапан пластика правый
    {
        ui->lbStateVPR->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVPR->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>10)&0x01)//клапан отвердителя левый
    {
        ui->lbStateVHL->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>11)&0x01)//клапан отвердителя правый
    {
        ui->lbStateVHR->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>8)&0x01)//шибер левый
    {
        ui->lbStateSHL->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateSHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>9)&0x01)//шибер правый
    {
        ui->lbStateSHR->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateSHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>18)&0x01)//капля1 левый
    {
        ui->lbStateVSP1L->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVSP1L->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>19)&0x01)//капля2 левый
    {
        ui->lbStateVSP2L->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVSP2L->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }
    if((state>>20)&0x01)//капля1 правый
    {
        ui->lbStateVSP1R->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVSP1R->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>21)&0x01)//капля2 правый
    {
        ui->lbStateVSP2R->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVSP2R->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>0)&0x01)//прочистка левая
    {
        ui->lbStateVCLL->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVCLL->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>1)&0x01)//прочистка правая
    {
        ui->lbStateVCLR->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVCLR->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>12)&0x01)//растворитель левый
    {
        ui->lbStateVWSHL->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVWSHL->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

    if((state>>13)&0x01)//растворитель правый
    {
        ui->lbStateVWSHR->setPixmap(QPixmap("://PNG/green-lamp.png"));
    }
    else
    {
        ui->lbStateVWSHR->setPixmap(QPixmap("://PNG/red-lamp.png"));
    }

}


//+++++++++++++[Процедура закрыть Хакер]++++++++++++++++++++++++++++++++++++++++++++++++
void MainWindow::stopCanHacker()
{
    QByteArray data; // Текстовая переменная
    //data = ui->cEnterText->text().toLocal8Bit().toHex() + '\r'; // Присвоение "data" значения из EnterText
    data ="Cx";
    data [1] = 13;
    writeData(data); // Отправка данных в порт
    Print(data); // Вывод данных в консоль
}

void MainWindow::on_pbWriteMode_clicked()
{
    //Включить/отключить режим редактирования
    if(readMode)
    {
        readMode=false;
        canPort->enableRead=false;
        ui->pbWriteMode->setText(tr("Включить режим чтения"));

    }
    else{
        readMode=true;
        canPort->enableRead=true;
        ui->pbWriteMode->setText(tr("Включить режим записи"));
       /* ui->pbSend1->hide();
        ui->pbSend2->hide();
        ui->pbSend3->hide();*/
    }
}


void MainWindow::on_pbDeletePistol_clicked()
{
    if(ui->leNumberGuns->text().toInt()>0 && ui->leNumberGuns->text().toInt()<ALL_GUNS_PAINT+1){
    canPort->Paintguns[ui->leNumberGuns->text().toInt()-1]->hideGun();
    }

}

void MainWindow::on_pushButton_clicked()
{
    writeToCANatID(ID_SEND_GYRO_ON);
}

void MainWindow::on_pushButton_2_clicked()
{
    writeToCANatID(ID_SEND_GYRO_OFF);
}

void MainWindow::on_pbStart_clicked()
{
    writeToCANatID(ID_SEND_START);
}

void MainWindow::on_pbStop_clicked()
{

    writeToCANatID(ID_SEND_STOP);
}

void MainWindow::on_pbAddPistol_clicked()
{
    if(ui->leNumberGuns->text().toInt()>0 && ui->leNumberGuns->text().toInt()<ALL_GUNS_PAINT+1){
    canPort->Paintguns[ui->leNumberGuns->text().toInt()-1]->showGun();
    }
}

void MainWindow::on_pbClearLog_clicked()
{
    ui->pTPrintLog->clear();
}

void MainWindow::on_BtCLEAR_clicked()
{
    ui->consol->clear();
}

void MainWindow::on_pushButton_7_clicked()
{
      customPlot->graph(1)->data()->clear();
      customPlot->graph(0)->data()->clear();
}

void MainWindow::on_pbGunTest_clicked()
{

   // if( testGun<16){
autotestGuns();
        // QTimer::singleShot(200, this, SLOT(autotestGuns()));


}

void MainWindow::autotestGuns()
{

    ui->pbGunTest->setText("Автотест пист: "+QString::number(testGun));

    if(testGun==ALL_GUNS_MIK)
    {
        testGun=0;
        writeToCANatID(ID_SEND_GUN_TEST);
        writeToCANatID(ID_SEND_STOP_GUN_TEST);
        ui->pbGunTest->setText("Тест окончен! ");
    }
    else{
        writeToCANatID(ID_SEND_GUN_TEST);
        testGun++;
        QTimer::singleShot(300, this, SLOT(autotestGuns()));
    }
}


void MainWindow::on_pbSpeedAuto_clicked()
{
    writeToCANatID(ID_SEND_SPEED_AUTO);
}

void MainWindow::on_pbReadDataFromMIK_clicked()
{
    writeToCANatID(ID_SEND_READ_DATA_MIK);
}



void MainWindow::on_pBWrLr_clicked()
{
    writeToCANatID(ID_WRITE_EEPROM_LR);
}

void MainWindow::on_pBWrPercDet_clicked()
{
    writeToCANatID(ID_WRITE_EEPROM_PERC_DET);
}

void MainWindow::on_pBWrPercOff_clicked()
{
    writeToCANatID(ID_WRITE_EEP_PERC_LED_OFF);
}

void MainWindow::on_pBWrLmark_clicked()
{
    writeToCANatID(ID_WRITE_EEPROM_LMARK);
}

void MainWindow::on_pbStartCycleSend_clicked()
{
    canPort->flagReadySendCycle=true;
    writeToCANatID(ID_SEND_START_CYCLE);

}

void MainWindow::on_cBGun1_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<00);
    else
    maskTestGun=maskTestGun|(1<<00);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
   // qDebug()<<maskTestGun<<arg1;
}

void MainWindow::on_cBGun2_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<01);
    else
    maskTestGun=maskTestGun|(1<<01);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun3_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<02);
    else
    maskTestGun=maskTestGun|(1<<02);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun4_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<03);
    else
    maskTestGun=maskTestGun|(1<<03);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun5_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<04);
    else
    maskTestGun=maskTestGun|(1<<04);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun6_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<05);
    else
    maskTestGun=maskTestGun|(1<<05);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun7_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<06);
    else
    maskTestGun=maskTestGun|(1<<06);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun8_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<7);
    else
    maskTestGun=maskTestGun|(1<<7);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun9_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<8);
    else
    maskTestGun=maskTestGun|(1<<8);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun10_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<9);
    else
    maskTestGun=maskTestGun|(1<<9);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun11_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<10);
    else
    maskTestGun=maskTestGun|(1<<10);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun12_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<11);
    else
    maskTestGun=maskTestGun|(1<<11);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}
void MainWindow::on_cBGun13_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<12);
    else
    maskTestGun=maskTestGun|(1<<12);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun14_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<13);
    else
    maskTestGun=maskTestGun|(1<<13);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun15_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<14);
    else
    maskTestGun=maskTestGun|(1<<14);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun16_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<15);
    else
    maskTestGun=maskTestGun|(1<<15);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun17_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<16);
    else
    maskTestGun=maskTestGun|(1<<16);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun18_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<17);
    else
    maskTestGun=maskTestGun|(1<<17);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun19_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<18);
    else
    maskTestGun=maskTestGun|(1<<18);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun20_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<19);
    else
    maskTestGun=maskTestGun|(1<<19);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun21_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<20);
    else
    maskTestGun=maskTestGun|(1<<20);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}

void MainWindow::on_cBGun22_stateChanged(int arg1)
{
    if(arg1==0)
    maskTestGun=maskTestGun&~(1<<21);
    else
    maskTestGun=maskTestGun|(1<<21);
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
}


void MainWindow::on_pbResetTestGuns_clicked()
{
    for(int i=1;i<=22;i++)
{
        QString cbName = QString("cBGun%1").arg(i);

            QCheckBox *chekbox = findChild<QCheckBox *>(cbName);

            if(chekbox != 0)
            {
                chekbox->setChecked(false);
               // qDebug()<<"Сброс cbName "<<cbName;
            }

 }
    maskTestGun=0;
  writeToCANatID(ID_SEND_STOP_GUN_TEST);
}

void MainWindow::on_pbSendMOdul_clicked()
{
     writeToCANatID(ID_SEND_SETUP_MODUL);
}

void MainWindow::on_pbStart_2_clicked()
{
  writeToCANatID(ID_SEND_START_CORR);
}



void MainWindow::on_pbSendRPMLM_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_MIXL);
}

void MainWindow::on_pbSendRPMLRM_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_MIXR);
}

void MainWindow::on_pbSendRPMLP_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_PUMPL);
}

void MainWindow::on_pbSendRPMLRP_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_PUMPR);
}

void MainWindow::on_pbSendRPMLH_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_HARDL);
}

void MainWindow::on_pbSendRPMLRH_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_HARDR);
}

void MainWindow::on_pbSendRPMPHP_clicked()
{
    writeToCANatID(ID_SEND_SET_RPM_PUMP_HP);
}


void MainWindow::on_pbSendMOdulCP_clicked()
{
     writeToCANatID(ID_SEND_SETUP_MODUL_CP);
}

void MainWindow::on_pbSendCleanCmd_pressed()
{
   writeToCANatID(ID_SEND_CLEANING_START);
}

void MainWindow::on_pbSendCleanCmd_released()
{
     writeToCANatID(ID_SEND_CLEANING_STOP);
}

void MainWindow::on_pbSendWashCmd_clicked()
{
    writeToCANatID(ID_SEND_WASHING);
}

void MainWindow::on_leSpeedAuto_editingFinished()
{
       writeToCANatID(ID_SEND_SPEED_AUTO);
}

void MainWindow::on_pbClearLog_2_clicked()
{
   ui->tEMikCommand->clear();
}

void MainWindow::on_pbSetReadADC_clicked()
{
   writeToCANatID(ID_SEND_READ_MODE_ADC);
}

void MainWindow::on_pbSendHardLimit_clicked()
{
     writeToCANatID(ID_SEND_LIMIT_HARDENER);
}

void MainWindow::on_cbGraph1_stateChanged(int arg1)
{
    saveVisibleGR[0]=arg1;
    settings->setValue("settings/saveVisibleGR1",arg1);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbGraph2_stateChanged(int arg1)
{
    saveVisibleGR[1]=arg1;
    settings->setValue("settings/saveVisibleGR2",arg1);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbGraph3_stateChanged(int arg1)
{
    saveVisibleGR[2]=arg1;
    settings->setValue("settings/saveVisibleGR3",arg1);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbGraph4_stateChanged(int arg1)
{
    saveVisibleGR[3]=arg1;
    settings->setValue("settings/saveVisibleGR4",arg1);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbData1_currentIndexChanged(int index)
{
    if(firstSetupCB)return; //Чтобы при добавление списка не сохранял индекс
     if(mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.прав" ||  mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.лев")index=index+GR_PAINT_ALL+1;
    saveValuePosGR[0]=index;
    settings->setValue("settings/saveValuePosGR1",index);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbData2_currentIndexChanged(int index)
{
    if(firstSetupCB)return;
    if(mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.прав" ||  mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.лев")index=index+GR_PAINT_ALL+1;
    saveValuePosGR[1]=index;
    settings->setValue("settings/saveValuePosGR2",index);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbData3_currentIndexChanged(int index)
{
    if(firstSetupCB)return;
    if(mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.прав" ||  mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.лев")index=index+GR_PAINT_ALL+1;
    saveValuePosGR[2]=index;
    settings->setValue("settings/saveValuePosGR3",index);
    settings->sync(); //записываем настройки
}

void MainWindow::on_cbData4_currentIndexChanged(int index)
{
    if(firstSetupCB)return;
    if(mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.прав" ||  mymik->mukBcParams[1][MUK_WORK_MODE]=="Магистр.лев")index=index+GR_PAINT_ALL+1;
    saveValuePosGR[3]=index;
    settings->setValue("settings/saveValuePosGR4",index);
    settings->sync(); //записываем настройки
}

void MainWindow::on_pushButton_8_clicked()
{
    cPlgraph->graph(1)->data()->clear();
    cPlgraph->graph(0)->data()->clear();
    cPlgraph->graph(2)->data()->clear();
    cPlgraph->graph(3)->data()->clear();
    cPlgraph->replot();
}

void MainWindow::on_pbReadMBO_clicked()
{
      writeToCANatID(ID_SEND_READ_MBO);
}

void MainWindow::on_lEContrast_editingFinished()
{
    //writeToCANatID(ID_SEND_WRITE_MBO_1);
}

void MainWindow::on_lEIznos_editingFinished()
{
  //  writeToCANatID(ID_SEND_WRITE_MBO_2);
}

void MainWindow::on_lEShum_editingFinished()
{
//    writeToCANatID(ID_SEND_WRITE_MBO_3);
}

void MainWindow::on_lEQuality_editingFinished()
{
   // writeToCANatID(ID_SEND_WRITE_MBO_QUALITY);
}

void MainWindow::on_lEContrast_returnPressed()
{
  writeToCANatID(ID_SEND_WRITE_MBO_1);
}

void MainWindow::on_lEIznos_returnPressed()
{
    writeToCANatID(ID_SEND_WRITE_MBO_2);
}

void MainWindow::on_lEShum_returnPressed()
{
     writeToCANatID(ID_SEND_WRITE_MBO_3);
}

void MainWindow::on_lEQuality_returnPressed()
{
    writeToCANatID(ID_SEND_WRITE_MBO_QUALITY);
}

void MainWindow::on_radioButton_2_clicked()
{
    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
            canPort->Paintguns[i]->hideGun();
    }
    // ui->GunsLayout->set();
    mymik=&(canPort->mik[1]);
    canPort->mikAdr=1;
    settings->setValue("settings/adrMIK",1);
    settings->sync(); //записываем настройки
    qDebug()<<"SET MIK1";
}

void MainWindow::on_radioButton_clicked()
{

    for(int i=0;i<ALL_GUNS_PAINT;i++)
    {
            canPort->Paintguns[i]->hideGun();
    }
    mymik=&(canPort->mik[0]);
         canPort->mikAdr=0;

         settings->setValue("settings/adrMIK",0);
         settings->sync(); //записываем настройки
         qDebug()<<"SET MIK0";
}

void MainWindow::setThemeWindow(int arg1)
{
    //  qDebug()<<"!!@# $"<<arg1;
    if(arg1==DARK_THEME){
        qApp->setStyle("fusion");
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window,QColor(53,53,53));
        darkPalette.setColor(QPalette::WindowText,Qt::white);
        darkPalette.setColor(QPalette::Disabled,QPalette::WindowText,QColor(127,127,127));
        darkPalette.setColor(QPalette::Base,QColor(42,42,42));
        darkPalette.setColor(QPalette::AlternateBase,QColor(66,66,66));
        darkPalette.setColor(QPalette::ToolTipBase,Qt::white);
        darkPalette.setColor(QPalette::ToolTipText,Qt::white);
        darkPalette.setColor(QPalette::Text,Qt::white);
        darkPalette.setColor(QPalette::Disabled,QPalette::Text,QColor(127,127,127));
        darkPalette.setColor(QPalette::Dark,QColor(35,35,35));
        darkPalette.setColor(QPalette::Shadow,QColor(20,20,20));
        darkPalette.setColor(QPalette::Button,QColor(53,53,53));
        darkPalette.setColor(QPalette::ButtonText,Qt::white);
        darkPalette.setColor(QPalette::Disabled,QPalette::ButtonText,QColor(127,127,127));
        darkPalette.setColor(QPalette::BrightText,Qt::red);
        darkPalette.setColor(QPalette::Link,QColor(42,130,218));
        darkPalette.setColor(QPalette::Highlight,QColor(42,130,218));
        darkPalette.setColor(QPalette::Disabled,QPalette::Highlight,QColor(80,80,80));
        darkPalette.setColor(QPalette::HighlightedText,Qt::white);
        darkPalette.setColor(QPalette::Disabled,QPalette::HighlightedText,QColor(127,127,127));
        ui->tableWidget->setStyleSheet( "QTableView { gridline-color: grey;/*background-color:yellow */}" );
        qApp->setPalette(darkPalette);
        ui->tableWidget->setPalette(darkPalette);
        //ui->tableWidget->scrollBarWidgets(darkPalette);
        settings->setValue("settings/WindowTheme",DARK_THEME);
        settings->sync(); //записываем настройки
        windowTheme=DARK_THEME;
    }
    else
    {
        qApp->setStyle("windowsvista");
        qApp->setPalette(style()->standardPalette());
        ui->tableWidget->setPalette(style()->standardPalette());
        // ui->tableWidget->setStyleSheet( "QTableView { gridline-color: black; }" );
        settings->setValue("settings/WindowTheme",LIGHT_THEME);
        settings->sync(); //записываем настройки
        windowTheme=LIGHT_THEME;
    }
}

void MainWindow::on_pBStartMagistral_clicked()
{
 // qDebug()<<"Нажали кнопку Старт Магистр"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
  if(delayStartMg>=0){
     ui->pBStartStroke->click();
     timerStartMag->start(delayStartMg);
  }
  else{
      ui->pBTestFisits->click();
      timerStartMag->start(abs(delayStartMg));
  }
}

void MainWindow::on_pBStopMagistral_clicked()
{
  //   qDebug()<<"Нажали кнопку Стоп Магистр"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
     if(delayStopMg>=0){
        ui->pBStartSpace->click();
        timerStopMag->start(delayStopMg);
     }
     else{
         ui->pBTestOfffists->click();
         timerStopMag->start(abs(delayStopMg));
     }
}

void MainWindow::on_lePwmSpace_editingFinished()
{
   // qDebug()<<"on_lePwmSpacree_textEdited";
    pwmSpace=ui->lePwmSpace->text().toInt();
    settings->setValue("settings/pwmSpace",pwmSpace);
    settings->sync(); //записываем настройки
}

void MainWindow::on_lePwmStroke_editingFinished()
{
   // qDebug()<<"on_lePwmStroke_textEdited";
    pwmStroke=ui->lePwmStroke->text().toInt();
    settings->setValue("settings/pwmStroke",pwmStroke);
    settings->sync(); //записываем настройки
}

void MainWindow::on_leDelayStartMg_editingFinished()
{
  //  qDebug()<<"delayStartMg";
    delayStartMg=ui->leDelayStartMg->text().toInt();
    settings->setValue("settings/delayStartMg",delayStartMg);
    settings->sync(); //записываем настройки
}

void MainWindow::on_leDelayStopMg_editingFinished()
{
   // qDebug()<<"delayStopMg";
    delayStopMg=ui->leDelayStopMg->text().toInt();
    settings->setValue("settings/delayStopMg",delayStopMg);
    settings->sync(); //записываем настройки
}

void MainWindow::on_lEPumpValue_editingFinished()
{
   // qDebug()<<"pumpValue";
    pumpValue=int(ui->lEPumpValue->text().toFloat()*1000);
    settings->setValue("settings/pumpValue",pumpValue);
    settings->sync(); //записываем настройки
}

void MainWindow::on_lePaintDens_editingFinished()
{
   // qDebug()<<"paintDens";
    paintDens=int(ui->lePaintDens->text().toFloat()*100);
    settings->setValue("settings/paintDens",paintDens);
    settings->sync(); //записываем настройки
}

void MainWindow::on_pushButAuto_clicked()
{
        writeToCANatID(ID_SEND_EMUL_PAR_MBO_3);
}

void MainWindow::on_pBenableLeft_clicked()
{
      writeToCANatID(ID_SEND_ENABLE_BLOCK);
}

void MainWindow::on_SlStroke_sliderMoved(int position)
{
    ui->lePwmStroke->setText(QString::number(position));
}

void MainWindow::on_SlSpase_sliderMoved(int position)
{
    ui->lePwmSpace->setText(QString::number(position));
}

void MainWindow::on_pBStartStroke_clicked()
{
  // qDebug()<<"Включаем насос"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    writeToCANatID(ID_SEND_START_STROKE);
    for(int i=0;i<4;i++)
    {
    if(saveValuePosGR[i]==GR_SET_MPUMP)
    realtimeDataGRAPH(i,51);
    }
}

void MainWindow::on_pBStartSpace_clicked()
{
 //  qDebug()<<"Отлючаем насос"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    writeToCANatID(ID_SEND_START_SPACE);
    for(int i=0;i<4;i++)
    {
    if(saveValuePosGR[i]==GR_SET_MPUMP)
    realtimeDataGRAPH(i,26);
    }
}

void MainWindow::on_lePidP_editingFinished()
{
    pidP=int(ui->lePidP->text().toInt());
    settings->setValue("settings/pidP",pidP);
    settings->sync(); //записываем настройки
}

void MainWindow::on_lePidI_editingFinished()
{
    pidI=int(ui->lePidI->text().toInt());
    settings->setValue("settings/pidI",pidI);
    settings->sync(); //записываем настройки
}

void MainWindow::on_lePidD_editingFinished()
{
    pidD=int(ui->lePidD->text().toInt());
    settings->setValue("settings/pidD",pidD);
    settings->sync(); //записываем настройки
}

void MainWindow::on_leimpRPM_editingFinished()
{
    impPPM=int(ui->leimpRPM->text().toInt());
    settings->setValue("settings/impPPM",impPPM);
    settings->sync(); //записываем настройки
}

void MainWindow::on_SlpidP_sliderMoved(int position)
{
    ui->lePidP->setText(QString::number(position));
    pidP=int(ui->lePidP->text().toInt());
    settings->setValue("settings/pidP",pidP);
    settings->sync(); //записываем настройки
}

void MainWindow::on_SlpidI_sliderMoved(int position)
{
     ui->lePidI->setText(QString::number(position));
     pidI=int(ui->lePidI->text().toInt());
     settings->setValue("settings/pidI",pidI);
     settings->sync(); //записываем настройки
}

void MainWindow::on_SlpidD_sliderMoved(int position)
{
      ui->lePidD->setText(QString::number(position));
      pidD=int(ui->lePidD->text().toInt());
      settings->setValue("settings/pidD",pidD);
      settings->sync(); //записываем настройки
}

void MainWindow::on_pbMagPumpSet_clicked()
{
    writeToCANatID(ID_SEND_PID_MAGISTRAL);
}

void MainWindow::on_pbSwitchONLog_clicked()
{
    writeToCANatID(ID_SEND_SWITCH_ON_LOG);
}

void MainWindow::on_pbSwitchOFFLog_clicked()
{
    writeToCANatID(ID_SEND_SWITCH_OFF_LOG);
}



void MainWindow::on_pBTestFisits_clicked()
{
     // qDebug()<<"Включаем Клапан"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    maskTestGun=stringToMask(ui->lEfists->text());
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
    for(int i=0;i<4;i++)
    {
    if(saveValuePosGR[i]==GR_FIST)
    realtimeDataGRAPH(i,50);
    }
}

void MainWindow::on_pBTestOfffists_clicked()
{
   // qDebug()<<"ОТключаем Клапан"<<QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    maskTestGun=0;
    writeToCANatID(ID_SEND_GUN_TEST_MASK);
    for(int i=0;i<4;i++)
    {
    if(saveValuePosGR[i]==GR_FIST)
    realtimeDataGRAPH(i,25);
    }
}

void MainWindow::on_pBStartAutoMagistral_clicked()
{
    timerAutoClick->start(ui->leTimerAuto->text().toInt());
}

void MainWindow::on_pBStopAutoMagistral_clicked()
{
     timerAutoClick->stop();
     ui->pBTestOfffists->click();
     ui->pBStartSpace->click();
}

void MainWindow::on_pbClearGyroGraph_clicked()
{
    cPlGyro->graph(1)->data()->clear();
    cPlGyro->graph(0)->data()->clear();
    cPlGyro->graph(2)->data()->clear();
    cPlGyro->graph(3)->data()->clear();
    cPlGyro->yAxis->setRange(-50,50);
    cPlGyro->replot();
}

void MainWindow::on_pbTpClean_toggled(bool checked)
{
    if(checked)
        writeToCANatID(ID_SEND_TP_CLEAN_ON);
    else
        writeToCANatID(ID_SEND_TP_CLEAN_OFF);
}

void MainWindow::on_pbTpPrevious_clicked()
{
    writeToCANatID(ID_SEND_TP_PREVIOUS);
}

void MainWindow::on_pbTpNext_clicked()
{
    writeToCANatID(ID_SEND_TP_NEXT);
}

void MainWindow::on_pbTpEncoder_sliderMoved(int position)
{

    // qDebug()<<"position"<<position;
    static int old_val;
    static bool first_time = true;
    if (first_time) {
        first_time = false;
        old_val = position;
    } else {
        int dx = position - old_val;
        if (qAbs(dx) > 50) {
            if (dx < 0) {
                dx += 100;
            } else {
                dx -= 100;
            }
        }
        old_val = position;
        // qDebug()<<"value"<<dx;
        techTerminalEncoder+=dx;
    }

}

void MainWindow::on_leConsMat1_editingFinished()
{
    // qDebug()<<"pumpValue";
     consMat1=int(ui->leConsMat1->text().toFloat());
     settings->setValue("settings/consMat1",consMat1);
     settings->sync(); //записываем настройки
}

void MainWindow::on_leConsMat2_editingFinished()
{
    // qDebug()<<"pumpValue";
     consMat2=int(ui->leConsMat2->text().toFloat());
     settings->setValue("settings/consMat2",consMat2);
     settings->sync(); //записываем настройки
}

void MainWindow::on_leConsSOG_editingFinished()
{
    // qDebug()<<"pumpValue";
     consSOG=int(ui->leConsSOG->text().toFloat());
     settings->setValue("settings/consSOG",consSOG);
     settings->sync(); //записываем настройки
}

void MainWindow::on_leCanMat1_editingFinished()
{
    // qDebug()<<"pumpValue";
    if( (int(ui->leCanMat1->text().toInt())>=0)&& (int(ui->leCanMat1->text().toInt()) < 4))
    {
     canalMat1=int(ui->leCanMat1->text().toInt());
     settings->setValue("settings/canalMat1",canalMat1);
     settings->sync(); //записываем настройки
    }
    else
    {
       ui->leCanMat1->setText("1");

    }
}

void MainWindow::on_leCanMat2_editingFinished()
{
    // qDebug()<<"pumpValue";
    if( (int(ui->leCanMat2->text().toInt())>+0)&& (int(ui->leCanMat2->text().toInt()) < 4))
    {
     canalMat2=int(ui->leCanMat2->text().toInt());
     settings->setValue("settings/canalMat2",canalMat2);
     settings->sync(); //записываем настройки
    }
    else
    {
       ui->leCanMat2->setText("1");

    }
}

void MainWindow::on_leCanSog_editingFinished()
{
    // qDebug()<<"pumpValue";
    if((int(ui->leCanSog->text().toInt())>=0)&& (int(ui->leCanSog->text().toInt()) < 4))
    {
     canalSOG=int(ui->leCanSog->text().toInt());
     settings->setValue("settings/canalSOG",canalSOG);
     settings->sync(); //записываем настройки
    }
    else
    {
       ui->leCanSog->setText("1");

    }
}

void MainWindow::on_pBSetAdressSMS_clicked()
{
   writeToCANatID(ID_SEND_SMS_ADRES_NEW);
   canPort->reqSMS=0;
}

void MainWindow::on_pBSetAdressDanf_clicked()
{
   writeToCANatID(ID_SEND_DANF_ADRES_NEW);
}



void MainWindow::on_pbTpEncoder_sliderReleased()
{
qDebug()<<"techTerminalEncoder"<<techTerminalEncoder;

    writeToCANatID(ID_SEND_TP_ENCODER);

}


void MainWindow::on_pbReadVersC2C_clicked()
{
    writeToCANatID(ID_VERS_CAN2CAN);
}

void MainWindow::on_pbSetModeC2C_clicked()
{
   writeToCANatID(ID_SET_MODE_CAN2CAN);
}


void MainWindow::on_leLenForConsuption_editingFinished()
{
    // qDebug()<<"pumpValue";
    if( (int(ui->leLenForConsuption->text().toInt())>5)&& (int(ui->leLenForConsuption->text().toInt())<200))
    {
     lengForConsuption=int(ui->leLenForConsuption->text().toInt());
     settings->setValue("settings/lengForConsuption",lengForConsuption);
     settings->sync(); //записываем настройки
    }
    else
    {
       ui->leLenForConsuption->setText("10");

    }
}

void MainWindow::on_pbSpeedUbup_toggled(bool checked)
{
   if(checked)
    writeToCANatID(ID_SEND_SPEED_PRESS);
   else
    writeToCANatID(ID_SEND_SPEED_REALESE);
}



void MainWindow::on_cbTechPanel_stateChanged(int arg1)
{
    qDebug()<<arg1<<"on_cbTechPanel_stateChanged";
    if(arg1==0){
        settings->setValue("settings/plPanel",0);
        settings->sync(); //записываем настройки
        plPanel->hide();
    }
     else
    {
        settings->setValue("settings/plPanel",arg1);
        settings->sync(); //записываем настройки
        plPanel->show();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    qDebug()<<"closeEvent()<<"<<event;
    // закрытие панели пластика не снимает галку с чекбокса
    QObject::disconnect(plPanel, &QDialog::rejected, this, nullptr);

    plPanel->close();
}
