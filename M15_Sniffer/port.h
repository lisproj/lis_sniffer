#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "can_prot.h"
#include "guns.h"
#define  NUMBER_FOR_MIK_STATISTIC 1000 //  Число записей для статистики мик
#define TIMEOUT_TIMER_CAN 2000  // Таймаут для широковещательных  пакетов модулей- состояния связи
#define NUMBER_SENSOR_DANFOSS 10 // число обрабатываемых датчиков
#define NUMBER_SENSOR_MOTION 10 // число обрабатываемых датчиков


enum moduleType{
    MODULE_NONE=-1, // не выбран
    MODULE_PAINT=0,// краска
    MODULE_TP=1 ,       //термо пластик
    MODULE_CP=2,         //холодный пластик

};


struct Settings {
    QString name;
    qint32 baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
};

struct MikStat{
int number;
QString time;
int type;
int width;
int gunsSolid;
int gunsBroke;
int gunsBeads;
int strokeSm;
int spaseSm;
int markCycle;
quint32 lenAllGunsSm;
quint32 lenStepSm;
int pumpCycle;
int averSpeed;
int isGenerator; // Включен ди генератор скорости
};



typedef enum {
ID_WORK_MODE,
ID_CONF_BASE,
ID_CONF_MODUL,
ID_SETUP_HYDRO,
ID_SETUP_HYDRO_1,
ID_SETUP_HYDRO_2,
ID_DELAYS_CP,
ID_CONF_CYCLE,
ID_GYRO_XYZ,
ID_LIS_CORR,
ID_BEADS_PAR,
ID_GUNS_SETUP,
ID_COMMAND_MIK,
ID_EVENT_MIK,
ID_STATUS_TERMINAL,
ID_STATUS_MIK,
ID_STATUS_UBUP,
ID_STATUS_MBO,
ID_STATUS_SHNEIDER,
ID_STATUS_CANHUB,
ID_MIK_ACK,
ID_DATA_MIK,
ID_DATA_MIK_STATISTIC,
ID_FOR_STATISTIC,
ID_MBO_CORRIDOR,
ID_WASHING_MIK,
ID_MBO_ANS_CMD,
ID_MBO_ANS_QUALITY,
ID_RPM_MIX_L,
ID_RPM_MIX_R,
ID_RPM_DANFOSS,
ID_PESSURE_SHNEIDER,
ID_RPM_SHNEIDER,
ID_SENSOR_MS,
ID_SENSOR_DANFOSS,
ID_VERS_C2C,
ID_BC_C2C,
ID_EVENT_C2C,
ID_REQ_QUAL,
ID_SET_QUAL,
}ID_PARAMS;

enum BroadCastMIKParms{
MIK_PAR_SPEED=0,
MIK_PAR_STEP_M,
MIK_PAR_ALL_MARK_SM,
MIK_PAR_PUMP_CYCL_SM,
MIK_PAR_PUMP_CYCL,
MIK_PAR_MARK_SM,
MIK_PAR_GAP_SM,
MIK_PAR_STATUS,
MIK_PAR_PUMP_CYCL_SM_2,
MIK_PAR_MILEAGE_SM,
MIK_PAR_CYCL,
MIK_PAR_MARK_TIME_S,
MIK_PAR_GUN_STATE,
MIK_PAR_SPEED_SENSOR,
MIK_PAR_GUN_STATE_2,
MIK_PAR_SPEED_SENSOR_2,
//*********
MIK_PAR_ALL_PARAMS
};
enum BroadCastMUKParms{
MUK_RPM_HYDRO_1=0,
MUK_RPM_HYDRO_2,
MUK_RPM_HYDRO_3,
MUK_RPM_HYDRO_4,
MUK_MODUL,
MUK_WORK_MODE,
MUK_RPM_MIXER,
MUK_ERRORS,
MUK_HARD_PERC_L,
MUK_HARD_PERC_R,
MUK_ADC_VOLT_1,
MUK_ADC_VOLT_2,
MUK_ADC_VOLT_3,
MUK_ADC_VOLT_4,
//*********
MUK_PAR_ALL_PARAMS
};

enum{
NUM_ALL,        // всего пакетов
NUM_BC,         // широковещательных
NUM_CMD,        // команды
NUM_ACK_GOOD,  // Ответ команды хороший
NUM_ACK_BAD,   // Ответ команды плохой
NUM_DATA_REQ,   // Запрос данныx
NUM_DATA,       // Запрос данныx
NUM_EVENT,      // событие
NUM_EVENT_ACK,       // Данные
NUM_BC_GYRO,       // Данные
//********
ALL_TYPES_COUNT
};

enum {
    HP_NONE=0,
    HP_EXTRUDER=1,
    HP_MULTIDOT=2,
    HP_MAGISTRAL_L=3,   // левая сторона магистральной термопластик 10 кул
    HP_MAGISTRAL_SPRAY_L=4, // левая сторона магистральной спрей 2 кул
    HP_MAGISTRAL_R=5,   // правая сторона магистральной термопластик 6 кул мультидот
    HP_MAGISTRAL_SPRAY_R=6, // левая сторона магистральной спрей 2 кул
    HP_CLOSED_MB=7, // ЗРБ
    HP_400_SPRAY=8,   // спрей пластик для к400
    //-----------------------
    TOTAL_HP_TYPE
};

enum deviceForStatus
{
    STATUS_TERMINAL,
    STATUS_MIK_0,
    STATUS_MIK_1,
    STATUS_GYRO,
    STATUS_UBUP,
    STATUS_MBO,
    STATUS_SHNEIDER,
    STATUS_CANHUB,
    //**************
    STATUS_ALL
};

struct Mik
{
 long statMIK[ALL_TYPES_COUNT];
 //MIK BC params
 QString mikBcParams[2][MIK_PAR_ALL_PARAMS];
 QString mukBcParams[2][MUK_PAR_ALL_PARAMS];
 int32_t mikGunState=0,mikGunStateOld=0;

 MikStat *mikStat; // статистика передаваемая с МИК


 int workMode;
 int transitionType; // Тип перехода
 int solidGuns; // сплошная
 int brokenGuns; // прерывистая
 int workGuns; // рабочие пистолеты
 //conf_base
 int markerPath;// расстояние от базовой линии до маркера в см
 int scanerParth; //  расстояние от базовой линии до Сканера в см
 int coefCorr; //  коэф коррекции цикла умнож. на 1000
 //conf_cycle
 int lenGap; // длина пробела в см
 int lenMark; // длина штриха в см
 int lenToleranceGap; // длина пробела в см
 int lenToleranceMark; // длина штриха в см

 // для статистики
 qint32 standart=0;
 int modul=0;
 qint32 setWidth=0;
 int counterMikStat=0; // счетик шагов статистики

 bool isMagistal=false; // магистральная?

 QString textCommand="";
 QString textEvent="";

};


class Port : public QObject
{
    Q_OBJECT

public:

    explicit Port(QObject *parent = 0);

    ~Port();

    QSerialPort thisPort;
    QTimer timer;
  //  QTimer *timeoutTimer;
    int mikOffset;
    //Для статстики
    long countSnifferPacket; // число перехваченных пакетов
    long countErrorPacket = 0;   
    long statTerm[ALL_TYPES_COUNT];
    long statMBO[ALL_TYPES_COUNT];
    long statUBUP[ALL_TYPES_COUNT];
    long statBCSchneider;
    long statBCCanHub;
    QString versC2C, nversC2C;//версии can 2 can
    bool isversC2C=false, isnversC2C=false;//версии can 2 can

    double countLocalBC[STATUS_ALL]; // число широковещательных пакетов за переиод времени

    Mik mik[2];
    int mikAdr;

    long speedCurPacketInMinute; // скорость число перехваченных пакетов в минуту
    long speedMidPacketInMinute; // скорость число перехваченных пакетов в минуту





    double countSendingPacket;// число отправленных пакетов от CANSNIFFER
//    double countPacketMustSended;// число пакетов  которые должны дойти
//    double countPacketAnswered;// число ответов
//   double countOtherPacketAnswered;// число других ответов
//    double countNotAnswered;// число других ответов
    //QSet <QByteArray> SendsPacket;

     bool enableRead;

     Guns *Paintguns[ALL_GUNS_PAINT];
     SensorMotion *sms[NUMBER_SENSOR_MOTION];//10 датчиков протока СОГ
     SensorDanfoss *sensDanf[NUMBER_SENSOR_DANFOSS];//10 датчиков протока СОГ
    //ЕЕПРОМ мик
    int percOff=0;// процент заполнения для выключения
    int lr=0; // Длина разрыва в сплошной
    int lmark=0; // Длина отрезка  сплошной между разрывами
    int percDet=0; // процент заполнения для детекции линии
    int limStroke=0; // допуск штриха
    int limSpace=0; // допуск пробела
    int counterSpeed=0; // допуск пробела

    int lstat=0; // расстояние для записи интервалов статистики(резерв)

    bool flagReadySendCycle=false;

    //Pult terminal params
    QString timeTerminal=""; // время терминала
    int  pultAdrMik=0;
    int  pultAdrUBUP=0;
    int  pultAdrMBO=0;

    // MIK MODULS

    int workModul=0;
    int mukModul=MODULE_NONE;
    int speedSensorType=0;
    int speedSensorCoef=0;
    int lineTypeCP=0;       // тип линии для ХП
    int distBetweenDots=0; // расстояние между точками для точечной для ХП
    int washingType=0;   // тип промывки

    // Параметры для гидро наосов
    int hydroType=0;
    int hydroFists[CanProt::ALL_HYDRO];
    int hydroRpm[CanProt::ALL_HYDRO];
    int hydroDirection[CanProt::ALL_HYDRO];
    int hydroMode[CanProt::ALL_HYDRO];
    int hydroImpOnRpm[CanProt::ALL_HYDRO]; // импульсы на оборот
    int hydroPID_P[CanProt::ALL_HYDRO]; //  пропорц. коэф ПИД
    int hydroPID_I[CanProt::ALL_HYDRO]; //  интегральный коэф ПИД
    int hydroPID_D[CanProt::ALL_HYDRO]; //  диф коэф ПИД
   // int hydroDelayOn[CanProt::ALL_HYDRO];   хранятся в задержках клапанов
   // int hydroDelayOff[CanProt::ALL_HYDRO];
    int presetsSCREW[11];

    //задержки для клапанов
    int valvDelOn[CanProt::NUM_DELAYS_ALL];      //включения
    int valvDelOff[CanProt::NUM_DELAYS_ALL];     //выключения




    //Гироскоп

    signed short gyroX;
    signed short gyroY;
    signed short gyroZ;
    signed short gyroSpeed;

    //UBUP

    uint16_t leftcoord=0;   // краевая координата левой границы целевого коридора.
    uint16_t rightcoord=0; // краевая координата правой границы целевого коридора.

    Settings SettingsPort;

    //MBO
  signed short  mboContrast;
  signed short  mboIznos;
  signed short  mboShum;
  signed short  mboQuality;

  // датчики  оборотов EMD
  int16_t rpmMixerL;
  int16_t rpmMixerR;

  // давление разметочного блока Шнайдер
  int16_t pressure;
  int16_t setLeftRpm;
  int16_t setRightRpm;

  // оборотыDanfoss
  int16_t rpmDanfossL;
  int16_t rpmDanfossR;

   int reqSMS=0;// запрос на установку датчика

signals:

    void finished_Port(); //

    void refrParams(int ID,QVariant par1 );
    void error_(QString err);

    void outPort(QString data);
    void toPrintLOG(int, int,int,QString data,bool isComent);

    void outStatus(QString data);

    void can_data_req(quint32 file_offset);

    void send_msg (qint32 id, quint8 data0, quint8 data1, quint8 data2, quint8 data3, quint8 data4, quint8 data5, quint8 data6, quint8 data7);

    void printStatusBar(QString);
    void readyStartCycle();
    void setModuleState(int);

public slots:

    void  DisconnectPort();

    void ConnectPort(void);

    void Write_Settings_Port(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);

    void process_Port();

    void WriteToPort(QByteArray data);

    void WriteToCanPort(QByteArray data);

    void ReadInPort();

    void workTimeout();


    void ParseBroadCast(int command, int transmitter, quint8 *data); // парсинг широковещательных команд
    void ParseAnswerCommand(int command, int reciever,int transmitter, quint8 *data, int len); // парсинг ответа на команду команд
    void ParseEvent(int command, int transmitter, quint8 *data); // парсинг событий
    void ParseData(int command, int reciever, int transmitter, quint8 *data); // парсинг данных
    void ParseCommand(int command,int reciever, int transmitter, quint8 *data); // парсинг данных
    void ParseRequest(int command,int reciever, int transmitter, quint8 *data); // парсинг данных


 //   void timeOut();

    void speedCalc();

private slots:

    void handleError(QSerialPort::SerialPortError error);//
    qint16  TwosComplementToRotate(quint8 LSB, quint8 MSB);
    void setSensDanf(int adr,int rpm);
    QString  setNameSensDanf(int adr);

public:

};

#endif // PORT_H
