#ifndef GUNS_H
#define GUNS_H

#include <QObject>
#include <QPushButton>
#include <QLineEdit>
#include <QLayout>
#include <QDebug>
#include <QLabel>
#include <QSpinBox>

#define ALL_GUNS_PAINT 16  // количество пистолетов
#define ALL_GUNS_MIK 22  // количество пистолетов

enum
{
  GUN_TYPE_NONE,
  GUN_TYPE_LIS,
  GUN_TYPE_SIMPLE,
  GUN_TYPE_BEADS
};

struct LisParams {    
    qint16 brokeOrSolid;
    qint16 Ti;
    qint16 To;
    qint8 S2;
    qint8 R2;
    qint16 Base;
    qint16 beads;
    bool isVisible;
};

struct BeadsParams {
   // qint16 number;
    quint16 Ti;
    quint16 To;
    qint16 Base;
    bool isVisible;
};




class Guns : public QHBoxLayout
{
    Q_OBJECT
public:
    explicit Guns(QWidget *parent = 0);
    ~Guns();
    static int ResID;   // Статическая переменная, счетчик номеров кнопок
    LisParams lisGuns;
    BeadsParams beadsguns;
    static qint16 beadsMask;
    qint8 type;//

     QLineEdit *leNumber;
     QLabel *leBrokeOrSolid;
     QLineEdit *leTi;
     QLineEdit *leTo;
     QLineEdit *leS2;
     QLineEdit *leR2;
     QLineEdit *leBase;
     QLineEdit *lebeads;
   //  QPushButton *btsendM;
     QLabel* lbState;


private:
    int buttonID;   // Локальная переменная, номер кнопки
signals:
    void PbSend(int);
    void setBeadsGuns();
    void setLisGuns();
    void setSimpleGuns();
    void setState(bool);

public slots:
        const int getID();        // Функция для возврата локального номера кнопки
        void initGuns();
        void showGun();
        void hideGun();
       // void slotGetNumber();
        void settingBeads(); //убираем лишнее лайнедиты для шаров
        void settingLisGuns(); //восcтановим лайнедиты для шаров
        void settingSimpleGuns(); //восcтановим лайнедиты для шаров
        void settingLamp(bool newstate); //восcтановим лайнедиты для шаров
};
class SensorMotion : public QHBoxLayout
{
    Q_OBJECT
public:
    explicit SensorMotion(QWidget *parent = 0);
    ~SensorMotion();
    static int SensID;   // Статическая переменная, счетчик номеров кнопок
    qint8 activity;
    qint8 numGuns;
    qint8 motion;
    qint8 alarm;
    qint8 errCan;
    qint8 errEprom;
    qint32 onOff;
    qint32 changeAdr;
    qint16 version;
    bool isVisible;

     QLineEdit *leNumber;
     QLabel *lbguns;
     QLabel *lbAlarm;
     QLabel *lbErrCAN;
     QLabel *lbErrEpr;
     QLabel *lbEprSwitch;
     QLabel *lbEprChangeAdr;
     QLabel *lbVers;
     QPushButton *btsendM;
     QPushButton *btsendM2;
     QPushButton *btsendM3;
     QSpinBox *sbNewAdress;
     QLabel* lbState;

private:
    int buttonID;   // Локальная переменная, номер кнопки
signals:
    void PbSend(int);
    void setState(bool);
    void sreqEprom(int adr);
    void sreqSetAlarm(int adr);
    void sreqSetAdr(int adr);

public slots:
        const int getID();        // Функция для возврата локального номера кнопки
        void initGuns();
        void showGun();
        void hideGun();
        void reqEprom();
        void reqSetAlarm();
        void reqSetAdr();
        void settingLamp(bool newstate); //восcтановим лайнедиты для шаров
};

class SensorDanfoss : public QHBoxLayout
{
    Q_OBJECT
public:
    explicit SensorDanfoss(QWidget *parent = 0);
    ~SensorDanfoss();
    static int SensDanfID;   // Статическая переменная, счетчик номеров кнопок

     QLabel *lbNumber;
     QLabel *lbAdress;
     QLabel *lbName;
     QLabel *lbRpm;
     QPushButton *btsendAdr;
     QPushButton *btsendSpeed;

    int buttonID;   // Локальная переменная, номер кнопки
    qint8 activity;
    quint8 addres;
    qint16 rpm;
    QString name;
    bool isVisible;

signals:
     void transmitAddres(int adr);
     void transmitSpeed(int adr);

public slots:
        const int getID();        // Функция для возврата локального номера кнопки
        void showSens();
        void hideSens();
        void reqAdr();
        void reqSpeed();
       /*  void reqEprom();
        void reqSetAlarm();
        void reqSetAdr();
        void settingLamp(bool newstate); //восcтановим лайнедиты для шаров*/
};

#endif // GUNS_H
