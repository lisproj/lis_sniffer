#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "port.h"
#include "can_prot.h"
#include "qcustomplot.h"
#include "guns.h"
#include "plasticpanel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    enum{
        TAB_SETUP=0,
        TAB_LOGGER,
        TAB_PISTOLS,
        TAB_GYRO,
        TAB_MIK,
        TAB_PULT,
        TAB_STATISTIC_CAN,
        TAB_STATISTIC_MIK,
        TAB_COLD_PLASTIC,
        TAB_HOT_PLASTIC,
        TAB_UBUP,
        TAB_LOG_PARSER,
        TAB_GRAPHS,
        TAB_MAGISTRAL,
        TAB_MBO,
        TAB_SMS,
        TAB_DANF_SENS,
        //-----------------//
        TAB_ALL
    };

    enum{
        ID_SEND_STOP,
        ID_SEND_START,
        ID_SEND_START_CORR,
        ID_SEND_CLEANING_START,
        ID_SEND_CLEANING_STOP,
        ID_SEND_WASHING,
        ID_SEND_READ_MODE_ADC,
        ID_SEND_GUNS,
        ID_SEND_WORK_MODE,
        ID_SEND_CONF_BASE,
        ID_SEND_CYCLE,
        ID_SEND_GYRO_ON,
        ID_SEND_GYRO_OFF,
        ID_SEND_GUN_TEST,
        ID_SEND_GUN_TEST_MASK,
        ID_SEND_STOP_GUN_TEST,
        ID_SEND_SPEED_AUTO,
        ID_SEND_READ_DATA_MIK,
        ID_WRITE_EEP_PERC_LED_OFF,
        ID_WRITE_EEPROM_LR,
        ID_WRITE_EEPROM_LMARK,
        ID_WRITE_EEPROM_PERC_DET,
        ID_SEND_START_CYCLE,
        ID_SEND_STANDART,
        ID_SEND_CYCLE_STOP,
        ID_SEND_SETUP_MODUL,
        ID_SEND_SETUP_MODUL_CP,
        ID_SEND_MIXER_LEFT_RPM,
        ID_SEND_MIXER_RIGHT_RPM,
        ID_SEND_MIXER_TANK_RPM,
        ID_SEND_DRUM_RPM,
        ID_SEND_DANFOS_MAG_RPM,
        ID_SEND_COLD_SPRAY,
        ID_SEND_SET_RPM_MIXL,
        ID_SEND_SET_RPM_MIXR,
        ID_SEND_SET_RPM_PUMPL,
        ID_SEND_SET_RPM_PUMPR,
        ID_SEND_SET_RPM_HARDL,
        ID_SEND_SET_RPM_HARDR,
        ID_SEND_SET_RPM_PUMP_HP,
        ID_SEND_WEIGHT_MAT1,
        ID_SEND_WEIGHT_MAT2,
        ID_SEND_WEIGHT_SOG,
        ID_SEND_LIMIT_HARDENER,
        ID_SEND_READ_MBO,
        ID_SEND_WRITE_MBO_1,
        ID_SEND_WRITE_MBO_2,
        ID_SEND_WRITE_MBO_3,
        ID_SEND_WRITE_MBO_QUALITY,
        ID_SEND_EMUL_PAR_MBO,
        ID_SEND_EMUL_PAR_MBO_2,
        ID_SEND_EMUL_PAR_MBO_3,
        ID_SEND_EMUL_MBO_BC,
        ID_SEND_EMUL_MBO_EVENT_LED,
        ID_SEND_ENABLE_BLOCK,
        ID_SEND_START_STROKE,
        ID_SEND_START_SPACE,
        ID_SEND_PID_MAGISTRAL,
        ID_SEND_SWITCH_ON_LOG,
        ID_SEND_SWITCH_OFF_LOG,
        ID_SEND_SWITCH_ON_TEST_VALVES,
        ID_SEND_TP_CLEAN_ON,
        ID_SEND_TP_CLEAN_OFF,
        ID_SEND_TP_PREVIOUS,
        ID_SEND_TP_NEXT,
        ID_SEND_TP_ENCODER,
        ID_SEND_BC_UBUP_0,
        ID_SEND_BC_UBUP_1,
        ID_SEND_BC_CANHUB,
        ID_SEND_SMS_ADRES_NEW,
        ID_SEND_SMS_REQ_EEP,
        ID_SEND_SMS_REQ_ALARM,
        ID_SEND_SMS_CH_ADRES,
        ID_SEND_DANF_ADRES_NEW,
        ID_SEND_SPEED_DANFOSS,
        ID_VERS_CAN2CAN,
        ID_SET_MODE_CAN2CAN,
        ID_SEND_FAKE_QUAL,
        ID_SEND_JOY_UP_PRESS,
        ID_SEND_JOY_UP_REALESE,
        ID_SEND_JOY_DOWN_PRESS,
        ID_SEND_JOY_DOWN_REALESE,
        ID_SEND_JOY_RIGHT_PRESS,
        ID_SEND_JOY_RIGHT_REALESE,
        ID_SEND_JOY_LEFT_PRESS,
        ID_SEND_JOY_LEFT_REALESE,
        ID_SEND_SPEED_PRESS,
        ID_SEND_SPEED_REALESE,
    };

    enum
    {
        COL_MODE,
        COL_CONF,
        COL_CYCLE,
        COL_GUNS1,
        COL_GUNS2,
        COL_BEADS,
        COL_RPM_SCREW,
    //*********************
        COL_ALL
    };



    enum valueForGraph  //возможные графики
    {
        GR_SET_NONE,
        GR_SET_PPL,
        GR_RPM_PPL,
        GR_SET_PPR,
        GR_RPM_PPR,
        GR_RPM_MXL,
        GR_RPM_MXR,
        GR_SET_PHL,
        GR_SET_PHR,
        GR_HARD_L,
        GR_HARD_R,
        GR_CANAL_L,
        GR_CANAL_R,

        //**************
        GR_CP_ALL,

        GR_SET_NONE_P,
        GR_BUTTON_START,
        GR_CANAL_1,
        GR_CANAL_3,
        //**************
        GR_PAINT_ALL,

        GR_SET_NONE_M,
        GR_SET_MPUMP,
        GR_SET_MDRUM,
        GR_RPM_PUMP,        //обороты насоса пластика
        GR_PRESSURE,        // давление в разметочном блоке
        GR_FIST,            // работа кулачка
        GR_RPM_PUMP_DRUM,   // работа насоса барабана
        //**************
        GR_MAGISTRAL_ALL,
        GR_ALL
    };



    enum{
    LIGHT_THEME=0,
    DARK_THEME=2
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void writeData(QByteArray data);
    void writeCanData(QByteArray data);
    void savesettings(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);
    void toBeadsForm(int);

private:
    Ui::MainWindow *ui;
    Port *canPort;//Новый компорт
    PlasticPanel *plPanel;
    QCustomPlot *customPlot;    // Объявляем графическое полотно
    QCustomPlot *cPlGyro;    // Объявляем графическое полотно
    QCustomPlot *cPlMBO;    // Объявляем графическое полотно
    QCustomPlot *customPlotLog;       // Объявляем отдельное графическое полотно для скорости
    QCustomPlot *cPlgraph;       // Объявляем отдельное графиков оборотов
    QCPItemTracer *tracer;      // Трасировщик по точкам графика

    QCPItemTracer *tracerL;      // Трасировщик по точкам графика
    QCPItemTracer *tracerR;      // Трасировщик по точкам графика

    QMap<int, QString>  timelist;
    QMap<int, int>  X2list;
    QMap<int, int>  Vlist;
    QMap<int, int>  Hrlist;
    QMap<int, int>  DrPoslist;
    QMap<int, int>  DrSetlist;
    QMap<int, int>  Ximulist;
    QMap<int, double>  lenghtlist;
    QSettings    *settings; //  сохраняемые настройки
    int testSet1;
    int oldMukModul=-1;

    QString namesHPForCB[GR_ALL]; // имена для чекбоксов
    int saveValuePosGR[4]; //для сохранения типов данных графиков
    bool firstSetupCB=true;
    bool saveVisibleGR[4]; //для сохранения видимости данных графиков


    // QCPGraph *graphic;          // Объявляем график
    QElapsedTimer timerG;
    QTimer *timer;
    QTimer *timerStartMag;// таймер для задержек Магистральной между клапаном и насосом
    QTimer *timerStopMag;// таймер для задержек Магистральной между клапаном и насосом
    QTimer *timerAutoClick;// таймер для задержек старт стоп


    //Шнайдер тензосистема
    QTimer *timerTenzo; //таймер отправки тензо
    int periodTimerTenzo;  //время передачи тензодатчиков со Шнайдера

    bool flagIsStroke=false;

    uint_fast16_t testGun=0;
    qint32 maskTestGun=0;
   int colred[COL_ALL]; // Для индикации цветом свежих пакетов
   int statusCAN;  //байт состояиния подключенных модулей
   int currentGun=0;  // Номер выбранног пистолета для отправки сообщения
   int currentSensor=0;  // Номер выбранног Датчика для отправки сообщения
    bool readMode;  //Режим чтения/записи 1-чтение, 0- запись

   Mik *mymik; // левый или правый мик
   int windowTheme; //Тема для окна
   int pwmSpace,pwmStroke;// значения шим для магистральной
   int pidP,pidI,pidD;// значения пид
   int impPPM;//  импульсы на оборот
   int delayStartMg,delayStopMg;// задержка включения выключения насоса и клапана пластика
   int pumpValue; //объем помпы, 1/1000 дм^3
   int paintDens;   // плотность краски, 1/100 кг/дм^3



   float oldSquare;
   float oldSquareCur;//
   float currSquare;
   int lengForConsuption; //Длина для текущего расчетв
   int labelForCons;// метки для произведения рачета

   int CurrWeight1;// Приращение веса в кг
   int CurrWeight2;// Приращение веса в кг
   int CurrWeight3;// Приращение веса в кг

   int SaveWeight1;// Приращение веса в кг
   int SaveWeight2;// Приращение веса в кг
   int SaveWeight3;// Приращение веса в кг


   int DeltaWeight1;// Приращение веса в кг
   int DeltaWeight2;// Приращение веса в кг
   int DeltaWeight3;// Приращение веса в кг



      int SaveWeightBegin1;// Сохраненный вес на промежутке
      int SaveWeightEnd1;

      int SaveWeightBegin2;// Сохраненный вес на промежутке
      int SaveWeightEnd2;

      int SaveWeightBegin3;// Сохраненный вес на промежутке
      int SaveWeightEnd3;



   //тензодатчики
   float consMat1;  // зад.расход материал, кг/м^2
   float consMat2;  // зад.расход материал, кг/м^2
   float consSOG;  // зад.расход шары , кг/м^2
   int  canalMat1;  // канал материал 1
   int  canalMat2;  // канал материал 2
   int  canalSOG;  // канал SOG

   float consTotMat1;  //расч.расход материал, кг/м^2
   float consTotMat2;  // расч.расход материал, кг/м^2
   float consTotSOG;  // расч.расход шары , кг/м^2

   float consCurMat1;  //расч.расход материал, кг/м^2
   float consCurMat2;  // расч.расход материал, кг/м^2
   float consCurSOG;  // расч.расход шары , кг/м^2

   int techTerminalEncoder;
   int currSensDanf;



private slots:
    void setThemeWindow(int arg1);
    int stringToMask(QString string);
    QString maskToString(int mask); //
    void initForm(void);  // инициализация формы
    void RefrParams(int params,QVariant par1);
    void fromGunsToMIk(int gun);
    void requestSetEprSms(int sens);
    void requestSetAlarmSms(int sens);
    void requestSetAdr(int sens);
    void adrDanfToLabel(int adr);
    void setSpeedDanfoss200ms(int adr);
    void addToManager(QString text);// добавить информацию в информационное поле
    void Print(QString data);
    void PrintLog(int from, int to, int type, QString data,bool isComment);
    void on_PortNameBox_currentIndexChanged(const QString &arg1);
    void on_Btn_Serch_clicked();
    void PrintStatusBar(QString text);
    void stopCanHacker();
    void startCanHacker();
    void openLogFile();
    void clickOnCPLOG(QMouseEvent*event);
    void clickOnCPGraph(QMouseEvent*event);
    void exportCSV();
    void refreshParams();
    void realtimeDataSlot();
    void realtimeDataSlotGyro();
    void realtimeDataSlotMBO();
    void startAfterTimer();
    void stopAfterTimer();
    void startValveMag();
    void stopValveMag();
    void autoClick();
    void checkTenzo(); // запускает или останавливает таймер передачи пакетов в зав-сти от состояний чекбоксов тензо
    void emultenzoToCAN(); // запускает или останавливает таймер передачи пакетов в зав-сти от состояний чекбоксов тензо
    void emulJoyCanLIS(); // эмулятор панели ЛИС по CAN

    void makeConsuption(); //  эмулятор расхода

    void realtimeDataGRAPH(int numberGraph, int data);
    void addGun(); // Добавить пистолет
    void addmikStatToTable(int rowTable, int indData); // добавляет статистику в выбранный ряд таблицы

    void writeToCANatID(int id); // Отправить команду в CAN  по ID
    void writeMarkCycle(void); // Отправить цикл команд  в  МИК
    int  calcComandInCycle(void); // Посчитать к-во  команд  в  цикле для МИК
    void refreshModuleState(int state); //обновления статуса блоков подключения по CAN

    void setColorLineEdit(QLineEdit *lE,int number);
    void setColorComboBox(QComboBox *cb,int number);

    void initGraphforCP();  // инициализация графиков для ХП
    void initGraphforPaint();  // инициализация графиков для краски
    void initGraphforMagistral();  // инициализация графиков для магистральной
    void initGraphforTermo();  // инициализация графиков для термопластика

    void SetValveCPSate(int state);

    void on_pbWriteMode_clicked();
   // void on_pbSend1_clicked();
   // void on_pbSend2_clicked();
    void on_pbDeletePistol_clicked();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pbStart_clicked();
    void on_pbStop_clicked();
    void on_pbAddPistol_clicked();
 //   void on_pbSend3_clicked();
    void on_pbClearLog_clicked();
    void on_BtCLEAR_clicked();
    void on_pushButton_7_clicked();
    void on_pbGunTest_clicked();

    void autotestGuns();
    void on_pbSpeedAuto_clicked();
    void on_pbReadDataFromMIK_clicked();
    void on_pBWrLr_clicked();
    void on_pBWrPercDet_clicked();
    void on_pBWrPercOff_clicked();
    void on_pBWrLmark_clicked();
    void on_pbStartCycleSend_clicked();
    void on_cBGun1_stateChanged(int arg1);
    void on_cBGun2_stateChanged(int arg1);
    void on_cBGun3_stateChanged(int arg1);
    void on_cBGun4_stateChanged(int arg1);

    void on_cBGun5_stateChanged(int arg1);
    void on_cBGun6_stateChanged(int arg1);
    void on_cBGun7_stateChanged(int arg1);
    void on_cBGun8_stateChanged(int arg1);
    void on_cBGun9_stateChanged(int arg1);
    void on_cBGun10_stateChanged(int arg1);
    void on_cBGun11_stateChanged(int arg1);
    void on_cBGun12_stateChanged(int arg1);
    void on_cBGun13_stateChanged(int arg1);
    void on_cBGun14_stateChanged(int arg1);
    void on_cBGun15_stateChanged(int arg1);
    void on_cBGun16_stateChanged(int arg1);
    void on_cBGun17_stateChanged(int arg1);
    void on_cBGun18_stateChanged(int arg1);
    void on_cBGun19_stateChanged(int arg1);
    void on_cBGun20_stateChanged(int arg1);
    void on_cBGun21_stateChanged(int arg1);
    void on_cBGun22_stateChanged(int arg1);
    void on_pbResetTestGuns_clicked();
    void on_pbSendMOdul_clicked();
    void on_pbStart_2_clicked();
    void on_pbSendRPMLM_clicked();
    void on_pbSendRPMLRM_clicked();
    void on_pbSendRPMLP_clicked();
    void on_pbSendRPMLRP_clicked();
    void on_pbSendRPMLH_clicked();
    void on_pbSendRPMLRH_clicked();
    void on_pbSendRPMPHP_clicked();
    void on_pbSendMOdulCP_clicked();
    void on_pbSendCleanCmd_pressed();
    void on_pbSendCleanCmd_released();
    void on_pbSendWashCmd_clicked();
    void on_leSpeedAuto_editingFinished();
    void on_pbClearLog_2_clicked();
    void on_pbSetReadADC_clicked();
    void on_pbSendHardLimit_clicked();
    void on_cbGraph1_stateChanged(int arg1);
    void on_cbGraph2_stateChanged(int arg1);
    void on_cbGraph3_stateChanged(int arg1);
    void on_cbGraph4_stateChanged(int arg1);
    void on_cbData1_currentIndexChanged(int index);
    void on_cbData2_currentIndexChanged(int index);
    void on_cbData3_currentIndexChanged(int index);
    void on_cbData4_currentIndexChanged(int index);
    void on_pushButton_8_clicked();
    void on_pbReadMBO_clicked();
    void on_lEContrast_editingFinished();
    void on_lEIznos_editingFinished();
    void on_lEShum_editingFinished();
    void on_lEQuality_editingFinished();
    void on_lEContrast_returnPressed();
    void on_lEIznos_returnPressed();
    void on_lEShum_returnPressed();
    void on_lEQuality_returnPressed();
    void on_radioButton_2_clicked();
    void on_radioButton_clicked();
    void on_pBStartMagistral_clicked();
    void on_pBStopMagistral_clicked();
    void on_lePwmSpace_editingFinished();
    void on_lePwmStroke_editingFinished();
    void on_leDelayStartMg_editingFinished();
    void on_leDelayStopMg_editingFinished();
    void on_lEPumpValue_editingFinished();
    void on_lePaintDens_editingFinished();
    void on_pushButAuto_clicked();
    void on_pBenableLeft_clicked();
    void on_SlStroke_sliderMoved(int position);
    void on_SlSpase_sliderMoved(int position);
    void on_pBStartStroke_clicked();
    void on_pBStartSpace_clicked();
    void on_lePidP_editingFinished();
    void on_lePidI_editingFinished();
    void on_lePidD_editingFinished();
    void on_leimpRPM_editingFinished();
    void on_SlpidP_sliderMoved(int position);
    void on_SlpidI_sliderMoved(int position);
    void on_SlpidD_sliderMoved(int position);
    void on_pbMagPumpSet_clicked();
    void on_pbSwitchONLog_clicked();
    void on_pbSwitchOFFLog_clicked();
    void on_pBTestFisits_clicked();
    void on_pBTestOfffists_clicked();
    void on_pBStartAutoMagistral_clicked();
    void on_pBStopAutoMagistral_clicked();
    void on_pbClearGyroGraph_clicked();
    void on_pbTpClean_toggled(bool checked);
    void on_pbTpPrevious_clicked();
    void on_pbTpNext_clicked();
    void on_pbTpEncoder_sliderMoved(int position);
    void on_leConsMat1_editingFinished();
    void on_leConsMat2_editingFinished();
    void on_leConsSOG_editingFinished();
    void on_leCanMat1_editingFinished();
    void on_leCanMat2_editingFinished();
    void on_leCanSog_editingFinished();
    void on_pBSetAdressSMS_clicked();
    void on_pBSetAdressDanf_clicked();
    void on_pbTpEncoder_sliderReleased();
    void on_pbReadVersC2C_clicked();
    void on_pbSetModeC2C_clicked();

    void on_leLenForConsuption_editingFinished();
    void on_pbSpeedUbup_toggled(bool checked);
    void on_cbTechPanel_stateChanged(int arg1);
protected:

    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
